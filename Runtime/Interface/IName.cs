namespace Core.Interface
{
    public interface INamed
    {
        string Name { get; }
    }

    public interface INameable : INamed
    {
        void SetName(string name);
    }
}