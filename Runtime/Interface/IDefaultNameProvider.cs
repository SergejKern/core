﻿namespace Core.Runtime.Interface
{
    public interface IDefaultNameProvider
    {
        string DefaultName { get; }
    }
}
