﻿using System;
using Core.Unity.Types;

namespace Core.Interface
{
    public interface IFSMEventRef : IProvider<IFSMComponent>
    {
        string EventName { get; }
    }
    public interface IFSMStateRef : IProvider<IFSMComponent>
    {
        string StateName { get; }
    }

    public interface IFSMComponent : IProvider<IFSMConfig>
    {
        void ResetFSM();
        void SetState(string stateName);
        void SendEvent(string eventName);
        string ActiveStateName { get; }

        IFSMConfig Fsm { get; }
    }

    public interface IFSMConfig
    {
        string[] StateNames { get; }
        string[] EventNames { get; }
    }


    [Serializable]
    public class RefIFSMComponent : InterfaceContainer<IFSMComponent>{}
}