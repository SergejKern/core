namespace Core.Interface
{
    public interface IUpdatable
    {
        void Update();
    }
}