﻿namespace Core.Interface.Obsolete
{
    public interface IFSMProvider
    {
#if CORE_PLAYMAKER
        PlayMakerFSM FSM { get; }
#else
        IFSMComponent FSM { get; }
#endif
    }

    public interface IFSMEventRef : IFSMProvider
    {
        string EventName { get; }
    }
    public interface IFSMStateRef : IFSMProvider
    {
        string StateName { get; }
    }
    public interface IFSMVariableRef : IFSMProvider
    {
        string VariableName { get; }

        void ApplyOverride();
        void Sync();
    }

    public interface IFSMComponent
    {
        void Reset();
        void SetState(string stateName);
        void SendEvent(string eventName);
        string FsmName { get; set; }
        string FsmDescription { get; set; }
        bool Active { get; }
        string ActiveStateName { get; }
        IFSMTemplate FsmTemplate { get; }
        IFSM Fsm { get; }
    }

    public interface IFSMTemplate
    {
        // ReSharper disable once InconsistentNaming
        IFSM fsm { get; set; }
        string Category { get; set; }
    }

    public interface IFSM
    {
        IFSMEvent[] Events { get; set; }
        IFSMState[] States { get; set; }
    }

    public interface IFSMEvent
    {
        string Name { get; }
    }

    public interface IFSMState
    {
        IFsmTransition[] Transitions { get; set; }
        string Name { get; }
    }

    public interface IFsmTransition
    {
        string EventName { get; }
    }

}