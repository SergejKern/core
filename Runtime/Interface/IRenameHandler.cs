﻿namespace Core.Runtime.Interface
{
    public interface IRenamedHandler
    {
        void OnRenamed();
    }
}
