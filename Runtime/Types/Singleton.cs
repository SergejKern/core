
namespace Core.Types
{
    /// <summary> simple Singleton implementation </summary>
    public abstract class Singleton<T> where T : Singleton<T>, new()
    {
        static T m_instance;

        /// <summary> 
        /// Singleton Access
        /// </summary>
        public static T I
        {
            get
            {
                if (m_instance != null)
                    return m_instance;

                if (m_instance != null)
                    return m_instance;
                m_instance = new T();
                m_instance.Init();
                return m_instance;
            }
        }

        /// <summary> init the singleton </summary>
        protected virtual void Init() { }

        /// <summary> destroys the singleton </summary>
        public virtual void DestroyData() => m_instance = null;
    }
}
