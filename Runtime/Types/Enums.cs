
namespace Core.Types
{
    public enum ButtonClickResult
    {
        None,
        Clicked
    }
    public enum BoolOperator
    {
        AND,
        OR,
        N_AND,
        N_OR
    }
    public enum BinaryOperation
    {
        AND,
        OR,
        None,
    }
    public enum FilterResult
    {
        Match,
        Filtered
    }
    public enum OperationResult
    {
        OK,
        Error
    }
    public enum FlowResult
    {
        Continue,
        Stop
    }
    public enum AssignResult
    {
        Assigned,
        NotAssigned
    }
    public enum FindResult
    {
        Found,
        NotFound
    }
    public enum ChangeCheck
    {
        Changed,
        NotChanged
    }

    public enum Cardinals
    {
        North,
        East,
        South,
        West,
        Invalid,
    }

    public enum Orientation
    {
        Up,
        Left,
        Right,
        Forward,
        Back,
        Down,
        Invalid
    }

    /// <summary> IntersectionType eg. for bounds </summary>
    public enum IntersectionType
    {
        /// <summary> </summary>
        Intersects, /// <summary></summary>
        Contains, /// <summary></summary>
        Contained, /// <summary></summary>
        Touch, /// <summary></summary>
        None
    }
}
