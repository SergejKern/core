﻿// ReSharper disable InvalidXmlDocComment
// (edited from MM)

using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Events
{
    /// <summary>
    /// This class handles event management, and can be used to broadcast events throughout the game, to tell one class (or many) that something's happened.
    /// Events are structs, you can define any kind of events you want. This manager comes with GameEvents, which are 
    /// basically just made of a string, but you can work with more complex ones if you want.
    /// 
    /// To trigger a new event, from anywhere, do YOUR_EVENT.Trigger(YOUR_PARAMETERS)
    /// So GameEvent.Trigger("Save"); for example will trigger a Save GameEvent
    /// 
    /// you can also call EventManager.TriggerEvent(YOUR_EVENT);
    /// For example : EventManager.TriggerEvent(new GameEvent("GameStart")); will broadcast an GameEvent named GameStart to all listeners.
    ///
    /// To start listening to an event from any class, there are 3 things you must do : 
    ///
    /// 1 - tell that your class implements the EventListener interface for that kind of event.
    /// For example: public class GUIManager : Singleton<GUIManager>, EventListener<GameEvent>
    /// You can have more than one of these (one per event type).
    ///
    /// 2 - On Enable and Disable, respectively start and stop listening to the event :
    /// void OnEnable()
    /// {
    /// 	this.EventStartListening<GameEvent>();
    /// }
    /// void OnDisable()
    /// {
    /// 	this.EventStopListening<GameEvent>();
    /// }
    /// 
    /// 3 - Implement the EventListener interface for that event. For example :
    /// public void OnEvent(GameEvent gameEvent)
    /// {
    /// 	if (gameEvent.eventName == "GameOver")
    ///		{
    ///			// DO SOMETHING
    ///		}
    /// } 
    /// will catch all events of type GameEvent emitted from anywhere in the game, and do something if it's named GameOver
    ///</summary>
    public static class EventMessenger
    {
        static readonly Dictionary<Type, List<IEventListenerBase>> k_subscribersList;

        static EventMessenger() => k_subscribersList = new Dictionary<Type, List<IEventListenerBase>>();

        /// <summary>
        /// Adds a new subscriber to a certain event.
        /// </summary>
        /// <param name="listener">listener.</param>
        /// <typeparam name="T">The event type.</typeparam>
        public static void AddListener<T>(IEventListener<T> listener) where T : struct
        {
            var eventType = typeof(T);

            if (!k_subscribersList.ContainsKey(eventType))
                k_subscribersList[eventType] = new List<IEventListenerBase>();

            if (!SubscriptionExists(eventType, listener))
                k_subscribersList[eventType].Add(listener);
        }

        /// <summary>
        /// Removes a subscriber from a certain event.
        /// </summary>
        /// <param name="listener">listener.</param>
        /// <typeparam name="T">The event type.</typeparam>
        public static void RemoveListener<T>(IEventListener<T> listener) where T : struct
        {
            var eventType = typeof(T);

            if (!k_subscribersList.ContainsKey(eventType))
                return;
            
            var subscriberList = k_subscribersList[eventType];

            for (var i = 0; i < subscriberList.Count; i++)
            {
                if (subscriberList[i] != listener)
                    continue;

                subscriberList.Remove(subscriberList[i]);

                if (subscriberList.Count == 0)
                    k_subscribersList.Remove(eventType);

                return;
            }
        }

        /// <summary>
        /// Triggers an event. All instances that are subscribed to it will receive it (and will potentially act on it).
        /// </summary>
        /// <param name="newEvent">The event to trigger.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void TriggerEvent<T>(T newEvent) where T : struct
        {
            if (!k_subscribersList.TryGetValue(typeof(T), out var list))
                return;
            using (var scoped = Types.SimplePool<List<IEventListenerBase>>.I.GetScoped())
            {
                scoped.Obj.AddRange(list);
                foreach (var t in scoped.Obj)
                    (t as IEventListener<T>)?.OnEvent(newEvent);
            }
        }

        /// <summary>
        /// Checks if there are subscribers for a certain type of events
        /// </summary>
        /// <returns><c>true</c>, if exists was subscriptioned, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        /// <param name="receiver">Receiver.</param>
        static bool SubscriptionExists(Type type, IEventListenerBase receiver)
        {
            return k_subscribersList.TryGetValue(type, out var receivers) 
                   && receivers.Any(t => t == receiver);
        }
    }

    /// <summary>
    /// Static class that allows any class to start or stop listening to events
    /// </summary>
    public static class EventRegister
    {
        //public delegate void Delegate<in T>(T eventType);

        public static void EventStartListening<T>(this IEventListener<T> caller) where T : struct => EventMessenger.AddListener(caller);

        public static void EventStopListening<T>(this IEventListener<T> caller) where T : struct => EventMessenger.RemoveListener(caller);
    }

    /// <summary>
    /// Event listener basic interface
    /// </summary>
    public interface IEventListenerBase { };

    /// <inheritdoc />
    /// <summary>
    /// A public interface you'll need to implement for each type of event you want to listen to.
    /// </summary>
    public interface IEventListener<in T> : IEventListenerBase
    {
        void OnEvent(T eventType);
    }
}