﻿using static Core.Events.EventMessenger;

namespace Core.Events
{
    /// <summary>
    /// GameEvents are used throughout the game for general game events (game started, game ended, life lost, etc.)
    /// </summary>
    public struct GameEvent
    {
        public string EventName;
        public GameEvent(string newName) => EventName = newName;
        public static void Trigger(string newName) => Trigger(new GameEvent(newName));
        public static void Trigger(GameEvent e) => TriggerEvent(e);
    }
}