using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Unity.Types
{
    /// <inheritdoc />
    /// <summary>
    /// Derivatives should be decorated with [System.Serializable] attribute.
    /// </summary>
    [Serializable]
    public abstract class InterfaceContainer<TResult> : InterfaceContainerBase.InterfaceContainerBase
        where TResult : class
    {
        public TResult Result
        {
            //Using the null coalescing operator will break web player execution
            get
            {
#if UNITY_EDITOR
                if (ObjectField == null && string.IsNullOrEmpty(ResultType))
                    return m_result = null;

                if (string.IsNullOrEmpty(ResultType))
                    m_result = null;
#endif

                return m_result ?? (m_result = ObjectField as TResult);
            }
            set
            {
                m_result = value;
                ObjectField = m_result as Object;

#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    if (m_result != null && ObjectField == null)
                    {
                        Debug.LogWarning("IUnifiedContainer: Cannot set Result property to non UnityEngine.Object derived types while application is not running.");
                        m_result = null;
                    }
                }
                ResultType = m_result != null ? ConstructResolvedName(m_result.GetType()) : "";
#endif
            }
        }

        public Object Object => ObjectField != null ? ObjectField : (ObjectField = m_result as Object);

        TResult m_result;
    }

    namespace InterfaceContainerBase
    {
        /// <summary>
        /// Used to enable a single CustomPropertyDrawer for all derivatives.
        /// Do not derive from this class, use the generic IUnifiedContainer&lt;TResult&gt; class instead.
        /// </summary>
        [Serializable]
        public abstract class InterfaceContainerBase
        {
            [SerializeField]
            [HideInInspector]
            protected Object ObjectField;

            //#if UNITY_EDITOR - Excluding this from the build seems to freak the serializer out and somehow result in prefab references coming through null - non-prefabs seem to continue working though.
            //Used internally to display properly in drawer.
#pragma warning disable 414
            [SerializeField]
            [HideInInspector]
            protected string ResultType;
#pragma warning restore 414
            //#endif

            static readonly Regex k_typeArgumentsReplace = new Regex(@"`[0-9]+");
            public static string ConstructResolvedName(Type type)
            {
                var typeName = type.Name;

                if (!type.IsGenericType)
                {
                    return typeName;
                }

                var argumentsString = type.GetGenericArguments().Aggregate((string)null,
                    (s, t) => s == null ? (ConstructResolvedName(t)) : $"{s}, {(ConstructResolvedName(t))}");
                return k_typeArgumentsReplace.Replace(typeName, $"<{argumentsString}>");
            }

#if UNITY_EDITOR
            public static string Editor_ObjectFieldPropName => nameof(ObjectField);
#endif
        }
    }
}

