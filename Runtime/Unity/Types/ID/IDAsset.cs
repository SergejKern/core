using Core.Unity.Attributes;
using UnityEngine;

namespace Core.Unity.Types.ID
{
    [EditorIcon("icon-id")]
    /// <summary>
    /// Create assets to help with identification and linking
    /// </summary>
    public class IDAsset : ScriptableObject { }
}