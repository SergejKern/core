using System.Collections.Generic;
using System.Linq;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using Core.Unity.Types.Attribute;
using UnityEditor;
using UnityEngine;

namespace Core.Unity.Types.ID
{
    [EditorIcon("icon-id")]
    [CreateAssetMenu()]
    public class IDConfig : ScriptableObject, IEnumGenerator
    {
        [ClassExtends(typeof(IDAsset))]
        public ClassTypeReference IDType; 
        public IDAsset[] Ids = new IDAsset[0];

        // optional:
        [SerializeField] internal DefaultAsset m_folder;
        [SerializeField] internal int m_namespaceStart;

        public DefaultAsset Folder => m_folder;
        public int NameSpaceStart => m_namespaceStart;
        public string EnumName => name;

        public IEnumerable<string> EnumEntries => Ids.Select(id => id.name);
        public string OutputFile => $"{AssetDatabase.GetAssetPath(Folder)}/{EnumName}.cs";
        public string NameSpace
        {
            get
            {
                if (Folder == null)
                    return "";

                var folder = $"{AssetDatabase.GetAssetPath(Folder)}";
                var folders = folder.Split('/');
                if (folders.Length == 0)
                    return "";

                var nameSpace = "";
                for (var i = NameSpaceStart; i < (folders.Length - 1); i++) 
                    nameSpace += $"{folders[i]}.";

                nameSpace += folders[folders.Length - 1];
                return nameSpace;
            }
        }

#if UNITY_EDITOR
        public static string Editor_FolderPropName => nameof(m_folder);
        public static string Editor_NamespacePropName => nameof(m_namespaceStart);
#endif
    }
}