using Core.Interface.Obsolete;
using UnityEngine;

#if CORE_PLAYMAKER
using System;
using Core.Extensions;
using HutongGames.PlayMaker;
#pragma warning disable 0649 // wrong warnings for SerializeField

namespace Core.Unity.Types.Fsm.Obsolete
{
    public static class FSMVariableExt
    {
        public static bool Find<T>(this IFSMVariableRef varRef, T[] arr, out int idx) where T : NamedVariable
        {
            idx = -1;
            if (varRef.VariableName.IsNullOrEmpty() || varRef.VariableName.Equals(Name.Ignore))
                return false;
            var searchName = varRef.VariableName;
            idx = Array.FindIndex(arr, s => string.Equals(s.Name, searchName));
            return idx != -1;
        }

        internal static bool NoFsm(this IFSMVariableRef var)
        {
            if (var.FSM != null)
                return false;
            // Debug.LogWarning($"FSM for variable {var.VariableName} is null");
            return true;
        }
    }

    [Serializable]
    public struct FSMStringRef : IFSMVariableRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_variableName;

        [SerializeField] string m_value;
        public string Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.StringVariables, out var idx))
                return;

            m_value = m_fsm.FsmVariables.StringVariables[idx].Value;
        }

        public void ApplyOverride()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.StringVariables, out var idx))
                return;

            m_fsm.FsmVariables.StringVariables[idx].Value = m_value;
        }
    }

    [Serializable]
    public struct FSMFloatRef : IFSMVariableRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_variableName;

        [SerializeField] float m_value;
        public float Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.FloatVariables, out var idx))
                return;

            m_value = m_fsm.FsmVariables.FloatVariables[idx].Value;
        }

        public void ApplyOverride()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.FloatVariables, out var idx))
                return;
            m_fsm.FsmVariables.FloatVariables[idx].Value = m_value;
        }
    }

    [Serializable]
    public struct FSMIntRef : IFSMVariableRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_variableName;

        [SerializeField] int m_value;
        public int Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.IntVariables, out var idx))
                return;
            m_value = m_fsm.FsmVariables.IntVariables[idx].Value;
        }

        public void ApplyOverride()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.IntVariables, out var idx))
                return;
            m_fsm.FsmVariables.IntVariables[idx].Value = m_value;
        }
    }

    [Serializable]
    public struct FSMBoolRef : IFSMVariableRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_variableName;

        [SerializeField] bool m_value;
        public bool Value {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter
        public void Sync()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.BoolVariables, out var idx))
                return;
            m_value = m_fsm.FsmVariables.BoolVariables[idx].Value;
        }

        public void ApplyOverride()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.BoolVariables, out var idx))
                return;
            m_fsm.FsmVariables.BoolVariables[idx].Value = m_value;
        }
    }

    [Serializable]
    public struct FSMColorRef : IFSMVariableRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_variableName;

        [SerializeField] Color m_value;
        public Color Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter
        public void Sync()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.ColorVariables, out var idx))
                return;
            m_value = m_fsm.FsmVariables.ColorVariables[idx].Value;
        }

        public void ApplyOverride()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.ColorVariables, out var idx))
                return;
            m_fsm.FsmVariables.ColorVariables[idx].Value = m_value;
        }
    }

    [Serializable]
    public struct FSMGameObjectRef : IFSMVariableRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_variableName;

        [SerializeField] GameObject m_value;
        public GameObject Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }
        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.GameObjectVariables, out var idx))
                return;

            m_value = m_fsm.FsmVariables.GameObjectVariables[idx].Value;
        }

        public void ApplyOverride()
        {
            if (this.NoFsm()) return;

            if (!this.Find(m_fsm.FsmVariables.GameObjectVariables, out var idx))
                return;
            m_fsm.FsmVariables.GameObjectVariables[idx].Value = m_value;
        }
    }
}
#else
// todo 1: UnityEvents if Playmaker not available
// currently these are empty stubs/ implementations
// ReSharper disable UnassignedGetOnlyAutoProperty
namespace Core.Unity.Types.Fsm
{
    public struct FSMStringRef : IFSMVariableRef
    {
        public IFSMComponent FSM { get; }
        public string Value { get; set; }
        public string VariableName => "";
        public void Sync() { }
        public void ApplyOverride() { }
    }
    public struct FSMFloatRef : IFSMVariableRef
    {
        public IFSMComponent FSM { get; }
        public float Value { get; set; }
        public string VariableName => "";
        public void Sync() { }
        public void ApplyOverride() { }
    }
    public struct FSMIntRef : IFSMVariableRef
    {
        public IFSMComponent FSM { get; }
        public int Value { get; set; }
        public string VariableName => "";
        public void Sync() { }
        public void ApplyOverride() { }
    }
    public struct FSMBoolRef : IFSMVariableRef
    {
        public IFSMComponent FSM { get; }
        public bool Value { get; set; }
        public string VariableName => "";
        public void Sync() { }
        public void ApplyOverride() { }
    }
    public struct FSMColorRef : IFSMVariableRef
    {
        public IFSMComponent FSM { get; }
        public Color Value { get; set; }
        public string VariableName => "";
        public void Sync() { }
        public void ApplyOverride() { }
    }
    public struct FSMGameObjectRef : IFSMVariableRef
    {
        public IFSMComponent FSM { get; }
        public GameObject Value { get; set; }
        public string VariableName => "";
        public void Sync() { }
        public void ApplyOverride(){}
    }
}

#endif