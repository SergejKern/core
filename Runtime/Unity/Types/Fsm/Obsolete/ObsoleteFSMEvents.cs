using Core.Interface.Obsolete;
using UnityEngine;
using JetBrains.Annotations;

#if CORE_PLAYMAKER
using System;
using Core.Extensions;
#pragma warning disable 0649 // wrong warnings for SerializeField

using Object = UnityEngine.Object;

namespace Core.Unity.Types.Fsm.Obsolete
{
    public static class FSMEventExt
    {
        internal static bool NoFsm(this IFSMEventRef ev)
        {
            if (ev.FSM != null)
                return false;
            //Debug.LogWarning($"FSM for event {ev.EventName} is null");
            return true;
        }
    }

    [Serializable]
    public struct FSMEventRef : IFSMEventRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string EventName => m_eventName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke()
        {
            if (this.NoFsm()) return;

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            m_fsm.SendEvent(m_eventName);
        }
        // todo 1: Playmaker Validation
    }

    [Serializable]
    public struct FSMFloatEventRef : IFSMEventRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string EventName => m_eventName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke(float f)
        {
            if (this.NoFsm()) return;

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            HutongGames.PlayMaker.Fsm.EventData.FloatData = f;
            m_fsm.SendEvent(m_eventName);
        }
    }


    [Serializable]
    public struct FSMObjectEventRef : IFSMEventRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string EventName => m_eventName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke(Object o)
        {
            if (this.NoFsm()) return;

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            HutongGames.PlayMaker.Fsm.EventData.ObjectData = o;
            m_fsm.SendEvent(m_eventName);
        }
    }

    //public class FSMDataWrapperObject : Object
    //{
    //    public object Data;
    //}
    //[Serializable]
    //public struct FSMDataEvent
    //{
    //    [SerializeField] PlayMakerFSM fsm;
    //    [SerializeField] string eventName;
    //    // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
    //    public PlayMakerFSM FSM => fsm;
    //    public string EventName => eventName;
    //    // ReSharper restore ConvertToAutoPropertyWithPrivateSetter
    //    public void Invoke(object o)
    //    {
    //        // todo 1: pool this
    //        Fsm.EventData.ObjectData = new FSMDataWrapperObject() { Data = o };
    //        fsm.SendEvent(eventName);
    //    }
    //}

    [Serializable]
    public struct FSMColliderEventRef : IFSMEventRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string EventName => m_eventName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke(Collider c)
        {
            if (this.NoFsm()) return;

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            HutongGames.PlayMaker.Fsm.EventData.ObjectData = c;
            FSM.SendEvent(m_eventName);
        }
    }

    [Serializable]
    public struct FSMTransformEventRef : IFSMEventRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string EventName => m_eventName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke(Transform t)
        {
            if (this.NoFsm()) return;

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            HutongGames.PlayMaker.Fsm.EventData.GameObjectData = t.gameObject;
            m_fsm.SendEvent(m_eventName);
        }
    }

    [Serializable]
    public struct FSMGameObjectEventRef : IFSMEventRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string EventName => m_eventName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke(GameObject g)
        {
            if (this.NoFsm()) return;

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            HutongGames.PlayMaker.Fsm.EventData.GameObjectData = g;
            m_fsm.SendEvent(m_eventName);
        }
    }

    [Serializable]
    public struct FSMVector2EventRef : IFSMEventRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string EventName => m_eventName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke(Vector2 vec)
        {
            if (this.NoFsm()) return;

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            HutongGames.PlayMaker.Fsm.EventData.Vector2Data = vec;
            m_fsm.SendEvent(m_eventName);
        }
        // todo 1: Playmaker Validation
    }

    [Serializable]
    public struct FSMStateRef : IFSMStateRef
    {
        [SerializeField] PlayMakerFSM m_fsm;
        [SerializeField] string m_stateName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public PlayMakerFSM FSM => m_fsm;
        public string StateName => m_stateName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public static implicit operator string(FSMStateRef s) => s.StateName;
    }
}

#else
// todo 1: UnityEvents if Playmaker not available
// currently these are empty stubs/ implementations
// ReSharper disable UnassignedGetOnlyAutoProperty
namespace Core.Unity.Types.Fsm.Obsolete
{
    public struct FSMEventRef : IFSMEventRef
    {
        public IFSMComponent FSM { get; }
        public string EventName { get; }
        public void Invoke() { }
    }
    public struct FSMFloatEventRef : IFSMEventRef
    {
        public IFSMComponent FSM { get; }
        public string EventName { get; }
        public void Invoke(float f) { }
    }
    public struct FSMObjectEventRef : IFSMEventRef
    {
        public IFSMComponent FSM { get; }
        public string EventName { get; }
        public void Invoke(Object o) { }
    }
    public struct FSMColliderEventRef : IFSMEventRef
    {
        public IFSMComponent FSM { get; }
        public string EventName { get; }
        public void Invoke(Collider c) { }
    }
    public struct FSMTransformEventRef : IFSMEventRef
    {
        public IFSMComponent FSM { get; }
        public string EventName { get; }
        public void Invoke(Transform t) { }
    }
    public struct FSMGameObjectEventRef : IFSMEventRef
    {
        public IFSMComponent FSM { get; }
        public string EventName { get; }
        public void Invoke(GameObject g) { }
    }
    public struct FSMVector2EventRef : IFSMEventRef
    {
        public IFSMComponent FSM { get; }
        public string EventName { get; }
        public void Invoke(Vector2 vec) { }
    }
    public struct FSMStateRef : IFSMStateRef
    {
        public IFSMComponent FSM { get; }
        public string StateName { get; }
        public static implicit operator string(FSMStateRef s) => s.StateName;
    }
}
#endif

namespace Core.Unity.Types.Fsm.Obsolete
{
    [BaseTypeRequired(typeof(IFSMProvider))]
    public class FSMDrawerAttribute : PropertyAttribute
    {
        public string AutoSelectName;
        public bool Self;
        public bool EditVariable;
        public FSMDrawerAttribute(string autoSelectName = "", bool self = true, bool editVariable = true)
        {
            AutoSelectName = autoSelectName;
            Self = self;
            EditVariable = editVariable;
        }
    }
}