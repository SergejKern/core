using System;
using Core.Interface;
using Core.Unity.Interface;
using Core.Unity.Types.Attribute;
using UnityEngine;

using JetBrains.Annotations;

#pragma warning disable 0649 // wrong warnings for SerializeField

namespace Core.Unity.Types
{
    public static class Name
    {
        public const string Ignore = "-";
    }

    public static class AnimatorTypePropNames
    {
        public const string AnimatorPropName = nameof(AnimatorStateRef.m_animator);
        public const string ControllerPropName = nameof(AnimatorStateRef.m_controller);
        public const string StatePropName = nameof(AnimatorStateRef.m_stateName);
        public const string ParameterPropName = nameof(AnimatorTriggerRef.m_parameterName);
        public const string ValuePropName = nameof(AnimatorBoolRef.m_value);
    }

    [Serializable]
    public struct AnimatorStateRef : IAnimatorState
    {
        public AnimatorStateRef(Animator anim, string stateName)
        {
            m_animator = anim;
            m_controller = anim.runtimeAnimatorController;
            m_stateName = stateName;
        }
        public AnimatorStateRef(RuntimeAnimatorController controller, string stateName)
        {
            m_animator = null;
            m_controller = controller;
            m_stateName = stateName;
        }

        [SerializeField] internal Animator m_animator;
        [SerializeField] internal RuntimeAnimatorController m_controller;
        [SerializeField] internal string m_stateName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Animator Animator => m_animator;
        public RuntimeAnimatorController Controller => m_animator != null ? m_animator.runtimeAnimatorController : m_controller;
        public string StateName => m_stateName;

        public int StateHash => Animator.StringToHash(m_stateName);
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public static implicit operator int(AnimatorStateRef s) => s.StateHash;
    }

    [Serializable]
    public struct AnimatorTriggerRef : IAnimatorParameter
    {
        public AnimatorTriggerRef (RuntimeAnimatorController ctrl, string param)
        {
            m_animator = null;
            m_controller = ctrl;
            m_parameterName = param;
        }

        [SerializeField] internal Animator m_animator;
        [SerializeField] internal RuntimeAnimatorController m_controller;
        [SerializeField] internal string m_parameterName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Animator Animator => m_animator;
        public RuntimeAnimatorController Controller => m_animator != null ? m_animator.runtimeAnimatorController : m_controller;

        public string ParameterName => m_parameterName;

        public int ParameterHash => Animator.StringToHash(m_parameterName);
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public static implicit operator int(AnimatorTriggerRef s) => s.ParameterHash;

        public void Invoke()
        {
            if (m_animator != null)
                m_animator.SetTrigger(m_parameterName);
        }
    }


    [Serializable]
    public struct AnimatorBoolRef : IAnimatorVariableRef
    {
        [SerializeField] internal Animator m_animator;
        [SerializeField] internal RuntimeAnimatorController m_controller;

        [SerializeField] internal string m_parameterName;
        [SerializeField] internal bool m_value;

        public bool Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public RuntimeAnimatorController Controller => m_animator != null ? m_animator.runtimeAnimatorController : m_controller;
        public string ParameterName => m_parameterName;
        public int ParameterHash => Animator.StringToHash(m_parameterName);

        public static implicit operator int(AnimatorBoolRef s) => s.ParameterHash;
        public Animator Animator
        {
            get => m_animator;
            set => m_animator = value;
        }
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void ApplyOverride()
        {
            if (m_animator != null)
                m_animator.SetBool(ParameterHash, m_value);
        }

        public void Sync() => m_value = m_animator.GetBool(ParameterHash);
    }

    [Serializable]
    public struct AnimatorFloatRef : IAnimatorVariableRef
    {
        [SerializeField] internal Animator m_animator;
        [SerializeField] internal RuntimeAnimatorController m_controller;
        [SerializeField] internal string m_parameterName;
        [SerializeField] internal float m_value;

        public float Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }
        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public RuntimeAnimatorController Controller => m_animator != null ? m_animator.runtimeAnimatorController : m_controller;
        public string ParameterName => m_parameterName;
        public int ParameterHash => Animator.StringToHash(m_parameterName);

        public static implicit operator int(AnimatorFloatRef s) => s.ParameterHash;
        public Animator Animator
        {
            get => m_animator;
            set => m_animator = value;
        }
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void ApplyOverride()
        {
            if (m_animator != null)
                m_animator.SetFloat(ParameterHash, m_value);
        }

        public void Sync() => m_value = m_animator.GetFloat(ParameterHash);
    }

    [BaseTypeRequired(typeof(IAnimatorVariableRef))]
    public class AnimTypeDrawerAttribute : ContextDrawerAttribute
    {
        public bool HideAnimator;

        public AnimTypeDrawerAttribute(string autoSelectName = AutoSelectNameDefault,
            Condition setToSelf = ContextSelfDefault,
            Condition showContext = ShowContextSelectorDefault,
            bool hideLabel = HideLabelDefault,
            bool showOnlyIfUnlinked = ShowOnlyIfUnlinkedDefault,
            bool breakAfterLabel = BreakAfterLabelDefault,
            bool hideAnimator = false)
            : base(autoSelectName, setToSelf, showContext, hideLabel, showOnlyIfUnlinked, breakAfterLabel)
        {
            HideAnimator = hideAnimator;
        }
    }

    public struct AnimatorAsControllerProvider : IProvider<RuntimeAnimatorController>
    {
        public Animator Animator;

        public void Get(out RuntimeAnimatorController provided) 
            => provided = Animator.runtimeAnimatorController;
    }
}