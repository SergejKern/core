using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Unity.Extensions;
using Core.Types;
using Core.Unity.Interface;
using UnityEngine;

namespace Core.Unity.Types
{
    /// <summary> Octree </summary>
    public class Octree<T> where T : IProvider<Bounds>
    {
        // ReSharper disable InconsistentNaming
        enum Position
        {
            UFL,
            UFR,
            UBL,
            UBR,
            DFL,
            DFR,
            DBL,
            DBR
        }
        // ReSharper restore InconsistentNaming
        /// <summary> Size of octree </summary>
        public float Size => Bounds.size.x;

        const int k_maxObjectsInNode = 4;
        const float k_minSize = 8.0f;

        readonly List<T> m_objects;
        public Bounds Bounds { get; private set; }
        public void Get(out Bounds provided) => provided = Bounds;

        readonly Octree<T> m_parent;
        public Octree<T>[] LeafNodes { get; }

        public bool HasLeaves { get; private set; }
        bool HasObjects => (m_objects.Count != 0);
        bool NeedSplit => (m_objects.Count > k_maxObjectsInNode && Size >= k_minSize);
        bool IsEmpty => ((!HasObjects) && (HasLeaves == false));
        bool AreLeavesEmpty
        {
            get
            {
                if (HasLeaves == false)
                    return true;

                for (var i = 0; i < 8; ++i)
                {
                    if (LeafNodes[i]?.IsEmpty == false)
                        return false;
                }
                return true;
            }
        }

        /// <summary> constructor </summary>
        public Octree(Vector3 center, float size, Octree<T> parent = null)
        {
            m_parent = parent;
            m_objects = new List<T>();
            LeafNodes = new Octree<T>[8];

            var diagonalVector = new Vector3(size / 2.0f, size / 2.0f, size / 2.0f);
            Bounds = new Bounds(center, 2 * diagonalVector);
        }

        IntersectionType GetIntersectionType(Bounds bounds)
        {
            return Bounds.GetIntersectionType(bounds);
        }

        void ClearLeaves()
        {
            for (var i = 0; i < 8; ++i)
                LeafNodes[i] = null;
            HasLeaves = false;
        }

        /// <summary> if Octtree contains Objects </summary>
        public bool ContainsObjectsRecursive()
        {
            if (m_objects.Count != 0)
                return true;

            if (!HasLeaves)
                return false;

            for (var index = 0; index < 8; index++)
            {
                if (LeafNodes[index].ContainsObjectsRecursive())
                    return true;
            }

            return false;
        }

        FindResult IndexOfObject(T octreeEntity, ref int objIndex)
        {
            if (!HasObjects)
                return FindResult.NotFound;

            objIndex = m_objects.IndexOf(octreeEntity);
            return -1 == objIndex ? FindResult.NotFound : FindResult.Found;
        }

        /// <summary> if Octree is free at bounds </summary>
        public bool IsFreePlace(Bounds b)
        {
            var t = GetIntersectionType(b);
            if (t != IntersectionType.None && t != IntersectionType.Touch)
                return _IsFreePlace(b);

            Debug.LogError("Outside of OctTree");
            return false;
        }

        bool _IsFreePlace(Bounds b)
        {
            var t = GetIntersectionType(b);
            if (t == IntersectionType.None || t == IntersectionType.Touch)
                return true;
            foreach (var obj in m_objects)
            {
                obj.Get(out var childBounds);
                t = childBounds.GetIntersectionType(b);
                if (t != IntersectionType.None && t!= IntersectionType.Touch)
                    return false;
            }
            return !HasLeaves || LeafNodes.All(leaf => leaf._IsFreePlace(b));
        }

        /// <summary>
        /// Gathers Objects that are contained in or intersecting with given bounds, returns true when no objects are found
        /// </summary>
        /// <param name="b">the bounds for the check</param>
        /// <param name="intersectingObjects">list of objects contained in or intersecting with bounds</param>
        /// <returns>whether it is a free place</returns>
        public bool IsFreePlaceOrGatherIntersecting(Bounds b, ref List<T> intersectingObjects)
        {
            var t = GetIntersectionType(b);
            if (t != IntersectionType.None && t != IntersectionType.Touch)
                return _IsFreePlaceOrGatherIntersecting(b, ref intersectingObjects);

            Debug.LogError("Outside of OctTree");
            return false;
        }

        bool _IsFreePlaceOrGatherIntersecting(Bounds b, ref List<T> intersectingObjects)
        {
            var t = GetIntersectionType(b);
            if (t == IntersectionType.None || t == IntersectionType.Touch)
                return true;
            foreach (var obj in m_objects)
            {
                obj.Get(out var childBounds);
                t = childBounds.GetIntersectionType(b);
                if (t != IntersectionType.None && t != IntersectionType.Touch)
                {
                    intersectingObjects.Add(obj);
                }
            }
            if (!HasLeaves)
                return intersectingObjects.Count == 0;
            foreach (var leaf in LeafNodes)
            {
                leaf._IsFreePlaceOrGatherIntersecting(b, ref intersectingObjects);
            }
            return intersectingObjects.Count == 0;
        }

        /// <summary> finds object enclosing given position </summary>
        public FindResult GetObjectEnclosing(Vector3 position, out T outObj)
        {
            outObj = default(T);
            if (!Bounds.Contains(position))
                return FindResult.NotFound;
            foreach(var obj in m_objects)
            {
                obj.Get(out var childBounds);
                if (!childBounds.Contains(position))
                    continue;
                outObj = obj;
                return FindResult.Found;
            }
            if (!HasLeaves)
                return FindResult.NotFound;
            foreach(var leaf in LeafNodes)
            {
                if (leaf.GetObjectEnclosing(position, out outObj) == FindResult.Found)
                    return FindResult.Found;
            }
            return FindResult.NotFound;
        }

        void FindTreeContainingObject(T entity, ref Octree<T> tree, ref int atIndex)
        {
            entity.Get(out var entityBounds);
            var t = GetIntersectionType(entityBounds);
            if (t == IntersectionType.None || t == IntersectionType.Touch) // Object is definitely not in this tree
                return;

            if (IndexOfObject(entity, ref atIndex) == FindResult.Found)
            {
                tree = this;
                return;
            }
            if (!HasLeaves)
                return;
            foreach (var node in LeafNodes)
            {
                node.FindTreeContainingObject(entity, ref tree, ref atIndex);
                if (tree != null)
                    break;
            }
        }

        // void RemoveObjectUnmerged(int objectIndex) => m_objects.RemoveAt(objectIndex);

        void RemoveObjectAt(int objectIndex)
        {
            // remove the object            
            m_objects.RemoveAt(objectIndex);

            // if all the children are empty lets clear the list
            if (AreLeavesEmpty)
            {
                ClearLeaves();
            }

            // try and collapse / merge the Octree by notifying the parent to check other children
            if (IsEmpty)
            {
                m_parent?.AttemptMerge();
            }
        }

        /// <summary> finds and removes object </summary>
        public FindResult FindAndRemoveObject(T entity)
        {
            Octree<T> tree = null;
            var atIndex = -1;
            FindTreeContainingObject(entity, ref tree, ref atIndex);

            if (tree == null)
                return FindResult.NotFound;

            tree.RemoveObjectAt(atIndex);
            return FindResult.Found;
        }

        /// <summary> Cleans up leaves upward </summary>
        protected void AttemptMerge()
        {
            if (!AreLeavesEmpty)
                return;
            ClearLeaves();

            if (!HasObjects)
            {
                m_parent?.AttemptMerge();
            }
        }

        /// <summary> adds object </summary>
        public OperationResult AddObject(T entity)
        {
            entity.Get(out var entityBounds);
            var t = GetIntersectionType(entityBounds);
            if (t == IntersectionType.None || t == IntersectionType.Touch)
                return OperationResult.Error;

            if (!HasLeaves) // If no children
            {
                m_objects.Add(entity);

                if (!NeedSplit)
                    return OperationResult.OK;

                CreateLeaveNodes();
                DistributeObjectsToChildren();
            }
            else if (AddObjectToChildren(entity) == OperationResult.Error)
                m_objects.Add(entity);
                
            return OperationResult.OK;
        }

        void CreateLeaveNodes()
        {
            var sizeOver2 = Size / 2.0f;
            var sizeOver4 = Size / 4.0f;

            var nodeUfr = new Octree<T>(Bounds.center + new Vector3(sizeOver4, sizeOver4, -sizeOver4), sizeOver2, this);
            var nodeUfl = new Octree<T>(Bounds.center + new Vector3(-sizeOver4, sizeOver4, -sizeOver4), sizeOver2, this);
            var nodeUbr = new Octree<T>(Bounds.center + new Vector3(sizeOver4, sizeOver4, sizeOver4), sizeOver2, this);
            var nodeUbl = new Octree<T>(Bounds.center + new Vector3(-sizeOver4, sizeOver4, sizeOver4), sizeOver2, this);
            var nodeDfr = new Octree<T>(Bounds.center + new Vector3(sizeOver4, -sizeOver4, -sizeOver4), sizeOver2, this);
            var nodeDfl = new Octree<T>(Bounds.center + new Vector3(-sizeOver4, -sizeOver4, -sizeOver4), sizeOver2, this);
            var nodeDbr = new Octree<T>(Bounds.center + new Vector3(sizeOver4, -sizeOver4, sizeOver4), sizeOver2, this);
            var nodeDbl = new Octree<T>(Bounds.center + new Vector3(-sizeOver4, -sizeOver4, sizeOver4), sizeOver2, this);

            LeafNodes[(int)Position.UFR] = (nodeUfr);
            LeafNodes[(int)Position.UFL] = (nodeUfl);
            LeafNodes[(int)Position.UBR] = (nodeUbr);
            LeafNodes[(int)Position.UBL] = (nodeUbl);
            LeafNodes[(int)Position.DFR] = (nodeDfr);
            LeafNodes[(int)Position.DFL] = (nodeDfl);
            LeafNodes[(int)Position.DBR] = (nodeDbr);
            LeafNodes[(int)Position.DBL] = (nodeDbl);

            HasLeaves = true;
        }

        void DistributeObjectsToChildren()
        {
            if (!HasLeaves)
                return;
            if (m_objects.Count == 0)
                return;

            for (var i = m_objects.Count - 1; i >= 0; --i)
            {
                var octreeObject = m_objects[i];
                if (AddObjectToChildren(octreeObject) == OperationResult.OK)
                {
                    RemoveObjectAt(i);
                }
                // else it is left in the parent
            }
        }

        OperationResult AddObjectToChildren(T entity)
        {
            if (!HasLeaves)
                return OperationResult.Error;

            for (var index = 0; index < 8; index++)
            {
                entity.Get(out var entityBounds);
                if (LeafNodes[index].GetIntersectionType(entityBounds) != IntersectionType.Contains)
                    continue;
                LeafNodes[index].AddObject(entity);
                return OperationResult.OK;
            }
            return OperationResult.Error;
        }

        //bool IsRayIntersecting(Ray ray) => Bounds.IntersectRay(ray);
    }
}
