﻿using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Types
{
    // todo 4: User-Settings option to always spawn specific PrefabRefs in Prefab-Mode?
    /// <summary>
    /// Used to preview a prefab without saving/ nesting it in the GameObject or Parent-Prefab.
    /// Then in the game we can f.e. use this to spawn the prefab at the gameObject-position via pool.
    /// </summary>
    public class PrefabRef : MonoBehaviour, IPrefabSpawnMarker
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PrefabData m_prefabData;
        [SerializeField] bool m_parentAfterSpawn;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable ConvertToAutoProperty
        public GameObject Prefab => m_prefabData.Prefab;
        public bool ParentAfterSpawn => m_parentAfterSpawn;

        public bool ApplyScale;
        // ReSharper restore ConvertToAutoProperty

        GameObject m_instance;

        public GameObject GetInstance()
        {
            if (m_instance != null)
                return m_instance;

            var tr = transform;
            m_instance = Prefab.GetPooledInstance(tr.position, tr.rotation);
            if (m_instance.TryGetComponent(out PoolEntity poolEntity))
                poolEntity.AddDespawnAction(OnDespawnedEntity);

            if (ApplyScale)
                m_instance.transform.localScale = tr.lossyScale;
            if (ParentAfterSpawn)
                m_instance.transform.SetParent(transform, true);
            return m_instance;
        }

        void OnDespawnedEntity()
        {
            if (m_instance.TryGetComponent(out PoolEntity poolEntity))
                poolEntity.RemoveDespawnAction(OnDespawnedEntity);
            m_instance = null;
        }

        public void OnValidate()
        {
            //var c = gameObject.GetComponents<Component>().Length;
            //if (c > 2)
            //    Debug.LogError($"Please don't add Components to GameObjects with component {nameof(PrefabRef)}! " +
            //                   $"It is intended only for previewing one prefab! Found in {gameObject}");

            if (transform.childCount <= 0)
                return;

            for (var i = 0; i < transform.childCount; i++)
            {
                if ((transform.GetChild(i).gameObject.hideFlags & HideFlags.DontSave) != HideFlags.DontSave)
                    Debug.LogError($"Please don't add child-objects under GameObjects with component {nameof(PrefabRef)}! " +
                                   $"It is intended only for editor-preview! Found in {gameObject}");
            }
        }

    }

#if UNITY_EDITOR
    [CustomEditor(typeof(PrefabRef))]
    public class NestedPrefabRefEditor : Editor
    {
        // ReSharper disable once InconsistentNaming
        new PrefabRef target => base.target as PrefabRef;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (target.transform.childCount <= 0)
            {
                if (!GUILayout.Button("Spawn Preview"))
                    return;

                var transform = target.transform;
                var go = Instantiate(target.Prefab, transform.position, transform.rotation);
                if (target.ApplyScale)
                    go.transform.localScale = transform.lossyScale;

                go.transform.SetParent(transform, !target.ParentAfterSpawn);
                go.transform.position = transform.position;
                go.transform.rotation = transform.rotation;

                go.name = "__Unsaved_Preview_" + target.Prefab.name;
                go.hideFlags = HideFlags.DontSave;
            }
            else if (GUILayout.Button("Remove Preview"))
            {
                for (var i = target.transform.childCount - 1; i >= 0; i--)
                {
                    target.transform.GetChild(i).gameObject.DestroyEx();
                }
            }
        }

    }

#endif
}