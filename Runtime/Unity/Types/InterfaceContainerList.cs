﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;

namespace Core.Unity.Types
{
    public class InterfaceContainerList<TContainer, TInterface> : IList<TInterface>
        where TInterface : class
        where TContainer : InterfaceContainer<TInterface>, new()
    {
        readonly Func<IList<TContainer>> m_getList;

        public InterfaceContainerList(Func<IList<TContainer>> getList)
        {
            m_getList = getList;
        }

        public int Count => m_getList().Count;

        public bool IsReadOnly => m_getList().IsReadOnly;

        public IEnumerator<TInterface> GetEnumerator()
        {
            return m_getList().Select(c => c?.Result).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        // ReSharper disable once MethodNameNotMeaningful
        public void Add(TInterface item)
        {
            m_getList().Add(new TContainer { Result = item });
        }

        public void Clear()
        {
            m_getList().Clear();
        }

        public bool Contains(TInterface item)
        {
            return IndexOf(m_getList(), item) >= 0;
        }

        public void CopyTo(TInterface[] array, int arrayIndex)
        {
            var list = m_getList().Select(c => c?.Result).ToList();
            Array.Copy(list.ToArray(), 0, array, arrayIndex, list.Count);
        }

        public bool Remove(TInterface item)
        {
            var list = m_getList();
            var indexToRemove = IndexOf(list, item);
            if(indexToRemove < 0)
            {
                return false;
            }

            list.RemoveAt(indexToRemove);
            return true;
        }

        public int IndexOf(TInterface item)
        {
            return IndexOf(m_getList(), item);
        }

        public void Insert(int index, TInterface item)
        {
            m_getList().Insert(index, new TContainer { Result = item });
        }

        public void RemoveAt(int index)
        {
            m_getList().RemoveAt(index);
        }

        public TInterface this[int index]
        {
            get
            {
                var container = m_getList()[index];
                return container?.Result;
            }
            set => m_getList()[index] = new TContainer { Result = value };
        }

        static int IndexOf(IList<TContainer> list, TInterface item)
        {
            return list.FirstIndexWhere(c =>
            {
                if(item == null)
                {
                    return c?.Result == null;
                }
                return c != null && c.Result == item;
            });
        }
    }
}