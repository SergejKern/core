using System.Collections.Generic;
using Core.Extensions;
using UnityEngine; 

namespace Core.Unity.Types
{
    [CreateAssetMenu(menuName = "Game/PrefabAsset")]
    public class PrefabAsset : ScriptableObject
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Tooltip("Prefab is randomly picked from list")]
        [SerializeField] List<GameObject> m_prefabList;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public GameObject Prefab => m_prefabList.IsNullOrEmpty() 
            ? null 
            : m_prefabList.RandomItem();
    }
}
