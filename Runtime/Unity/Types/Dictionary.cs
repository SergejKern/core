﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Unity.Types
{
    [Serializable]
    public class SerializedDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField] List<KeyValue> m_list = new List<KeyValue>();

        Dictionary<TKey, TValue> m_dictionary = new Dictionary<TKey, TValue>();
        bool m_keyCollision;

        [Serializable]
        struct KeyValue
        {
            public TKey Key;
            public TValue Value;
            public KeyValue(TKey key, TValue value)
            {
                Key = key;
                Value = value;
            }
        }

        public TValue this[TKey key]
        {
            get => m_dictionary[key];
            set => m_dictionary[key] = value;
        }

        public ICollection<TKey> Keys => m_dictionary.Keys;
        public ICollection<TValue> Values => m_dictionary.Values;
        public int Count => m_dictionary.Count;
        public bool IsReadOnly { get; set; }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => m_dictionary.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => m_dictionary.GetEnumerator();

        public void OnBeforeSerialize()
        {
            foreach (var pair in m_dictionary)
            {
                var kv = new KeyValue(pair.Key, pair.Value);
                if (!m_list.Contains(kv)) 
                    m_list.Add(kv);
            }
        }

        public void OnAfterDeserialize()
        {
            m_keyCollision = false;
            m_dictionary = new Dictionary<TKey, TValue>(m_list.Count);
            foreach (var pair in m_list)
            {
                if (pair.Key != null && !ContainsKey(pair.Key))
                    Add(pair.Key, pair.Value);
                else if (!m_keyCollision) 
                    m_keyCollision = true;
            }
        }

        public void Add(TKey key, TValue value) => m_dictionary.Add(key, value);

        public void Add(KeyValuePair<TKey, TValue> item) => m_dictionary.Add(item.Key, item.Value);

        public void Clear()
        {
            m_dictionary.Clear();
            m_list.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item) =>
            m_dictionary.TryGetValue(item.Key, out var value) 
            && EqualityComparer<TValue>.Default.Equals(value, item.Value);

        public bool ContainsKey(TKey key) => m_dictionary.ContainsKey(key);

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentException("The array cannot be null.");
            if (arrayIndex < 0)
                throw new ArgumentException("The starting array index cannot be negative.");
            if (array.Length - arrayIndex < m_dictionary.Count)
                throw new ArgumentException("The destination array has fewer elements than the collection.");

            foreach (var pair in m_dictionary)
            {
                array[arrayIndex] = pair;
                arrayIndex++;
            }
        }

        public bool Remove(TKey key)
        {
            if (!m_dictionary.Remove(key)) 
                return false;
            var item = new KeyValue();
            foreach (var element in m_list)
            {
                if (!EqualityComparer<TKey>.Default.Equals(element.Key, key)) 
                    continue;
                item = element;
                break;
            }
            m_list.Remove(item);
            return true;

        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (!m_dictionary.TryGetValue(item.Key, out var value)) 
                return false;
            var valueMatch = EqualityComparer<TValue>.Default.Equals(value, item.Value);
            if (!valueMatch) 
                return false;
            m_dictionary.Remove(item.Key);
            return true;
        }

        public bool TryGetValue(TKey key, out TValue value) => m_dictionary.TryGetValue(key, out value);
    }
}