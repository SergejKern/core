using System;
using System.IO;
using UnityEngine;

namespace Core.Unity.Types
{
    /// <summary>
    /// Serialize System.Type.
    /// Does not come with an editor to select the type. Use ClassTypeReference instead.
    /// todo 1: check if we can get rid of this when we have ClassTypeReference
    /// ! careful: https://docs.unity3d.com/Manual/ScriptingRestrictions.html
    /// </summary>
    [Serializable]
    public class SerializableType : ISerializationCallbackReceiver
    {
        public byte[] Data;
        public Type Type;

        public SerializableType(Type aType)
        {
            Type = aType;
        }

        public void OnBeforeSerialize()
        {
            using (var stream = new MemoryStream())
            using (var w = new BinaryWriter(stream))
            {
                Write(w, Type);
                Data = stream.ToArray();
            }
        }
        public void OnAfterDeserialize()
        {
            using (var stream = new MemoryStream(Data))
            using (var r = new BinaryReader(stream))
            {
                Type = Read(r);
            }
        }
        public static Type Read(BinaryReader aReader)
        {
            var paramCount = aReader.ReadByte();
            if (paramCount == 0xFF)
                return null;

            var typeName = aReader.ReadString();
            var type = Type.GetType(typeName);

            if (type == null)
                throw new Exception("Can't find type; '" + typeName + "'");

            if (!type.IsGenericTypeDefinition || paramCount <= 0)
                return type;

            var p = new Type[paramCount];
            for (var i = 0; i < paramCount; i++)
                p[i] = Read(aReader);

            type = type.MakeGenericType(p);
            return type;
        }
        public static void Write(BinaryWriter aWriter, Type aType)
        {
            if (aType == null)
            {
                aWriter.Write((byte) 0xFF);
                return;
            }

            if (aType.IsGenericType)
            {
                var t = aType.GetGenericTypeDefinition();
                var p = aType.GetGenericArguments();
                aWriter.Write((byte) p.Length);
                aWriter.Write(t.AssemblyQualifiedName ?? throw new InvalidOperationException());
                foreach (var arg in p)
                    Write(aWriter, arg);

                return;
            }

            aWriter.Write((byte) 0);
            aWriter.Write(aType.AssemblyQualifiedName ?? throw new InvalidOperationException());
        }

        public override bool Equals(object obj)
        {
            var temp = obj as SerializableType;
            return (object) temp != null && Equals(temp);
        }

        // ReSharper disable NonReadonlyMemberInGetHashCode
        public override int GetHashCode() => Type != null ? Type.GetHashCode() : 0;
        // ReSharper restore NonReadonlyMemberInGetHashCode

        public bool Equals(SerializableType obj) => obj.Type == Type; //return m_AssemblyQualifiedName.Equals(_Object.m_AssemblyQualifiedName);

        public static bool operator ==(SerializableType a, SerializableType b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            return a.Equals(b);
        }

        public static bool operator !=(SerializableType a, SerializableType b)
        {
            return !(a == b);
        }
    }
}