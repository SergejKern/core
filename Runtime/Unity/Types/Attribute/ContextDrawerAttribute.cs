using Core.Unity.Attributes;

namespace Core.Unity.Types.Attribute
{
    public struct ContextDrawerAttributeData
    {
        // todo autoSelectNames = new[] { drawer.AutoSelectName, property.name, property.displayName};
        public string AutoSelectName;
        public ContextDrawerAttribute.Condition ContextSelf;
        public ContextDrawerAttribute.Condition ShowContext;
        public bool ShowOnlyIfUnlinked;
        public bool HideLabel;
        public bool BreakAfterLabel;

        public static ContextDrawerAttributeData Default => new ContextDrawerAttributeData()
        {
            AutoSelectName = ContextDrawerAttribute.AutoSelectNameDefault,
            ContextSelf = ContextDrawerAttribute.ContextSelfDefault,
            ShowContext = ContextDrawerAttribute.ShowContextSelectorDefault,
            ShowOnlyIfUnlinked = ContextDrawerAttribute.ShowOnlyIfUnlinkedDefault,
            HideLabel = ContextDrawerAttribute.HideLabelDefault,
            BreakAfterLabel = ContextDrawerAttribute.BreakAfterLabelDefault,
        };
    }

    public class ContextDrawerAttribute : MultiPropertyAttribute
    {
        public enum Condition
        {
            Never,
            OnlyWhenNull,
            Always
        }

        public const string AutoSelectNameDefault = "";
        public const Condition ContextSelfDefault = Condition.OnlyWhenNull;
        public const Condition ShowContextSelectorDefault = Condition.Always;
        public const bool HideLabelDefault = false;
        public const bool ShowOnlyIfUnlinkedDefault = false;
        public const bool BreakAfterLabelDefault = false;

        public readonly ContextDrawerAttributeData Data;

        public ContextDrawerAttribute(string autoSelectName = AutoSelectNameDefault,
            Condition setToSelf = ContextSelfDefault, 
            Condition showContext = ShowContextSelectorDefault,
            bool hideLabel = HideLabelDefault,
            bool showOnlyIfUnlinked = ShowOnlyIfUnlinkedDefault,
            bool breakAfterLabel = BreakAfterLabelDefault)
        {
            Data = new ContextDrawerAttributeData()
            {
                AutoSelectName = autoSelectName,
                ContextSelf = setToSelf,
                ShowContext = showContext,
                HideLabel = hideLabel,
                ShowOnlyIfUnlinked = showOnlyIfUnlinked,
                BreakAfterLabel = breakAfterLabel
            };
        }
    }
}