using System.Collections.Generic;
using UnityEditor;

namespace Core.Unity.Interface
{
    public interface IEnumGenerator
    {
        string EnumName { get; }
        DefaultAsset Folder { get; }
        int NameSpaceStart { get; }
        IEnumerable<string> EnumEntries { get; }
        string OutputFile { get; }
        string NameSpace { get; }
    }
}