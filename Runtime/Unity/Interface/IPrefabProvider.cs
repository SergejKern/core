using UnityEngine;

namespace Core.Unity.Interface
{
    public interface IPrefabProvider
    {
        GameObject Prefab { get; }
    }

    /// <summary>
    /// Use this on GameObjects that are used as AttachmentPoints to spawn prefabs
    /// ! Don't put this on a prefab-root-object
    /// </summary>
    public interface IPrefabSpawnMarker : IPrefabProvider
    {
        bool ParentAfterSpawn { get; }
        GameObject GetInstance();
    }

    /// <summary>
    /// Used for instances of prefabs
    /// </summary>
    public interface IPrefabInstanceMarker : IPrefabProvider
    {
        new GameObject Prefab { get; set; }
    }
}
