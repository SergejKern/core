using System;
using Core.Interface;
using Core.Unity.Types;
using UnityEngine;

namespace Core.Unity.Interface
{
    [Serializable]
    public class RefIBounds: InterfaceContainer<IProvider<Bounds>> { }
}
