﻿using Core.Interface;
using UnityEngine;

namespace Core.Unity.Interface
{
    public interface ITarget : IProvider<Transform>, IProvider<GameObject>
    {
        bool IsStatic { get; }
        bool IsTargetingPossible { get; }
    }

    public static class TargetOp
    {
        public static GameObject GameObject(this ITarget target)
        {
            if (target == null)
                return null;
            target.Get(out GameObject go);
            return go;
        }
        public static Transform Transform(this ITarget target)
        {
            if (target == null)
                return null;
            target.Get(out Transform tr);
            return tr;
        }
    }
}