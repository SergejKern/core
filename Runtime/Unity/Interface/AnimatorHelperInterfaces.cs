﻿using UnityEngine;

namespace Core.Unity.Interface
{
    public interface IAnimatorController
    {
        RuntimeAnimatorController Controller { get; }
    }
    public interface IAnimatorState : IAnimatorController
    {
        string StateName { get; }
        int StateHash { get; }
    }
    public interface IAnimatorParameter : IAnimatorController
    {
        string ParameterName { get; }
        int ParameterHash { get; }
    }
    public interface IAnimatorVariableRef : IAnimatorParameter
    {
        Animator Animator { get; set; }
        void ApplyOverride();
        void Sync();
    }
}