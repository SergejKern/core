﻿using System;
using UnityEngine;

using Core.Unity.Types;
using Core.Unity.Extensions;

namespace Core.Unity.Interface
{
    public interface ITransitionActivation
    {
        InactiveFollowup InactiveFollowup { get; set; }
        bool IsActiveOrActivating { get; }
        bool IsInTransition { get; }

        void InitActive(bool active);
        void SetActive(bool active);
    }

    public static class TransitionActivationExt
    {
        public static bool FollowUp(this ITransitionActivation activation)
        {
            var activationMono = activation as MonoBehaviour;
            if (activationMono == null)
                return false;
            var gameObject = activationMono.gameObject;
            switch (activation.InactiveFollowup)
            {
                case InactiveFollowup.None: break;
                case InactiveFollowup.Despawn:
                    gameObject.TryDespawn(true);
                    return true;
                case InactiveFollowup.DespawnOrDestroy:
                    gameObject.TryDespawnOrDestroy(true);
                    return true;
                default: throw new ArgumentOutOfRangeException();
            }

            return false;
        }
    }

    [Serializable]
    public class RefITransitionActivation : InterfaceContainer<ITransitionActivation> { }

    public enum InactiveFollowup
    {
        None, Despawn, DespawnOrDestroy
    }
}
