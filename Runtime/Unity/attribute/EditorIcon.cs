﻿using System;

namespace Core.Unity.Attributes
{
    /// <summary>
    /// Specify a texture name from your assets which you want to be assigned as an icon to the MonoScript.
    /// see EditorIconPostProcessor
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EditorIcon : Attribute
    {
        public EditorIcon(string name) => Name = name;

        public string Name { get; set; }
    }
}
