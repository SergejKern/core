﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
    public class ShowIfAttribute : MultiPropertyAttribute
    {
        readonly string m_conditionBoolean;

        public ShowIfAttribute(string conditionBoolean) => 
            m_conditionBoolean = conditionBoolean;

#if UNITY_EDITOR
        public override bool EDITOR_IsVisible(SerializedProperty property)
        {
            var showResult = EDITOR_GetConditionAttributeResult(property);
            return showResult;
        }

        bool EDITOR_GetConditionAttributeResult(SerializedProperty property)
        {
            var enabled = true;
            //var propertyPath = property.propertyPath.Split('.')[0]; 
            //var conditionPath = propertyPath.Replace(property.name, condHAtt.ConditionBoolean); 
            var sourcePropertyValue = property.serializedObject.FindProperty(m_conditionBoolean);

            if (sourcePropertyValue != null)
                enabled = sourcePropertyValue.boolValue;
            else
                Debug.LogWarning("No matching boolean found for ConditionAttribute in object: " + m_conditionBoolean);

            return enabled;
        }
#endif
    }
}
