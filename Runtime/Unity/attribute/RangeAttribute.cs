﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Core.Unity.Attributes
{
    public class RangeAttribute : MultiPropertyAttribute
    {
        readonly float m_min;
        readonly float m_max;
        public RangeAttribute(float min, float max)
        {
            m_min = min;
            m_max = max;
        }

#if UNITY_EDITOR
        public override void EDITOR_OnGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.Float)
                EditorGUI.Slider(position, property, m_min, m_max, label);
            else
                EditorGUI.LabelField(position, label.text, "Use Range with float or int.");
        }
#endif

    }
}