﻿using System;
using System.Linq;
using Core.Extensions;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class SelectAssetByName : MultiPropertyAttribute
    {
        readonly string m_name;

        public SelectAssetByName(string name) => m_name = name;

#if UNITY_EDITOR
        public override bool EDITOR_IsVisible(SerializedProperty property)
        {
            //Debug.Log($"EDITOR_IsVisible type: {property.type} propType: {property.propertyType}")
            if (property.objectReferenceValue == null) 
                TryAssign(property);
            if (property.objectReferenceValue == null)
                return true;

            return !string.Equals(property.objectReferenceValue.name, m_name);
        }

        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            base.EDITOR_OnPreGUI(ref position, property, label);

            var miniButtonRect = new Rect(position.x, position.y, 20f, 20f);
            position.x += 20f;
            position.width -= 20f;

            if (GUI.Button(miniButtonRect, "A"))
                TryAssign(property);
        }

        void TryAssign(SerializedProperty prop)
        {
            var propType = prop.type;
            Debug.Assert(propType.Contains("PPtr<$")); // expected format = PPtr<$type>
            var assetType = prop.type.Substring(6, propType.Length-7); 

            var filter = $"{m_name} t:{assetType}";
            var assets = AssetDatabase.FindAssets($"{m_name} t:{assetType}");
            //Debug.Log($"Found {assets.Length} with filter: {filter}");

            if (assets.IsNullOrEmpty())
                return;
            var path = AssetDatabase.GUIDToAssetPath(assets[0]);
            var objs = AssetDatabase.LoadAllAssetsAtPath(path);
            var selectedObj = objs.FirstOrDefault(o => string.Equals(o.name, m_name));

            //Debug.Log($"newRef path {path} name {selectedObj?.name}");

            if (prop.objectReferenceValue == selectedObj)
                return;

            //Debug.Log($"Assigning {selectedObj?.name}");
            prop.objectReferenceValue = selectedObj;

            prop.serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(prop.serializedObject.targetObject);
        }
#endif
    }
}
