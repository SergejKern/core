﻿using System;

namespace Core.Unity.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property |
                    AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
    public class ShowPropertyIfAttribute : MultiPropertyAttribute
    {
        public readonly string ConditionalSourceField;

        public ShowPropertyIfAttribute(string conditionalSourceField) 
            => ConditionalSourceField = conditionalSourceField;
    }
}
