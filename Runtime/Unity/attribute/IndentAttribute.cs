﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class IndentAttribute : MultiPropertyAttribute
    {
#if UNITY_EDITOR
        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label) => EditorGUI.indentLevel++;
        public override void EDITOR_OnPostGUI(ref Rect position, SerializedProperty property) => EditorGUI.indentLevel--;
#endif
    }
}