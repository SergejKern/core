﻿using System.Text.RegularExpressions;
using Core.Extensions;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    public class EnumStringSyncAttribute : MultiPropertyAttribute
    {
        public EnumStringSyncAttribute(string name) => StringPropertyName = name;
        public readonly string StringPropertyName;

        int m_enumIdx;

#if UNITY_EDITOR
        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.enumNames.IsNullOrEmpty())
                return;
            m_enumIdx = property.enumValueIndex;
        }

        public override void EDITOR_OnPostGUI(ref Rect position, SerializedProperty property)
        {
            if (property.enumNames.IsNullOrEmpty())
                return;
            if (property.enumValueIndex == m_enumIdx)
                return;
            UpdateString(property);
        }

        void UpdateString(SerializedProperty property)
        {
            var propertyPath = property.propertyPath;
            var stringPath = Regex.Replace(propertyPath, $"\\b{property.name}\\b",
                StringPropertyName);
            //propertyPath.Replace(property.name, enumStringSyncAttribute.StringPropertyName);

            var prop = property.serializedObject.FindProperty(stringPath);
            prop.stringValue = property.enumNames[property.enumValueIndex];
        }
#endif


    }
}