﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace Core.Unity.Attributes
{
    public class InformationAttribute : MultiPropertyAttribute 
	{
        // determines the space after the help box, the space before the text box, and the width of the help box icon
        const int k_spaceBeforeProp = 5;
        const int k_spaceAfterProp = 10;
        const int k_iconWidth = 55;
        const string k_helpPrefKey = "ShowHelpInInspectors";

#if UNITY_EDITOR
        public readonly string Message;
		public readonly UnityEditor.MessageType Type;
		public readonly bool MessageAfterProperty;

		public InformationAttribute(string message, MessageType type, bool messageAfterProperty)
		{
			Message = message;
            Type = (UnityEditor.MessageType) type;
            MessageAfterProperty = messageAfterProperty;
		}

        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            if (!HelpEnabled())
                return;
            if (MessageAfterProperty)
                return;

            EditorStyles.helpBox.richText = true;
            var helpPosition = position;

            helpPosition.height = DetermineTextBoxHeight(Message);
            var spacing = helpPosition.height + k_spaceBeforeProp;
            position.y += spacing;
            position.height -= spacing;

            EditorGUI.HelpBox(helpPosition, Message, Type);
        }

        public override void EDITOR_OnPostGUI(ref Rect position, SerializedProperty property)
        {
            if (!HelpEnabled())
                return;
            if (!MessageAfterProperty)
                return;

            EditorStyles.helpBox.richText = true;
            var helpPosition = position;

            helpPosition.y += k_spaceAfterProp;
            helpPosition.height = DetermineTextBoxHeight(Message);
            var spacing = helpPosition.height + k_spaceAfterProp;

            position.y += spacing;
            position.height -= spacing;

            EditorGUI.HelpBox(position, Message, Type);
        }

        /// <summary>
	    /// Returns the complete height of the whole block (property + help text)
	    /// </summary>
        public override float EDITOR_GetPropertyHeight(SerializedProperty property, GUIContent label, float height)
        {
            if (HelpEnabled())
                return height + DetermineTextBoxHeight(Message) 
                              + k_spaceAfterProp 
                              + k_spaceBeforeProp;

            return height;
        }

        /// <summary>
        /// Checks the editor prefs to see if help is enabled or not
        /// </summary>
        /// <returns><c>true</c>, if enabled was helped, <c>false</c> otherwise.</returns>
        static bool HelpEnabled()
        {
            return EditorPrefs.HasKey(k_helpPrefKey) &&
                   EditorPrefs.GetBool(k_helpPrefKey);
        }

        /// <summary>
        /// Determines the height of the text-box.
        /// </summary>
        /// <returns>The text-box height.</returns>
        /// <param name="message">Message.</param>
        static float DetermineTextBoxHeight(string message)
        {
            var style = new GUIStyle(EditorStyles.helpBox) { richText = true };

            var newHeight = style.CalcHeight(new GUIContent(message), EditorGUIUtility.currentViewWidth - k_iconWidth);
            return newHeight;
        }
#else
		public InformationAttribute(string message, MessageType type, bool messageAfterProperty)
		{

		}
#endif
    }
    // double definition for being able to use attribute outside of editor-context
    public enum MessageType
    {
        None,
        Info,
        Warning,
        Error,
    }
}
