#if MATHEMATICS
using Unity.Mathematics;
#endif

using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Unity.Extensions
{
    /// <summary>
    /// Conversion from one Vector-type to another
    /// </summary>
    public static class VectorExtensions
    {
        /// <summary> converts Vector3 to Vector3Int </summary>
        public static Vector3Int Vector3Int(this Vector3 vec) => new Vector3Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y), Mathf.RoundToInt(vec.z));
        /// <summary> converts Vector3 to Vector2 </summary>
        public static Vector2 Vector2(this Vector3 vec) => new Vector2(vec.x, vec.z);
        /// <summary> converts Vector3 to Vector2Int </summary>
        public static Vector2Int Vector2Int(this Vector3 vec) => new Vector2Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.z));

        /// <summary> converts Vector3Int to Vector3 </summary>
        public static Vector3 Vector3(this Vector3Int vec) => new Vector3(vec.x, vec.y, vec.z);
        /// <summary> converts Vector3Int to Vector2 </summary>
        public static Vector2 Vector2(this Vector3Int vec) => new Vector2(vec.x, vec.z);
        /// <summary> converts Vector3Int to Vector2Int </summary>
        public static Vector2Int Vector2Int(this Vector3Int vec) => new Vector2Int(vec.x, vec.z);

        /// <summary> converts Vector2 to Vector3Int </summary>
        public static Vector3Int Vector3Int(this Vector2 vec, int y = 0) => new Vector3Int(Mathf.RoundToInt(vec.x), y, Mathf.RoundToInt(vec.y));
        /// <summary> converts Vector2 to Vector3 </summary>
        public static Vector3 Vector3(this Vector2 vec, float y = 0) => new Vector3(vec.x, y, vec.y);
        /// <summary> converts Vector2 to Vector2Int </summary>
        public static Vector2Int Vector2Int(this Vector2 vec) => new Vector2Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y));

        /// <summary> converts Vector3Int to Vector3 </summary>
        public static Vector3 Vector3(this Vector2Int vec, float y = 0) => new Vector3(vec.x, y, vec.y);
        /// <summary> converts Vector3Int to Vector2 </summary>
        public static Vector2 Vector2(this Vector2Int vec) => new Vector2(vec.x, vec.y);
        /// <summary> converts Vector3Int to Vector2Int </summary>
        public static Vector3Int Vector3Int(this Vector2Int vec, int y = 0) => new Vector3Int(vec.x, y, vec.y);

#if MATHEMATICS
        /// <summary> converts Vector3 to float3 </summary>
        public static float3 Float3(this Vector3 vec) => new float3(vec.x, vec.y, vec.z);
        /// <summary> converts Vector3 to float2 </summary>
        public static float2 Float2(this Vector3 vec) => new float2(vec.x, vec.z);
        /// <summary> converts Vector3 to int3 </summary>
        public static int3 Int3(this Vector3 vec) => new int3(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y), Mathf.RoundToInt(vec.z));
        /// <summary> converts Vector3 to int2 </summary>
        public static int2 Int2(this Vector3 vec) => new int2(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.z));

        /// <summary> converts Vector3Int to float3 </summary>
        public static float3 Float3(this Vector3Int vec) => new float3(vec.x, vec.y, vec.z);
        /// <summary> converts Vector3Int to float2 </summary>
        public static float2 Float2(this Vector3Int vec) => new float2(vec.x, vec.z);
        /// <summary> converts Vector3Int to int3 </summary>
        public static int3 Int3(this Vector3Int vec) => new int3(vec.x, vec.y, vec.z);
        /// <summary> converts Vector3Int to int2 </summary>
        public static int2 Int2(this Vector3Int vec) => new int2(vec.x, vec.z);

        /// <summary> converts Vector2 to float3 </summary>
        public static float3 Float3(this Vector2 vec, float y = 0) => new float3(vec.x, y, vec.y);
        /// <summary> converts Vector2 to float2 </summary>
        public static float2 Float2(this Vector2 vec) => new float2(vec.x, vec.y);
        /// <summary> converts Vector2 to int3 </summary>
        public static int3 Int3(this Vector2 vec, int y = 0) => new int3(Mathf.RoundToInt(vec.x), y, Mathf.RoundToInt(vec.y));
        /// <summary> converts Vector2 to int2 </summary>
        public static int2 Int2(this Vector2 vec) => new int2(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y));

        /// <summary> converts Vector3Int to float3 </summary>
        public static float3 Float3(this Vector2Int vec, float y = 0) => new float3(vec.x, y, vec.y);
        /// <summary> converts Vector3Int to float2 </summary>
        public static float2 Float2(this Vector2Int vec) => new float2(vec.x, vec.y);
        /// <summary> converts Vector3Int to int3 </summary>
        public static int3 Int3(this Vector2Int vec, int y = 0) => new int3(vec.x, y, vec.y);
        /// <summary> converts Vector3Int to int2 </summary>
        public static int2 Int2(this Vector2Int vec) => new int2(vec.x, vec.y);

        /// <summary> converts float3 to Vector3Int </summary>
        public static Vector3Int Vector3Int(this float3 vec) => new Vector3Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y), Mathf.RoundToInt(vec.z));
        /// <summary> converts float3 to Vector2 </summary>
        public static Vector2 Vector2(this float3 vec) => new Vector2(vec.x, vec.z);
        /// <summary> converts float3 to Vector2Int </summary>
        public static Vector2Int Vector2Int(this float3 vec) => new Vector2Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.z));
        /// <summary> converts float3 to Vector3 </summary>
        public static Vector3 Vector3(this float3 vec) => new Vector3(vec.x, vec.y, vec.z);
        /// <summary> converts float3 to float2 </summary>
        public static float2 Float2(this float3 vec) => new float2(vec.x, vec.z);
        /// <summary> converts float3 to int3 </summary>
        public static int3 Int3(this float3 vec) => new int3(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y), Mathf.RoundToInt(vec.z));
        /// <summary> converts float3 to int2 </summary>
        public static int2 Int2(this float3 vec) => new int2(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.z));

        /// <summary> converts float2 to Vector3Int </summary>
        public static Vector3Int Vector3Int(this float2 vec, int y = 0) => new Vector3Int(Mathf.RoundToInt(vec.x), y, Mathf.RoundToInt(vec.y));
        /// <summary> converts float2 to Vector3 </summary>
        public static Vector3 Vector3(this float2 vec, float y = 0) => new Vector3(vec.x, y, vec.y);
        /// <summary> converts float2 to Vector2Int </summary>
        public static Vector2Int Vector2Int(this float2 vec) => new Vector2Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y));
        /// <summary> converts float2 to float3 </summary>
        public static float3 Float3(this float2 vec, float y = 0) => new float3(vec.x, y, vec.y);
        /// <summary> converts float2 to Vector2 </summary>
        public static Vector2 Vector2(this float2 vec) => new float2(vec.x, vec.y);
        /// <summary> converts float2 to int3 </summary>
        public static int3 Int3(this float2 vec, int y = 0) => new int3(Mathf.RoundToInt(vec.x), y, Mathf.RoundToInt(vec.y));
        /// <summary> converts float2 to int2 </summary>
        public static int2 Int2(this float2 vec) => new int2(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y));

        /// <summary> converts int3 to Vector3 </summary>
        public static Vector3 Vector3(this int3 vec) => new Vector3(vec.x, vec.y, vec.z);
        /// <summary> converts int3 to Vector2 </summary>
        public static Vector2 Vector2(this int3 vec) => new Vector2(vec.x, vec.z);
        /// <summary> converts int3 to Vector2Int </summary>
        public static Vector2Int Vector2Int(this int3 vec) => new Vector2Int(vec.x, vec.z);
        /// <summary> converts int3 to float3 </summary>
        public static float3 Float3(this int3 vec) => new float3(vec.x, vec.y, vec.z);
        /// <summary> converts int3 to float2 </summary>
        public static float2 Float2(this int3 vec) => new float2(vec.x, vec.z);
        /// <summary> converts int3 to Vector3Int </summary>
        public static Vector3Int Vector3Int(this int3 vec) => new Vector3Int(vec.x, vec.y, vec.z);
        /// <summary> converts int3 to int2 </summary>
        public static int2 Int2(this int3 vec) => new int2(vec.x, vec.z);

        /// <summary> converts int2 to Vector3 </summary>
        public static Vector3 Vector3(this int2 vec, float y = 0) => new Vector3(vec.x, y, vec.y);
        /// <summary> converts int2 to Vector2 </summary>
        public static Vector2 Vector2(this int2 vec) => new Vector2(vec.x, vec.y);
        /// <summary> converts int2 to Vector2Int </summary>
        public static Vector3Int Vector3Int(this int2 vec, int y = 0) => new Vector3Int(vec.x, y, vec.y);
        /// <summary> converts int2 to float3 </summary>
        public static float3 Float3(this int2 vec, float y = 0) => new float3(vec.x, y, vec.y);
        /// <summary> converts int2 to float2 </summary>
        public static float2 Float2(this int2 vec) => new float2(vec.x, vec.y);
        /// <summary> converts int2 to int3 </summary>
        public static int3 Int3(this int2 vec, int y = 0) => new int3(vec.x, y, vec.y);
        /// <summary> converts int2 to Vector2Int </summary>
        public static Vector2Int Vector2Int(this int2 vec) => new Vector2Int(vec.x, vec.y);

        /// <summary> returns int3(1,1,1) </summary>
        public static int3 Int3One { get; } = new int3(1, 1, 1);
        /// <summary> returns int2(1,1) </summary>
        public static int2 Int2One { get; } = new int2(1, 1);

        /// <summary> returns float3(1,1,1) </summary>
        public static float3 Float3One { get; } = new float3(1, 1, 1);
        /// <summary> returns float2(1,1) </summary>
        public static float2 Float2One { get; } = new float2(1, 1);

        /// <summary> Shorthand for writing int3(0, 0, 1) </summary>
        public static int3 Int3Forward { get; } = new int3(0, 0, 1);
        /// <summary> Shorthand for writing int3(0, 0, -1) </summary>
        public static int3 Int3Back { get; } = new int3(0, 0, -1);
        /// <summary> Shorthand for writing int3(1, 0, 0) </summary>
        public static int3 Int3Right { get; } = new int3(1, 0, 0);
        /// <summary> Shorthand for writing int3(0, -1, 0) </summary>
        public static int3 Int3Down { get; } = new int3(0, -1, 0);
        /// <summary> Shorthand for writing int3(-1, 0, 0) </summary>
        public static int3 Int3Left { get; } = new int3(-1, 0, 0);
        /// <summary> Shorthand for writing int3(0, 1, 0) </summary>
        public static int3 Int3Up { get; } = new int3(0, 1, 0);

        /// <summary> Shorthand for writing float3(0, 0, 1) </summary>
        public static float3 Float3Forward { get; } = new float3(0, 0, 1);
        /// <summary> Shorthand for writing float3(0, 0, -1) </summary>
        public static float3 Float3Back { get; } = new float3(0, 0, -1);
        /// <summary> Shorthand for writing float3(1, 0, 0) </summary>
        public static float3 Float3Right { get; } = new float3(1, 0, 0);
        /// <summary> Shorthand for writing float3(0, -1, 0) </summary>
        public static float3 Float3Down { get; } = new float3(0, -1, 0);
        /// <summary> Shorthand for writing float3(-1, 0, 0) </summary>
        public static float3 Float3Left { get; } = new float3(-1, 0, 0);
        /// <summary> Shorthand for writing float3(0, 1, 0) </summary>
        public static float3 Float3Up { get; } = new float3(0, 1, 0);

        /// <summary> Shorthand for writing int2(0, 0, 1) </summary>
        public static int2 Int2Forward { get; } = new int2(0, 1);
        /// <summary> Shorthand for writing int2(0, 0, -1) </summary>
        public static int2 Int2Back { get; } = new int2(0, -1);
        /// <summary> Shorthand for writing int2(1, 0, 0) </summary>
        public static int2 Int2Right { get; } = new int2(1, 0);
        /// <summary> Shorthand for writing int2(-1, 0, 0) </summary>
        public static int2 Int2Left { get; } = new int2(-1, 0);
        ///// <summary> Shorthand for writing int2(0, -1, 0) </summary>
        //public static int2 Int2Down { get; } = new int2(0, -1);
        ///// <summary> Shorthand for writing int2(0, 1, 0) </summary>
        //public static int2 Int2Up { get; } = new int2(0, 1);

        /// <summary> Shorthand for writing float2(0, 0, 1) </summary>
        public static float2 Float2Forward { get; } = new float2(0, 1);
        /// <summary> Shorthand for writing float2(0, 0, -1) </summary>
        public static float2 Float2Back { get; } = new float2(0, -1);
        /// <summary> Shorthand for writing float2(1, 0, 0) </summary>
        public static float2 Float2Right { get; } = new float2(1, 0);
        /// <summary> Shorthand for writing float2(-1, 0, 0) </summary>
        public static float2 Float2Left { get; } = new float2(-1, 0);
        ///// <summary> Shorthand for writing float2(0, -1, 0) </summary>
        //public static float2 Float2Down { get; } = new float2(0, -1);
        ///// <summary> Shorthand for writing float2(0, 1, 0) </summary>
        //public static float2 Float2Up { get; } = new float2(0, 1);
#endif
        public static Vector3 Direction(this Vector3 from, Vector3 to) => (to - @from).normalized;
        public static Vector3 Flattened(this Vector3 vector) => new Vector3(vector.x, 0f, vector.z);
        public static float DistanceFlat(this Vector3 origin, Vector3 destination) => UnityEngine.Vector3.Distance(origin.Flattened(), destination.Flattened());

        /// <summary>
        /// Returns a random Vector2 from 2 defined Vector2.
        /// </summary>
        public static Vector2 RandomVector2(Vector2 min, Vector2 max) =>
            new Vector2(Random.Range(min.x, max.x),
                Random.Range(min.y, max.y));

        /// <summary>
        /// Returns a random Vector3 from 2 defined Vector3.
        /// </summary>
        public static Vector3 RandomVector3(Vector3 min, Vector3 max) =>
            new Vector3(Random.Range(min.x, max.x),
                Random.Range(min.y, max.y),
                Random.Range(min.z, max.z));

        /// <summary>
        /// Rotates a point around the given pivot.
        /// </summary>
        /// <returns>The new point position.</returns>
        /// <param name="point">The point to rotate.</param>
        /// <param name="pivot">The pivot's position.</param>
        /// <param name="angle">The angle as a Vector3.</param>
        public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angle)
            => RotatePointAroundPivot(point, pivot, Quaternion.Euler(angle));

        /// <summary>
        /// Rotates a point around the given pivot.
        /// </summary>
        /// <returns>The new point position.</returns>
        /// <param name="point">The point to rotate.</param>
        /// <param name="pivot">The pivot's position.</param>
        /// <param name="quaternion">The angle as a Vector3.</param>
        public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion quaternion)
        {
            // we get point direction from the point to the pivot
            var direction = point - pivot;
            // we rotate the direction
            direction = quaternion * direction;
            // we determine the rotated point's position
            point = direction + pivot;
            return point;
        }

        /// <summary>
        /// Rotates a vector2 by angleInDegrees
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="angleInDegrees"></param>
        /// <returns></returns>
        public static Vector2 Rotate(this Vector2 vector, float angleInDegrees)
        {
            // todo 1: is this needed?
            // ? if (Math.Abs(angleInDegrees) < float.Epsilon)
            // ?   return vector;

            var sin = Mathf.Sin(angleInDegrees * Mathf.Deg2Rad);
            var cos = Mathf.Cos(angleInDegrees * Mathf.Deg2Rad);
            var tx = vector.x;
            var ty = vector.y;
            vector.x = (cos * tx) - (sin * ty);
            vector.y = (sin * tx) + (cos * ty);
            return vector;
        }

        /// <summary>
        /// Rounds all components of a Vector3.
        /// </summary>
        /// <returns>The vector3.</returns>
        /// <param name="vector">Vector.</param>
        public static Vector3 Round(this Vector3 vector) => 
            new Vector3(Mathf.Round(vector.x), Mathf.Round(vector.y), Mathf.Round(vector.z));

        /// <summary>
        /// Computes and returns the angle between two vectors, on a 360� scale
        /// </summary>
        /// <returns>The <see cref="System.Single"/>.</returns>
        /// <param name="vectorA">Vector a.</param>
        /// <param name="vectorB">Vector b.</param>
        public static float AngleBetween(Vector2 vectorA, Vector2 vectorB)
        {
            var angle = UnityEngine.Vector2.Angle(vectorA, vectorB);
            var cross = UnityEngine.Vector3.Cross(vectorA, vectorB);

            if (cross.z > 0)
                angle = 360 - angle;

            return angle;
        }

        /// <summary>
        /// Returns the distance between a point and a line.
        /// </summary>
        /// <returns>The between point and line.</returns>
        /// <param name="point">Point.</param>
        /// <param name="lineStart">Line start.</param>
        /// <param name="lineEnd">Line end.</param>
        public static float DistanceBetweenPointAndLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
        {
            return UnityEngine.Vector3.Magnitude(ProjectPointOnLine(point, lineStart, lineEnd) - point);
        }

        /// <summary>
        /// Projects a point on a line (perpendicularly) and returns the projected point.
        /// </summary>
        /// <returns>The point on line.</returns>
        /// <param name="point">Point.</param>
        /// <param name="lineStart">Line start.</param>
        /// <param name="lineEnd">Line end.</param>
        public static Vector3 ProjectPointOnLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
        {
            var rhs = point - lineStart;
            var vector2 = lineEnd - lineStart;
            var magnitude = vector2.magnitude;
            var lhs = vector2;
            if (magnitude > 1E-06f)
                lhs /= magnitude;

            var num2 = Mathf.Clamp(UnityEngine.Vector3.Dot(lhs, rhs), 0f, magnitude);
            return (lineStart + lhs * num2);
        }

    }
}