﻿using System.Collections.Generic;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class GameObjectExtensions 
    {
        public static void DestroyComponents<T>(this GameObject parent) where T : Component
        {
            var comps = parent.GetComponentsInChildren<T>(true);
            foreach (var c in comps)
            {
                Object.Destroy(c);
            }
        }

        public static void SetComponentsEnabled<T>(this GameObject parent, bool enabled = true) where T : MonoBehaviour
        {
            var comps = parent.GetComponentsInChildren<T>(enabled);
            foreach (var c in comps)
            {
                c.enabled = enabled;
            }
        }

        public static void SetColliderEnabled<T>(this GameObject parent, bool enabled = true) where T : Collider
        {
            var comps = parent.GetComponentsInChildren<T>(enabled);
            foreach (var c in comps)
            {
                c.enabled = enabled;
            }
        }

        public static void SetActive(this IEnumerable<GameObject> go, bool active = true)
        {
            foreach (var g in go)
                g.SetActive(active);
        }

        public static void SetActiveWithTransition(this GameObject go, bool active, out bool isInTransition)
        {
            isInTransition = false;
            if (go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
            {
                transitionActivation.SetActive(active);
                isInTransition = transitionActivation.IsInTransition;
            }
            else go.SetActive(active);
        }

        public static void SetActiveWithTransition(this GameObject go, bool active = true)
        {
            if (go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
                transitionActivation.SetActive(active);
            else
                go.SetActive(active);
        }

        public static bool IsActiveOrActivating(this GameObject go)
        {
            return go.TryGetComponent<ITransitionActivation>(out var transitionActivation)
                ? transitionActivation.IsActiveOrActivating
                : go.activeSelf;
        }
        public static bool IsInTransition(this GameObject go) => 
            go.TryGetComponent<ITransitionActivation>(out var transitionActivation)
            && transitionActivation.IsInTransition;

        public static void SetActiveWithTransition(this GameObject[] go, bool active = true)
        {
            foreach (var g in go)
                g.SetActiveWithTransition(active);
        }

        // ReSharper disable once FlagArgument
        public static void TryDespawn(this GameObject go, bool ignoreTransition = false)
        {
            if (go == null)
                return;
            if (!ignoreTransition && go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
            {
                transitionActivation.InactiveFollowup = InactiveFollowup.Despawn;
                transitionActivation.SetActive(false);
            }
            else if (go.TryGetComponent<PoolEntity>(out var poolEntity))
                poolEntity.Despawn();
        }

        // ReSharper disable once FlagArgument
        public static void TryDespawnOrDestroy(this GameObject go, bool ignoreTransition = false)
        {
            if (go == null)
                return;
            if (!ignoreTransition && go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
            {
                transitionActivation.InactiveFollowup = InactiveFollowup.DespawnOrDestroy;
                transitionActivation.SetActive(false);
            }
            else if (go.TryGetComponent<PoolEntity>(out var poolEntity))
                poolEntity.Despawn();
            else go.DestroyEx();
        }

        // untested methods:
        //static readonly List<Component> m_componentCache = new List<Component>();
        ///// <summary>
        ///// Grabs a component without allocating memory uselessly
        ///// </summary>
        ///// <param name="this"></param>
        ///// <param name="componentType"></param>
        ///// <returns></returns>
        //public static Component GetComponentNoAlloc(this GameObject @this, System.Type componentType)
        //{
        //    @this.GetComponents(componentType, m_componentCache);
        //    var component = m_componentCache.Count > 0? m_componentCache[0] : null;
        //    m_componentCache.Clear();
        //    return component;
        //}
        ///// <summary>
        ///// Grabs a component without allocating memory uselessly
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="this"></param>
        ///// <returns></returns>
        //public static T GetComponentNoAlloc<T>(this GameObject @this) where T : Component
        //{
        //    @this.GetComponents(typeof(T), m_componentCache);
        //    var component = m_componentCache.Count > 0? m_componentCache[0] : null;
        //    m_componentCache.Clear();
        //    return component as T;
        //}
    }
}
