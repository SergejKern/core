using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class ParticleSystemExtension
    {
        public static void Enable(this ParticleSystem particleSystem, bool enabled)
        {
            var emission = particleSystem.emission;
            emission.enabled = enabled;
        }

        public static float GetEmissionRate_OT(this ParticleSystem particleSystem) => 
            particleSystem.emission.rateOverTime.constantMax;

        public static void SetEmissionRate_OT(this ParticleSystem particleSystem, float emissionRate)
        {
            var emission = particleSystem.emission;
            var rate = emission.rateOverTime;
            rate.constantMax = emissionRate;
            emission.rateOverTime = rate;
        }
    }
}