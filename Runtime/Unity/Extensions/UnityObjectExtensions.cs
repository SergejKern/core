﻿using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class UnityObjectExtensions
    {
        /// <summary>
        /// Destroys object correctly depending on whether we are in the editor or in the game
        /// </summary>
        public static void DestroyEx(this Object obj)
        {
            var editor = Application.isEditor && !Application.isPlaying;
            if (editor)
                Object.DestroyImmediate(obj, true);
            else
                Object.Destroy(obj);
        }
    }
}
