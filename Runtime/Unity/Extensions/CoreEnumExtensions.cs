using System;
using Core.Extensions;
using Core.Types;
using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class CoreEnumExtensions
    {
        // helper for better serialization with enums
        public static void OnBeforeSerializeEnum<T>(out string toSerializeString, T enumVal) where T : struct
            => toSerializeString = enumVal.ToString();

        public static void OnAfterDeserializeEnum<T>(ref T enumVal, string serializedString) where T : struct
        {
            if (Enum.TryParse<T>(serializedString, out var key))
                enumVal = key;
        }

        /// <summary> converts Orientation to Vector3 </summary>
        public static Vector3 ToVector3(this Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.Up: return Vector3.up;
                case Orientation.Left: return Vector3.left;
                case Orientation.Right: return Vector3.right;
                case Orientation.Forward: return Vector3.forward;
                case Orientation.Back: return Vector3.back;
                case Orientation.Down: return Vector3.down;
                case Orientation.Invalid: break;
                default: throw new ArgumentOutOfRangeException(nameof(orientation), orientation, null);
            }
            return Vector3.zero;
        }
        /// <summary> converts Vector3 to Cardinals </summary>
        public static Vector3 ToVector3(this Cardinals card) => card.ToOrientation().ToVector3();
    }
}