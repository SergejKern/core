﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class TextureExtensions
    {
        struct FloodFillData
        {
            public Vector2Int Dimension;
            public Color32[] Pixels;
            public Color32 OrigColor;
            public Color32 FillColor;
            public Queue<Vector2Int> Queue;
        }

        public static void FloodFillArea(this Texture2D tex, int xPos, int yPos, Color32 fillColor)
        {
            var pixels = tex.GetPixels32();
            var dim = new Vector2Int(tex.width, tex.height);

            var floodFillData = new FloodFillData()
            {
                Dimension = dim,
                Pixels = pixels,
                OrigColor = pixels[xPos + yPos * tex.width],
                FillColor = fillColor,
                Queue = new Queue<Vector2Int>()
            };

            floodFillData.Queue.Enqueue(new Vector2Int(xPos, yPos));

            while (floodFillData.Queue.Count > 0)
            {
                var point = floodFillData.Queue.Dequeue();
                for (var i = point.x; i < dim.x; i++)
                {
                    if (FloodFillAreaInnerLoop(floodFillData, i, point))
                        break;
                }
                for (var j = point.x - 1; j >= 0; j--)
                {
                    if (FloodFillAreaInnerLoop(floodFillData, j, point))
                        break;
                }
            }
            tex.SetPixels32(pixels);
        }

        static bool FloodFillAreaInnerLoop(FloodFillData dat, int i, Vector2Int point)
        {
            var dim = dat.Dimension;
            Color color2 = dat.Pixels[i + point.y * dim.x];
            if (color2 != dat.OrigColor || color2 == dat.FillColor)
                return true;

            dat.Pixels[i + point.y * dim.x] = dat.FillColor;
            if (point.y + 1 < dim.y)
                FloodFillContinue(dat, dat.Pixels[i + point.y * dim.x + dim.x],
                    new Vector2Int(i, point.y + 1));
            if (point.y - 1 >= 0)
                FloodFillContinue(dat, dat.Pixels[i + point.y * dim.x - dim.x],
                    new Vector2Int(i, point.y - 1));
            return false;
        }

        static void FloodFillContinue(FloodFillData dat, Color pixel, Vector2Int point)
        {
            if (pixel != dat.OrigColor || pixel == dat.FillColor)
                return;
            dat.Queue.Enqueue(point);
        }
    }
}
