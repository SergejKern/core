using System.Collections.Generic;
using System.Linq;

#if CORE_PLAYMAKER
using HutongGames.PlayMaker;
#else
using Core.Interface;
#endif

namespace Core.Unity.Operations
{
    public static class FsmOperations
    {
        public struct EventsAndStatesExtracted
        {
            public List<string> StateNames;
            public List<string> EventNames;
        }

#if CORE_PLAYMAKER
        public static EventsAndStatesExtracted Extract(Fsm fsm)
        {
            var eventNames = fsm.Events.Select(e => e.Name).ToList();
            var stateNames = fsm.States.Select(e => e.Name).ToList();

            return new EventsAndStatesExtracted() { EventNames = eventNames, StateNames = stateNames };
        }
#else
        public static EventsAndStatesExtracted Extract(IFSM fsm)
        {
            var eventNames = fsm.Events.Select(e => e.Name).ToList();
            var stateNames = fsm.States.Select(e => e.Name).ToList();

            return new EventsAndStatesExtracted() { EventNames = eventNames, StateNames = stateNames };
        }
#endif
    }
}
