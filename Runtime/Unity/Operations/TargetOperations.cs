﻿using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Interface;
using UnityEngine;

namespace Core.Unity.Operations
{
    public static class TargetOperations
    {
        public static GameObject GetNearestGameObject(Transform transform, IEnumerable<GameObject> targets)
        {
            GameObject nearestTarget = null;
            var nearestSqrMagnitude = float.MaxValue;

            foreach (var t in targets)
            {
                var targetTr = t.transform;
                var pos = targetTr.position;
                var sqrMagnitudeAim = Vector3.SqrMagnitude(pos - transform.position);
                if (sqrMagnitudeAim > nearestSqrMagnitude)
                    continue;

                nearestSqrMagnitude = sqrMagnitudeAim;
                nearestTarget = t;
            }

            return nearestTarget;
        }
        public static T GetNearest<T>(Transform transform, IEnumerable<T> targets) where T : Component
        {
            T nearestTarget = null;
            var nearestSqrMagnitude = float.MaxValue;

            foreach (var t in targets)
            {
                var targetTr = t.transform;
                var pos = targetTr.position;
                var sqrMagnitudeAim = Vector3.SqrMagnitude(pos - transform.position);
                if (sqrMagnitudeAim > nearestSqrMagnitude)
                    continue;

                nearestSqrMagnitude = sqrMagnitudeAim;
                nearestTarget = t;
            }

            return nearestTarget;
        }
        public static T GetNearest2<T>(Transform transform, IEnumerable<T> targets) where T : IProvider<Transform>
        {
            T nearestTarget = default;
            var nearestSqrMagnitude = float.MaxValue;

            foreach (var t in targets)
            {
                t.Get(out var targetTr);
                var pos = targetTr.position;
                var sqrMagnitudeAim = Vector3.SqrMagnitude(pos - transform.position);
                if (sqrMagnitudeAim > nearestSqrMagnitude)
                    continue;

                nearestSqrMagnitude = sqrMagnitudeAim;
                nearestTarget = t;
            }

            return nearestTarget;
        }
    }
}
