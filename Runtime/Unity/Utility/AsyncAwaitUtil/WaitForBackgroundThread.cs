using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Core.Unity.Utility.AsyncAwaitUtil
{
    public class WaitForBackgroundThread
    {
        public ConfiguredTaskAwaitable.ConfiguredTaskAwaiter GetAwaiter() => 
            Task.Run(() => {}).ConfigureAwait(false).GetAwaiter();
    }
}
