using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using Core.Unity.Utility.AsyncAwaitUtil.Internal;
using UnityEngine;
using UnityEngine.Networking;

// We could just add a generic GetAwaiter to YieldInstruction and CustomYieldInstruction
// but instead we add specific methods to each derived class to allow for return values
// that make the most sense for the specific instruction type
namespace Core.Unity.Utility.AsyncAwaitUtil
{
    // ReSharper disable once InconsistentNaming
    public static class IEnumeratorAwaitExtensions
    {
        public static SimpleCoroutineAwaiter GetAwaiter(this WaitForSeconds instruction) => GetAwaiterReturnVoid(instruction);

        public static SimpleCoroutineAwaiter GetAwaiter(this WaitForUpdate instruction) => GetAwaiterReturnVoid(instruction);

        public static SimpleCoroutineAwaiter GetAwaiter(this WaitForEndOfFrame instruction) => GetAwaiterReturnVoid(instruction);

        public static SimpleCoroutineAwaiter GetAwaiter(this WaitForFixedUpdate instruction) => GetAwaiterReturnVoid(instruction);

        public static SimpleCoroutineAwaiter GetAwaiter(this WaitForSecondsRealtime instruction) => GetAwaiterReturnVoid(instruction);

        public static SimpleCoroutineAwaiter GetAwaiter(this WaitUntil instruction) => GetAwaiterReturnVoid(instruction);

        public static SimpleCoroutineAwaiter GetAwaiter(this WaitWhile instruction) => GetAwaiterReturnVoid(instruction);

        public static SimpleCoroutineAwaiter<AsyncOperation> GetAwaiter(this AsyncOperation instruction) => GetAwaiterReturnSelf(instruction);

        public static SimpleCoroutineAwaiter<UnityEngine.Object> GetAwaiter(this ResourceRequest instruction)
        {
            var awaiter = new SimpleCoroutineAwaiter<UnityEngine.Object>();
            RunOnUnityScheduler(() => AsyncCoroutineRunner.Instance.StartCoroutine(
                InstructionWrappers.ResourceRequest(awaiter, instruction)));
            return awaiter;
        }

        // Return itself so you can do things like (await new WWW(url)).bytes
        public static SimpleCoroutineAwaiter<UnityWebRequest> GetAwaiter(this UnityWebRequest instruction) => GetAwaiterReturnSelf(instruction);

        public static SimpleCoroutineAwaiter<AssetBundle> GetAwaiter(this AssetBundleCreateRequest instruction)
        {
            var awaiter = new SimpleCoroutineAwaiter<AssetBundle>();
            RunOnUnityScheduler(() => AsyncCoroutineRunner.Instance.StartCoroutine(
                InstructionWrappers.AssetBundleCreateRequest(awaiter, instruction)));
            return awaiter;
        }

        public static SimpleCoroutineAwaiter<UnityEngine.Object> GetAwaiter(this AssetBundleRequest instruction)
        {
            var awaiter = new SimpleCoroutineAwaiter<UnityEngine.Object>();
            RunOnUnityScheduler(() => AsyncCoroutineRunner.Instance.StartCoroutine(
                InstructionWrappers.AssetBundleRequest(awaiter, instruction)));
            return awaiter;
        }

        public static SimpleCoroutineAwaiter<T> GetAwaiter<T>(this IEnumerator<T> coroutine)
        {
            var awaiter = new SimpleCoroutineAwaiter<T>();
            RunOnUnityScheduler(() => AsyncCoroutineRunner.Instance.StartCoroutine(
                new CoroutineWrapper<T>(coroutine, awaiter).Run()));
            return awaiter;
        }

        public static SimpleCoroutineAwaiter<object> GetAwaiter(this IEnumerator coroutine)
        {
            var awaiter = new SimpleCoroutineAwaiter<object>();
            RunOnUnityScheduler(() => AsyncCoroutineRunner.Instance.StartCoroutine(
                new CoroutineWrapper<object>(coroutine, awaiter).Run()));
            return awaiter;
        }

        static SimpleCoroutineAwaiter GetAwaiterReturnVoid(object instruction)
        {
            var awaiter = new SimpleCoroutineAwaiter();
            RunOnUnityScheduler(() => AsyncCoroutineRunner.Instance.StartCoroutine(
                InstructionWrappers.ReturnVoid(awaiter, instruction)));
            return awaiter;
        }

        static SimpleCoroutineAwaiter<T> GetAwaiterReturnSelf<T>(T instruction)
        {
            var awaiter = new SimpleCoroutineAwaiter<T>();
            RunOnUnityScheduler(() => AsyncCoroutineRunner.Instance.StartCoroutine(
                InstructionWrappers.ReturnSelf(awaiter, instruction)));
            return awaiter;
        }

        static void RunOnUnityScheduler(Action action)
        {
            if (SynchronizationContext.Current == SyncContextUtil.UnitySynchronizationContext)
                action();
            else
                SyncContextUtil.UnitySynchronizationContext.Post(_ => action(), null);
        }

        // ReSharper disable once FlagArgument
        static void Assert(bool condition)
        {
            if (condition) return;
            throw new Exception("Assert hit in UnityAsyncUtil package!");
        }

        public class SimpleCoroutineAwaiter<T> : INotifyCompletion
        {
            Exception m_exception;
            Action m_continuation;
            T m_result;

            public bool IsCompleted { get; private set; }

            public T GetResult()
            {
                Assert(IsCompleted);

                if (m_exception != null)
                {
                    ExceptionDispatchInfo.Capture(m_exception).Throw();
                }

                return m_result;
            }

            public void Complete(T result, Exception e)
            {
                Assert(!IsCompleted);

                IsCompleted = true;
                m_exception = e;
                m_result = result;

                // Always trigger the continuation on the unity thread when awaiting on unity yield
                // instructions
                if (m_continuation != null)
                    RunOnUnityScheduler(m_continuation);
            }

            void INotifyCompletion.OnCompleted(Action continuation)
            {
                Assert(m_continuation == null);
                Assert(!IsCompleted);

                m_continuation = continuation;
            }
        }

        public class SimpleCoroutineAwaiter : INotifyCompletion
        {
            Exception m_exception;
            Action m_continuation;

            public bool IsCompleted { get; private set; }

            public void GetResult()
            {
                Assert(IsCompleted);

                if (m_exception != null)
                    ExceptionDispatchInfo.Capture(m_exception).Throw();
            }

            public void Complete(Exception e)
            {
                Assert(!IsCompleted);

                IsCompleted = true;
                m_exception = e;

                // Always trigger the continuation on the unity thread when awaiting on unity yield
                // instructions
                if (m_continuation != null)
                    RunOnUnityScheduler(m_continuation);
            }

            void INotifyCompletion.OnCompleted(Action continuation)
            {
                Assert(m_continuation == null);
                Assert(!IsCompleted);

                m_continuation = continuation;
            }
        }

        class CoroutineWrapper<T>
        {
            readonly SimpleCoroutineAwaiter<T> m_awaiter;
            readonly Stack<IEnumerator> m_processStack;

            public CoroutineWrapper(
                IEnumerator coroutine, SimpleCoroutineAwaiter<T> awaiter)
            {
                m_processStack = new Stack<IEnumerator>();
                m_processStack.Push(coroutine);
                m_awaiter = awaiter;
            }

            // ReSharper disable once MethodNameNotMeaningful
            public IEnumerator Run()
            {
                while (true)
                {
                    var topWorker = m_processStack.Peek();

                    bool isDone;

                    try
                    {
                        isDone = !topWorker.MoveNext();
                    }
                    catch (Exception e)
                    {
                        // The IEnumerators we have in the process stack do not tell us the
                        // actual names of the coroutine methods but it does tell us the objects
                        // that the IEnumerators are associated with, so we can at least try
                        // adding that to the exception output
                        var objectTrace = GenerateObjectTrace(m_processStack);

                        if (objectTrace.Any())
                        {
                            m_awaiter.Complete(
                                default(T), new Exception(
                                    GenerateObjectTraceMessage(objectTrace), e));
                        }
                        else
                        {
                            m_awaiter.Complete(default(T), e);
                        }

                        yield break;
                    }

                    if (isDone)
                    {
                        m_processStack.Pop();

                        if (m_processStack.Count == 0)
                        {
                            m_awaiter.Complete((T)topWorker.Current, null);
                            yield break;
                        }
                    }

                    // We could just yield return nested IEnumerator's here but we choose to do
                    // our own handling here so that we can catch exceptions in nested coroutines
                    // instead of just top level coroutine
                    if (topWorker.Current is IEnumerator item)
                    {
                        m_processStack.Push(item);
                    }
                    // Return the current value to the unity engine so it can handle things like
                    // WaitForSeconds, WaitToEndOfFrame, etc.
                    else yield return topWorker.Current;
                }
            }

            static string GenerateObjectTraceMessage(IEnumerable<Type> objTrace)
            {
                var result = new StringBuilder();

                foreach (var objType in objTrace)
                {
                    if (result.Length != 0)
                    {
                        result.Append(" -> ");
                    }

                    result.Append(objType);
                }

                result.AppendLine();
                return "Unity Coroutine Object Trace: " + result;
            }

            static List<Type> GenerateObjectTrace(IEnumerable<IEnumerator> enumerators)
            {
                var objTrace = new List<Type>();

                foreach (var enumerator in enumerators)
                {
                    // NOTE: This only works with scripting engine 4.6
                    // And could easily stop working with unity updates
                    var field = enumerator.GetType().GetField("$this", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

                    if (field == null)
                    {
                        continue;
                    }

                    var obj = field.GetValue(enumerator);

                    if (obj == null)
                    {
                        continue;
                    }

                    var objType = obj.GetType();

                    if (!objTrace.Any() || objType != objTrace.Last())
                    {
                        objTrace.Add(objType);
                    }
                }

                objTrace.Reverse();
                return objTrace;
            }
        }

        static class InstructionWrappers
        {
            public static IEnumerator ReturnVoid(
                SimpleCoroutineAwaiter awaiter, object instruction)
            {
                // For simple instructions we assume that they don't throw exceptions
                yield return instruction;
                awaiter.Complete(null);
            }

            public static IEnumerator AssetBundleCreateRequest(
                SimpleCoroutineAwaiter<AssetBundle> awaiter, AssetBundleCreateRequest instruction)
            {
                yield return instruction;
                awaiter.Complete(instruction.assetBundle, null);
            }

            public static IEnumerator ReturnSelf<T>(
                SimpleCoroutineAwaiter<T> awaiter, T instruction)
            {
                yield return instruction;
                awaiter.Complete(instruction, null);
            }

            public static IEnumerator AssetBundleRequest(
                SimpleCoroutineAwaiter<UnityEngine.Object> awaiter, AssetBundleRequest instruction)
            {
                yield return instruction;
                awaiter.Complete(instruction.asset, null);
            }

            public static IEnumerator ResourceRequest(
                SimpleCoroutineAwaiter<UnityEngine.Object> awaiter, ResourceRequest instruction)
            {
                yield return instruction;
                awaiter.Complete(instruction.asset, null);
            }
        }
    }
}
