using UnityEngine;

namespace Core.Unity.Utility.AsyncAwaitUtil.Internal
{
    public class AsyncCoroutineRunner : MonoBehaviour
    {
        static AsyncCoroutineRunner m_instance;

        public static AsyncCoroutineRunner Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new GameObject("AsyncCoroutineRunner")
                        .AddComponent<AsyncCoroutineRunner>();
                }

                return m_instance;
            }
        }

        void Awake()
        {
            // Don't show in scene hierarchy
            var go = gameObject;
            go.hideFlags = HideFlags.HideAndDontSave;

            DontDestroyOnLoad(go);
        }
    }
}
