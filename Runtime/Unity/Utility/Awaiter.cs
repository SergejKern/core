﻿#if ASYNC_OP

using System;
using System.Runtime.CompilerServices;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace Core.Unity.Utility
{
    public static class AsyncOperationAwaiterExtensions
    {
        public static AsyncOperationAwaiter GetAwaiter(this AsyncOperationHandle operation) => new AsyncOperationAwaiter(operation);

        public static AsyncOperationAwaiter<T> GetAwaiter<T>(this AsyncOperationHandle<T> operation) where T : Object => new AsyncOperationAwaiter<T>(operation);

        public readonly struct AsyncOperationAwaiter : INotifyCompletion
        {
            readonly AsyncOperationHandle m_operation;

            public AsyncOperationAwaiter(AsyncOperationHandle operation) => m_operation = operation;

            public bool IsCompleted => m_operation.Status != AsyncOperationStatus.None;

            public void OnCompleted(Action continuation) => m_operation.Completed += (op) => continuation?.Invoke();

            public object GetResult() => m_operation.Result;

        }

        public readonly struct AsyncOperationAwaiter<T> : INotifyCompletion where T : Object
        {
            readonly AsyncOperationHandle<T> m_operation;

            public AsyncOperationAwaiter(AsyncOperationHandle<T> operation) => m_operation = operation;

            public bool IsCompleted => m_operation.Status != AsyncOperationStatus.None;

            public void OnCompleted(Action continuation) => m_operation.Completed += (op) => continuation?.Invoke();
            public T GetResult() => m_operation.Result;
        }
    }
}

#endif