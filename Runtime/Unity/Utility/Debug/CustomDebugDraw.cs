using UnityEngine;

namespace Core.Unity.Utility.Debug
{
    /// <summary> Helper for debug-drawing other shapes </summary>
    public static class CustomDebugDraw
    {
        /// <summary> draws bounds box </summary>
        public static void DrawBounds(Bounds b, Color color, float duration = 0.0f)
        {
            var p0 = b.min;
            var p1 = p0 + (Vector3.right * b.size.x);
            var p2 = p0 + (Vector3.up * b.size.y);
            var p7 = b.max;
            var p3 = p7 - (Vector3.forward * b.size.z);
            var p4 = p0 + (Vector3.forward * b.size.z);
            var p5 = p7 - (Vector3.up * b.size.y);
            var p6 = p7 - (Vector3.right * b.size.x);


            UnityEngine.Debug.DrawLine(p0, p1, color, duration);
            UnityEngine.Debug.DrawLine(p0, p2, color, duration);
            UnityEngine.Debug.DrawLine(p3, p1, color, duration);
            UnityEngine.Debug.DrawLine(p3, p2, color, duration);

            UnityEngine.Debug.DrawLine(p4, p5, color, duration);
            UnityEngine.Debug.DrawLine(p4, p6, color, duration);
            UnityEngine.Debug.DrawLine(p7, p5, color, duration);
            UnityEngine.Debug.DrawLine(p7, p6, color, duration);

            UnityEngine.Debug.DrawLine(p0, p4, color, duration);
            UnityEngine.Debug.DrawLine(p1, p5, color, duration);
            UnityEngine.Debug.DrawLine(p2, p6, color, duration);
            UnityEngine.Debug.DrawLine(p3, p7, color, duration);
        }

        /// <summary> draws small gizmo at given position </summary>
        public static void DrawPosition(Vector3 v, Color color, float size =1.0f, float duration = 0.0f)
        {
            UnityEngine.Debug.DrawLine(v-Vector3.left * size, v+Vector3.left * size, color, duration);
            UnityEngine.Debug.DrawLine(v-Vector3.up * size, v+Vector3.up * size, color, duration);
            UnityEngine.Debug.DrawLine(v-Vector3.forward * size, v+Vector3.forward * size, color, duration);
        }

        public static void DrawText(Vector3 pos, string text, Color textColor, GUIStyle style = null)
        {
            var screenPos = CalculateScreenPosition(pos);
            DrawText(screenPos, text, textColor, style);
        }

        public static void DrawText(Vector2 screenPos, string text, Color textColor, GUIStyle style = null)
        {
            var rect = GetTextRect(screenPos, text, style);
            DrawText(rect, text, textColor, style);
        }

        public static void DrawText(Rect textRect, string text, Color textColor, GUIStyle style = null)
        {
            if (style == null)
                style = GUI.skin.label;
            using (new GUITools.ColorScope(textColor))
                GUI.Label(textRect, text, style);
        }
        public static Vector2 CalculateScreenPosition(Vector3 position)
        {
			//TODO 1: make it clear that it currently only works for in game
            var cam = Camera.main;
            return cam == null ? default : cam.WorldToScreenPoint(position);
        }

        public static Rect GetTextRect(Vector2 centerPosition, string text, GUIStyle style = null)
        {
            if (style == null)
                style = GUI.skin.label;
            var size = style.CalcSize(new GUIContent(text));

            var textRect = new Rect(centerPosition.x - (size.x / 2),
                Screen.height - centerPosition.y, size.x, size.y);
            return textRect;
        }


    }
}