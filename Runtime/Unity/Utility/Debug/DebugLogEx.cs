// todo 1: currently not happy with this solution

//#define LOGLEVEL_VV
//#define LOGLEVEL_V

// #define LOGLEVEL_I?
#define LOGLEVEL_D

using System.Collections.Generic;

namespace Core.Unity.Utility.Debug
{
    /// <summary> quickly enable or disable debugs via verbosity levels and tags </summary>
    public static class DEBUG {

        static readonly List<int> k_enabledDebugKeys = new List<int>();

        public static void EnableKey(string key)
        {
            if (!k_enabledDebugKeys.Contains(key.GetHashCode()))
                k_enabledDebugKeys.Add(key.GetHashCode());
        }

        public static void LogVV(string key, string message) {
#if LOGLEVEL_VV
        if (!enabledDebugKeys.Contains(key.GetHashCode()))
            return;
        Debug.Log($"VV {key}: {message}");
#endif
        }

        public static void LogV(string key, string message)
        {
#if LOGLEVEL_V
        if (!enabledDebugKeys.Contains(key.GetHashCode()))
            return;
        Debug.Log($"V {key}: {message}");
#endif
        }

        public static void LogD(string key, string message)
        {
#if LOGLEVEL_D
            if (!k_enabledDebugKeys.Contains(key.GetHashCode()))
                return;
            UnityEngine.Debug.Log($"D {key}: {message}");
#endif
        }
    }
}
