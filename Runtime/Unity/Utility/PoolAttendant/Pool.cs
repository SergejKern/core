using System;
using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Unity.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Core.Unity.Utility.PoolAttendant
{
    public class Pool
    {
        public const string SettingsLoadPath = "PoolSettings";
        const string k_poolName = "Pool";

        static Pool m_instance;

        readonly Dictionary<int, List<GameObject>> m_items = new Dictionary<int, List<GameObject>>();
        readonly Dictionary<int, Transform> m_parents = new Dictionary<int, Transform>();

        Transform m_container;
        PoolSettings m_settings;

        public static Pool Instance
        {
            get
            {
                if (m_instance != null) return m_instance;

                m_instance = new Pool
                {
                    m_container = new GameObject(k_poolName).transform,
                    m_settings = Resources.Load<PoolSettings>(SettingsLoadPath)
                };

                m_instance.m_container.position = new Vector3(0, 0, 0);

                if (m_instance.m_settings == null)
                    UnityEngine.Debug.LogWarning(
                        "PoolSettings not found at path \"Resources\\PoolSettings\", " +
                        "if you want to populate the pool with initial values you can create the" +
                        "settings with [\"Tools\\Pool\\Create PoolSettings\"]");

                if (m_instance.m_settings.AutoCreateDefaultItems)
                    m_instance.CreateDefaultItems();

                SceneManager.activeSceneChanged += m_instance.DeactivateAllObjects;

                return m_instance;
            }
        }

        public static GameObject Initialize()
        {
            return Instance.m_container.gameObject;
        }

        public void DelayInactivate(GameObject prefab, int delayFrames)
        {
            var idx = Array.FindIndex(m_settings.Items, a => a.Prefab == prefab);
            if (idx == -1)
            {
                var l = m_settings.Items.ToList();
                var item = new DefaultPoolItem() {DelayInactivateForFrames = delayFrames, Prefab = prefab, Size = 5};
                l.Add(item);
                m_settings.Items = l.ToArray();
                return;
            }

            m_settings.Items[idx].DelayInactivateForFrames = delayFrames;
        }

        public int GetDelayFrames(GameObject prefab)
        {
            var item = Array.Find(m_settings.Items, a => a.Prefab == prefab);
            return item.DelayInactivateForFrames;
        }

        public void CreateDefaultItems()
        {
            foreach (var item in m_settings.Items) 
                InstantiateDefaultItems(item);
        }

        void InstantiateDefaultItems(DefaultPoolItem item)
        {
            if (item.Prefab == null) 
                return;

            if (!m_parents.TryGetValue(item.Prefab.GetInstanceID(), out _))
                m_parents.Add(item.Prefab.GetInstanceID(), CreateParent(item.Prefab.name));

            InstantiateListAndGetFirst(item.Prefab, item.Size);
        }

        void DeactivateAllObjects(Scene currentScene, Scene newScene) => DeactivateAllObjects();
        public void DeactivateAllObjects()
        {
            foreach (var objects in m_items.Values)
            foreach (var obj in objects)
                if (obj != null)
                    obj.SetActive(false);
        }

        public void DestroyAllObjects()
        {
            foreach (var objects in m_items.Values)
            {
                foreach (var obj in objects)
                    if (obj != null)
                        obj.DestroyEx();
                objects.Clear();
            }
        }

        public void Clear() => m_items.Clear();

        GameObject InstantiateListAndGetFirst(GameObject prefab, int size = 1)
        {
            var exists = m_items.TryGetValue(prefab.GetInstanceID(), out var list);
            if (!exists)
                list = new List<GameObject>();

            for (var i = list.Count; i < size; i++)
            {
                var obj = CreateNew(prefab);
                obj.name = $"{prefab.name} {list.Count}";
                //UnityEngine.Debug.Log($"Created {obj.name}");
                list.Add(obj);
            }

            if (!exists)
                m_items.Add(prefab.GetInstanceID(), list);
            else 
                m_items[prefab.GetInstanceID()] = list;

            return list.FirstOrDefault();
        }

        GameObject CreateNew(GameObject prefab)
        {
            var id = prefab.GetInstanceID();

            if (!m_parents.TryGetValue(id, out var parent))
            {
                parent = CreateParent(prefab.name);
                m_parents.Add(prefab.GetInstanceID(), parent);
            }

            var obj = Object.Instantiate(
                prefab,
                prefab.transform.position,
                prefab.transform.rotation,
                parent);

            if (!obj.TryGetComponent(out PoolEntity pe))
                pe = obj.AddComponent<PoolEntity>();
            pe.Prefab = prefab;

            obj.SetActive(false);
            return obj;
        }

        Transform CreateParent(string name)
        {
            var childContainer = new GameObject(name + string.Empty + k_poolName).transform;
            childContainer.transform.SetParent(m_container);

            return childContainer;
        }

        // ReSharper disable once MethodNameNotMeaningful
        internal GameObject Get(GameObject prefab, Vector3 position, Quaternion rotation, Vector3 scale, string name = null)
        {
            GameObject obj;

            if (m_items.TryGetValue(prefab.GetInstanceID(), out var list))
            {
                obj = list.Find(o => !o.activeInHierarchy);

                if (obj == null)
                {
                    if (m_settings.DebugLogsEnabled)
                        UnityEngine.Debug.LogWarning($"Pool for {prefab} was too small ({list.Count})!");
                    obj = CreateNew(prefab);
                    obj.name = $"{prefab.name} {list.Count}";
                    //UnityEngine.Debug.Log($"Created {obj.name}");
                    list.Add(obj);
                } 
                //else 
                //    UnityEngine.Debug.Log($"using {obj.name}");
            }
            else
            {
                if (m_settings.DebugLogsEnabled)
                    UnityEngine.Debug.LogWarning($"No Pool for {prefab} setup!");
                obj = InstantiateListAndGetFirst(prefab);
            }

            obj.transform.position = position;
            obj.transform.rotation = rotation;
            obj.transform.localScale = scale;

            obj.SetActive(true);
            if (name != null)
                obj.name = name;

            var poolable = obj.GetComponents<IPoolable>();
            foreach (var p in poolable)
                p.OnSpawn();

            return obj;
        }

        public T Get<T>(GameObject prefab, Vector3 position, Quaternion rotation, Vector3 scale) where T : Component
        {
            return Get(prefab, position, rotation, scale).GetComponent<T>();
        }

        public void Reparent(GameObject obj, int instanceId)
        {
            if (!m_parents.TryGetValue(instanceId, out var parentTransform)) return;

            if (obj.transform.parent != null && obj.transform.parent.Equals(parentTransform)) return;

            obj.transform.SetParent(parentTransform, true);
        }

        public void Remove(GameObject gameObject, int instanceId)
        {
            if (!m_items.TryGetValue(instanceId, out var list)) return;

            list.Remove(gameObject);
        }
    }
}