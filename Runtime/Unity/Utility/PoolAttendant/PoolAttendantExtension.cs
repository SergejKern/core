using Core.Unity.Interface;
using UnityEngine;

namespace Core.Unity.Utility.PoolAttendant
{
    public static class PoolAttendantExtension
    {
        public static GameObject GetPooledInstance(this MonoBehaviour mono) => mono.gameObject.GetPooledInstance();

        public static GameObject GetPooledInstance(this MonoBehaviour mono, Vector3 position) => mono.gameObject.GetPooledInstance(position, Quaternion.identity);

        public static GameObject GetPooledInstance(this MonoBehaviour mono, Vector3 position, Quaternion rotation) => mono.gameObject.GetPooledInstance(position, rotation, Vector3.one);

        public static GameObject GetPooledInstance(this MonoBehaviour mono, Vector3 position, Quaternion rotation,
            Vector3 scale)
        {
            var marker = mono.GetComponent<IPrefabSpawnMarker>();
            return marker != null 
                ? marker.GetInstance() 
                : Pool.Instance.Get(mono.gameObject, position, rotation, scale);
        }

        public static T GetPooledInstance<T>(this MonoBehaviour mono) where T : Component => 
            mono.gameObject.GetPooledInstance<T>(Vector3.zero);

        public static T GetPooledInstance<T>(this MonoBehaviour mono, Vector3 position) where T : Component => 
            mono.gameObject.GetPooledInstance<T>(position, Quaternion.identity);

        public static T GetPooledInstance<T>(this MonoBehaviour mono, Vector3 position, Quaternion rotation)
            where T : Component =>
            mono.gameObject.GetPooledInstance<T>(position, rotation, Vector3.one);

        public static T GetPooledInstance<T>(this MonoBehaviour mono, Vector3 position, Quaternion rotation,
            Vector3 scale) where T : Component
        {
            var marker = mono.GetComponent<IPrefabSpawnMarker>();
            return Pool.Instance.Get<T>(marker == null ? mono.gameObject : marker.Prefab, position, rotation, scale);
        }

        public static GameObject GetPooledInstance(this GameObject prefab) => 
            prefab.GetPooledInstance(Vector3.zero);

        public static GameObject GetPooledInstance(this GameObject prefab, Vector3 position) => prefab.GetPooledInstance(position, Quaternion.identity);

        public static GameObject GetPooledInstance(this GameObject prefab, Vector3 position, Quaternion rotation, string name = null) => prefab.GetPooledInstance(position, rotation, prefab.transform.localScale, name);

        public static GameObject GetPooledInstance(this GameObject prefab, Vector3 position, Quaternion rotation,
            Vector3 scale, string name = null)
        {
            var marker = prefab.GetComponent<IPrefabSpawnMarker>();
            return marker != null 
                ? marker.GetInstance() 
                : Pool.Instance.Get(prefab, position, rotation, scale, name);
        }

        public static T GetPooledInstance<T>(this GameObject prefab) where T : Component => 
            prefab.GetPooledInstance<T>(Vector3.zero);

        public static T GetPooledInstance<T>(this GameObject prefab, Vector3 position) where T : Component => 
            prefab.GetPooledInstance<T>(Vector3.zero, Quaternion.identity);

        public static T GetPooledInstance<T>(this GameObject prefab, Vector3 position, Quaternion rotation)
            where T : Component =>
            prefab.GetPooledInstance<T>(Vector3.zero, Quaternion.identity, Vector3.one);

        public static T GetPooledInstance<T>(this GameObject prefab, Vector3 position, Quaternion rotation,
            Vector3 scale) where T : Component
        {
            var marker = prefab.GetComponent<IPrefabSpawnMarker>();
            return Pool.Instance.Get<T>(marker == null ? prefab : marker.Prefab, position, rotation, scale);
        }
    }
}