using System.Collections;
using Core.Interface;
using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Core.Unity.Utility.PoolAttendant
{
    public class PoolEntity : MonoBehaviour, IPrefabInstanceMarker
    {
        public bool DespawnOnDisable;
        readonly UnityEvent m_onDespawn = new UnityEvent();

        [SerializeField] GameObject m_prefab;
        // ReSharper disable once ConvertToAutoPropertyWithPrivateSetter, ConvertToAutoPropertyWhenPossible
        public GameObject Prefab
        {
            get => m_prefab;
            set => m_prefab = value;
        }

        void OnDisable()
        {
            if (!gameObject.activeInHierarchy && DespawnOnDisable)
                Invoke(nameof(Despawn), Time.deltaTime);
        }

        public void AddDespawnAction(UnityAction a) => m_onDespawn.AddListener(a);
        public void RemoveDespawnAction(UnityAction a) => m_onDespawn.RemoveListener(a);

        public void Despawn()
        {
            if (m_prefab == null)
                return;

            m_onDespawn?.Invoke();

            var poolable = gameObject.GetComponents<IPoolable>();
            foreach (var p in poolable)
                p.OnDespawn();
            //UnityEngine.Debug.Log($"Despawning {gameObject.name}");
            Pool.Instance.Reparent(gameObject, m_prefab.GetInstanceID());

            if (gameObject.activeInHierarchy)
            {
                //UnityEngine.Debug.Log($"OnDisable {gameObject.name} {Environment.StackTrace}");
                StartCoroutine(Inactivate());
            }   
            else gameObject.SetActive(false);
        }

        IEnumerator Inactivate()
        {
            var delayFrames = Pool.Instance.GetDelayFrames(m_prefab);
            for (var i = 0; i < delayFrames; i++)
                yield return null;

            yield return new WaitForEndOfFrame();

            gameObject.SetActive(false);
        }

        void OnDestroy()
        {
#if UNITY_EDITOR
            if (EditorApplication.isPlayingOrWillChangePlaymode) // <- !isStopping
                UnityEngine.Debug.LogWarning($"PoolEntity {gameObject} was destroyed! You should probably use Despawn instead!");
#endif
            Pool.Instance.Remove(gameObject, m_prefab.GetInstanceID());
        }
    }
}