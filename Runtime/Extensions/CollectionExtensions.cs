﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Types;
using UnityEngine;
using Random = System.Random;

namespace Core.Extensions
{
    public static class Arrays<T>
    {
        public static readonly T[] Empty = new T[0];
        public static readonly T[] Single = new T[1];
    }
    public static class Lists<T>
    {
        public static readonly List<T> Empty = new List<T>(0);
        public static readonly List<T> Single = new List<T>(1);
    }
    /// <summary> Various handy Extensions </summary>
    public static class CollectionExtensions
    {
        #region Array
        /// <summary> checks if something is contained in a list of variables 
        /// f.e. a.IsAny(b, c) can be used as shorthand for (a==b || a==c) </summary>
        public static bool IsAny<T>(this T @this, params T[] other) => other.Contains(@this);

        ///// <summary> checks whether index is in range of collection </summary>
        //public static bool IsIndexInRange(this Array arr, int index)
        //{
        //    if (arr == null)
        //        return false;
        //    return index >= 0 && index < arr.Length;
        //}
        public static void Remove<T>(ref T[] arr, T val)
        {
            var idx = Array.IndexOf(arr, val);
            if (idx != -1)
                RemoveAt(ref arr, idx);
        }
        public static void RemoveAt<T>(ref T[] arr, int idx)
        {
            var l = arr.ToList();
            l.RemoveAt(idx);
            arr = l.ToArray();
        }
        public static void Add<T>(ref T[] arr, T data)
        {
            var l = arr.ToList();
            l.Add(data);
            arr = l.ToArray();
        }

        // ReSharper disable once RedundantAssignment
        //public static void Clear<T>(ref T[] arr) => arr = new T[0];
        public static void RemoveAll<T>(ref T[] arr, Predicate<T> match)
        {
            var l = arr.ToList();
            l.RemoveAll(match);
            arr = l.ToArray();
        }
        #endregion

        #region Collection
        /// <summary> checks whether index is in range of collection </summary>
        public static bool IsIndexInRange<T>(this ICollection<T> collection, int index)
        {
            if (collection == null)
                return false;
            return index >= 0 && index < collection.Count;
        }
        #endregion

        #region Enumerable
        /// <summary> Checks IEnumerable for null and empty </summary>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            switch (enumerable)
            {
                case null:
                    return true;
                /* If this is a list, use the Count property for efficiency. 
                * The Count property is O(1) while IEnumerable.Count() is O(N). */
                case ICollection<T> collection:
                    return collection.Count < 1;
                default:
                    return !enumerable.Any();
            }
        }
        public static string Stringify(this IEnumerable<string> enumerable) => enumerable.Aggregate("", (current, st) => current + (st + ", "));

        /// <summary> Checks IEnumerable for null and empty </summary>
        public static bool IsInitializedWith<T>(this IEnumerable<T> enumerable, int count)
        {
            switch (enumerable)
            {
                case null:
                    return false;
                case ICollection<T> collection:
                    return collection.Count == count;
                default:
                    return enumerable.Count() == count;
            }
        }

        public static List<TContainer> ToContainerList<TContainer, TInterface>(this IEnumerable<TInterface> interfaces)
            where TInterface : class
            where TContainer : InterfaceContainer<TInterface>, new()
        {
            return interfaces?.Select(i => new TContainer { Result = i }).ToList();
        }

        #endregion

        #region List
        /// <summary> gets element with index from collection </summary>
        public static T GetAt<T>(this IList<T> collection, int index) => !collection.IsIndexInRange(index) ? default : collection[index];

        /// <summary>
        /// Shuffle the list in place using the Fisher-Yates method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(this IList<T> list)
        {
            var rng = new Random();
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = rng.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Return a random item from the list.
        /// Sampling with replacement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static T RandomItem<T>(this IList<T> list)
        {
            if (list.Count == 0) throw new IndexOutOfRangeException("Cannot select a random item from an empty list");
            return list[UnityEngine.Random.Range(0, list.Count)];
        }

        /// <summary>
        /// Removes a random item from the list, returning that item.
        /// Sampling without replacement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static T PopRandom<T>(this IList<T> list)
        {
            if (list.Count == 0) throw new IndexOutOfRangeException("Cannot remove a random item from an empty list");
            var index = UnityEngine.Random.Range(0, list.Count);
            var item = list[index];
            list.RemoveAt(index);
            return item;
        }

        public static int FirstIndexWhere<T>(this IList<T> list, Func<T, bool> predicate)
        {
            for (var i = 0; i < list.Count; ++i)
            {
                if (predicate(list[i]))
                {
                    return i;
                }
            }

            return -1;
        }

        public static List<TTo> ConvertList<TFrom, TTo>(this IEnumerable<TFrom> list) where TFrom : TTo => list.Cast<TTo>().ToList();
        public static TTo[] ConvertListToArray<TFrom, TTo>(this List<TFrom> list) where TFrom : TTo
        {
            var array = new TTo[list.Count];
            for (var i = 0; i < list.Count; i++) 
                array[i] = list[i];

            return array;
        }
        #endregion


    }
}
