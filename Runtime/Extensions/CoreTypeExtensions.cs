using System;
using Core.Types;

namespace Core.Extensions
{
    /// <summary> Various handy Extensions </summary>
    public static class CoreTypeExtensions
    {
        /// <summary> converts Cardinals to Orientation enum </summary>
        public static Orientation ToOrientation(this Cardinals c)
        {
            switch (c)
            {
                case Cardinals.North: return Orientation.Forward;
                case Cardinals.East: return Orientation.Right;
                case Cardinals.South: return Orientation.Back;
                case Cardinals.West: return Orientation.Left;
                case Cardinals.Invalid:
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(c), c, null);
            }
            return Orientation.Invalid;
        }
    }
}
