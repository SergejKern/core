using Core.Editor.Inspector;
using Core.Editor.Tools;
using Core.Types;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Settings
{
    [CustomEditor(typeof(CoreSettings))]
    internal class CoreSettingsInspector : BaseInspector<CoreSettingsEditor>{}

    internal class CoreSettingsEditor : BaseEditor<CoreSettings>
    {
        public override string Name => "Core Settings";

        bool m_changed;
        CodeGeneratorConfigEditor m_menuStringsEditor;

        public override void OnGUI(float width)
        {
            GUILayout.Label("Defines: ", EditorStyles.boldLabel);
            var defineChanges = false;

            foreach (var def in CoreSettings.K_PossibleDefinesForCore)
            {
                var prevToggled = Target.ExtraDefinesForCore.Contains(def);
                var toggled = EditorGUILayout.Toggle(def, prevToggled);
                if (prevToggled == toggled)
                    continue;

                if (toggled)
                    Target.ExtraDefinesForCore.Add(def);
                else
                    Target.ExtraDefinesForCore.Remove(def);

                defineChanges = true;
            }

            if (defineChanges)
                DefinesChanged();

            MenuStrings();
        }

        void MenuStrings()
        {
            if (Target.MenuStrings == null)
                CoreSettingsUpdater.LinkMenuStrings(Target);
            if (Target.MenuStrings == null)
                return;

            GUILayout.Label("Menu-Strings: ", EditorStyles.boldLabel);

            CreateEditor(Target.MenuStrings, ref m_menuStringsEditor);
            if (m_menuStringsEditor == null)
                return;
            var changeCheck = m_menuStringsEditor.ArgumentsOnlyGUI();
            m_changed |= (changeCheck == ChangeCheck.Changed);

            m_menuStringsEditor.GenerateButtonGUI("Update Menu");
        }

        void DefinesChanged()
        {
            EditorUtility.SetDirty(Target);
            AssetDatabase.SaveAssets();
            CoreSettingsUpdater.UpdateDefines(Target);
        }

        public override void Terminate()
        {
            base.Terminate();
            if (m_changed)
                CodeGeneratorConfigEditor.GenerateCode(Target.MenuStrings);
        }
    }
}

