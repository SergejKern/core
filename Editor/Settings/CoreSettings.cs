﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Core.Editor.Tools;
using Core.Extensions;
using Core.Types;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Settings
{
    internal class CoreSettings : ScriptableObject
    {
        const string k_configsPath = "Assets/Settings/";
        internal const string K_ConfigLinksPath = k_configsPath + "CoreSettings.asset";

        internal static readonly string K_FullConfigLinksPath = Path.GetFullPath(".") + Path.DirectorySeparatorChar + K_ConfigLinksPath;
        //todo 3: group defines like feature/ , custom-inspector/ or Menu/ then provide explanation/ info/ tooltip
        internal static readonly string[] K_PossibleDefinesForCore = {
            "CORE_PLAYMAKER",
            "CORE_ED_INSPECTOR_TRANSFORM",
            "CORE_ED_CREATE_ASSET_MENU_GEN_CONF",
            "CORE_ED_MENU_REGEN_GUIDS",
            "CORE_ED_MENU_CREATE_POOL",
            "CORE_ED_NAV_HISTORY",
            "CORE_ED_MENU_GUID_TOOL"
            //"FEATURE_XYZ"
        };

        public List<string> ExtraDefinesForCore = new List<string>();

        public CodeGeneratorConfig MenuStrings;
    }

    internal static class CoreSettingsUpdater
    {
        const string k_coreSettingsInit = "CoreSettingsInitialized";
        const string k_packagePath = "Packages/com.sergejkern.core/";
        const string k_packagePathRuntime = k_packagePath + "Runtime/";
        const string k_packagePathEditor = k_packagePath + "Editor/";
        const string k_packagePathEditorResources = k_packagePath + "Editor Resources/";

        const string k_menuStringsFile = "menustrings.asset";

        const string k_net35File = "mcs.rsp";
        const string k_net4XFile = "csc.rsp";

        [InitializeOnLoadMethod]
        static void InitCore()
        {
            if (SessionState.GetBool(k_coreSettingsInit, false))
                return;
            if (GetOrCreateCoreSettings(out var settings) != OperationResult.OK) 
                return;
            SessionState.SetBool(k_coreSettingsInit, true);
            UpdateDefines(settings);
            GenerateMenuStrings(settings);
        }

        static void EnsureCorrectScriptGuidInAsset()
        {
            var contents = File.ReadAllText(CoreSettings.K_FullConfigLinksPath);
            
            var scriptIdx = contents.IndexOf("m_Script: ", StringComparison.Ordinal);
            var guidIdx = scriptIdx + contents.Substring(scriptIdx).IndexOf("guid: ", StringComparison.Ordinal) + 6;
            var endGuidIdx = guidIdx + contents.Substring(guidIdx).IndexOf(",", StringComparison.Ordinal);
            var oldGuid = contents.Substring(guidIdx, endGuidIdx-guidIdx);
            var newGuid = AssetDatabase.AssetPathToGUID(k_packagePath + "Editor/Settings/CoreSettings.cs");
            if (string.Equals(oldGuid, newGuid, StringComparison.Ordinal))
                return;
            Debug.LogWarning($"CoreSettings with wrong guid: {oldGuid} Should be {newGuid}!");
            contents = contents.Replace(oldGuid, newGuid);
            File.WriteAllText(CoreSettings.K_FullConfigLinksPath, contents);
        }

        internal static OperationResult GetOrCreateCoreSettings(out CoreSettings settings)
        {
            if (File.Exists(CoreSettings.K_FullConfigLinksPath))
            {
                EnsureCorrectScriptGuidInAsset();
                AssetDatabase.Refresh();
                Debug.Log($"CoreSettings exist!");

                //repeat
                settings = AssetDatabase.LoadAssetAtPath<CoreSettings>(CoreSettings.K_ConfigLinksPath);
                if (settings != null)
                {
                    Debug.Log($"CoreSettings found!");
                    return OperationResult.OK;
                }

                EditorApplication.delayCall += InitCore;
                return OperationResult.Error;
            }

            Debug.LogWarning("No CORE-Settings found... creating");
            settings = ScriptableObject.CreateInstance<CoreSettings>();

            var dir = Path.GetDirectoryName(CoreSettings.K_ConfigLinksPath);
            // ReSharper disable once AssignNullToNotNullAttribute
            if (!dir.IsNullOrEmpty() && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            AssetDatabase.CreateAsset(settings, CoreSettings.K_ConfigLinksPath);
            AssetDatabase.SaveAssets();
            return OperationResult.OK;
        }

        internal static void GenerateMenuStrings(CoreSettings settings)
        {
            if (settings.MenuStrings == null)
                LinkMenuStrings(settings);
            if (settings.MenuStrings == null)
            {
                Debug.LogError("Could not GenerateMenuStrings!");
                return;
            }

            CodeGeneratorConfigEditor.GenerateCode(settings.MenuStrings);
        }

        internal static void LinkMenuStrings(CoreSettings settings)
        {
            Debug.Log("LinkMenuStrings");
            var origMenuStrings = AssetDatabase.LoadAssetAtPath<CodeGeneratorConfig>(k_packagePathEditorResources + k_menuStringsFile);
            if (origMenuStrings == null)
                Debug.LogError($"Could not load at {k_packagePathEditorResources + k_menuStringsFile}");

            var clone = Object.Instantiate(origMenuStrings);
            clone.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;
            AssetDatabase.AddObjectToAsset(clone, settings);
            settings.MenuStrings = clone;
            EditorUtility.SetDirty(settings);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        internal static void UpdateDefines(CoreSettings settings)
        {
            var sb = new StringBuilder();
            foreach (var define in settings.ExtraDefinesForCore)
            {
                sb.AppendLine($"-define:{define}");
            }

            var contents = sb.ToString();
            File.WriteAllText(Path.GetFullPath(k_packagePathRuntime + k_net35File), contents);
            File.WriteAllText(Path.GetFullPath(k_packagePathRuntime + k_net4XFile), contents);
            File.WriteAllText(Path.GetFullPath(k_packagePathEditor + k_net35File), contents);
            File.WriteAllText(Path.GetFullPath(k_packagePathEditor + k_net4XFile), contents);
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }
    }
}