﻿#if CORE_PLAYMAKER

using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface.Obsolete;
using Core.Unity.Types.Fsm.Obsolete;
using HutongGames.PlayMaker;
using UnityEditor;
using UnityEngine;


// IngredientDrawerUIE
namespace Core.Editor.Types.Fsm.Obsolete
{
    [CustomPropertyDrawer(typeof(IFSMStateRef))]
    [CustomPropertyDrawer(typeof(IFSMEventRef))]
    [CustomPropertyDrawer(typeof(IFSMVariableRef))]
    [CustomPropertyDrawer(typeof(FSMDrawerAttribute))]
    public class FSMEventDrawer : PropertyDrawer
    {
        static HutongGames.PlayMaker.Fsm m_lastCachedFsm;

        static string[] m_cachedEventOptions;
        static string[] m_cachedStateOptions;

        static string[] m_cachedStringVarOptions;
        static string[] m_cachedIntVarOptions;
        static string[] m_cachedFloatVarOptions;
        static string[] m_cachedBoolVarOptions;
        static string[] m_cachedColorVarOptions;
        static string[] m_cachedObjectVarOptions;
        static string[] m_cachedGameObjectVarOptions;

        const float k_height = 16f;
        static readonly List<string> k_hideProperties = new List<string>();

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (k_hideProperties.Contains(property.propertyPath))
                return 0;

            var noOverride = attribute is FSMDrawerAttribute drawer && !drawer.EditVariable;

            var variableValueProperty = property.FindPropertyRelative("m_value");
            var variableNameProperty = property.FindPropertyRelative("m_variableName");
            if (variableNameProperty == null || noOverride)
                return k_height;

            var add = 0f;
            if (variableValueProperty != null)
                add += EditorGUI.GetPropertyHeight(variableValueProperty);

            return (k_height * 2) + add;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (k_hideProperties.Contains(property.propertyPath))
                return;

            var fsmProperty = property.FindPropertyRelative("m_fsm");
            var eventNameProperty = property.FindPropertyRelative("m_eventName");
            var stateNameProperty = property.FindPropertyRelative("m_stateName");
            var variableNameProperty = property.FindPropertyRelative("m_variableName");
            var variableValueProperty = property.FindPropertyRelative("m_value");

            if (eventNameProperty!= null)
                label.text += " (E)";
            if (stateNameProperty != null)
                label.text += " (S)";
            if (variableNameProperty != null)
                label.text += " (V)";

            //EditorGUI.BeginProperty(position, label, property);
            //var initialPos = position;
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            position = new Rect(position.x, position.y, position.width, k_height);

            position = FSMReferenceGUI(position, property, fsmProperty);

            string[] autoSelectNames;
            if (attribute is FSMDrawerAttribute drawer && !drawer.AutoSelectName.IsNullOrEmpty())
                autoSelectNames = new[] { drawer.AutoSelectName, property.name, property.displayName};
            else
                autoSelectNames = new[] { property.name, property.displayName};

            var fsm = fsmProperty.objectReferenceValue as PlayMakerFSM;
            if (eventNameProperty != null)
                FSMEventGUI(position, fsm, property, eventNameProperty, autoSelectNames);
            if (stateNameProperty != null)
                FSMStateGUI(position, fsm, property, stateNameProperty, autoSelectNames);
            if (variableNameProperty != null)
                FSMVariableGUI(position, fsm, property, variableNameProperty, variableValueProperty, autoSelectNames);
        }

        void FSMVariableGUI(Rect position, PlayMakerFSM playFsm,
            SerializedProperty parentProperty, SerializedProperty nameProperty, SerializedProperty valueProperty,
            IEnumerable<string> autoSelectNames)
        {
            var disabled = false;
            if (playFsm == null)
            {
                EditorGUI.PropertyField(position, nameProperty, GUIContent.none);
            }
            else
            {
                var fsm = playFsm.FsmTemplate ? playFsm.FsmTemplate.fsm : playFsm.Fsm;
                GetVarOptions(fsm, valueProperty, out var options, out var optionLength);

                var needCacheStates = !options.IsInitializedWith(optionLength) || m_lastCachedFsm != fsm;
                if (needCacheStates)
                {
                    CacheOptions(fsm);
                    GetVarOptions(fsm, valueProperty, out options, out _);
                }

                disabled = AutoSelect(nameProperty, autoSelectNames, options);
                var idx = Array.IndexOf(options, nameProperty.stringValue);

                using (new EditorGUI.DisabledScope(disabled))
                {
                    idx = EditorGUI.Popup(position, idx, options);
                    if (!disabled && idx >= 0 && idx < options.Length)
                        nameProperty.stringValue = options[idx];
                }
            }
            position = new Rect(position.x, position.y + k_height, position.width, position.height);

            var drawerAttr = attribute as FSMDrawerAttribute;
            var noOverride = drawerAttr != null && !drawerAttr.EditVariable;
            if (noOverride)
            {
                // we don't need the user to see or config the field, if it auto-linked itself successfully
                if (disabled && drawerAttr.Self)
                    k_hideProperties.Add(parentProperty.propertyPath);

                return;
            }

            position.height = EditorGUI.GetPropertyHeight(valueProperty);
            if (position.height > k_height + float.Epsilon)
            {
                position.x = 0;
                position.width *= 2;
            }
            EditorGUI.PropertyField(position, valueProperty, GUIContent.none, true);
        }

        Rect FSMReferenceGUI(Rect position, SerializedProperty property, SerializedProperty fsmProperty)
        {
            var component = (property.serializedObject.targetObject as Component);

            var self = attribute is FSMDrawerAttribute drawer && drawer.Self;
            var fsmIsOnSelf = self && component != null;
            PlayMakerFSM playMaker = null;

            if (fsmIsOnSelf)
            {
                var playMakers = component.gameObject.GetComponents<PlayMakerFSM>();
                if (playMakers.Length == 1)
                    playMaker = playMakers[0];
            }

            if (playMaker != null)
                fsmProperty.objectReferenceValue = playMaker;
            else
            {
                var pos = new Rect(position.x, position.y, position.width * 0.45f, position.height);
                EditorGUI.PropertyField(pos, fsmProperty, GUIContent.none);
                position = new Rect(pos.x + position.width * 0.5f, pos.y, position.width * 0.45f, position.height);
            }

            return position;
        }

        static void GetVarOptions(HutongGames.PlayMaker.Fsm fsm, SerializedProperty valueProperty, out string[] options, out int optionLength)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (valueProperty.propertyType)
            {
                case SerializedPropertyType.Boolean:
                    options = m_cachedBoolVarOptions;
                    optionLength = fsm.Variables.BoolVariables.Length + 1;
                    break;
                case SerializedPropertyType.Color:
                    options = m_cachedColorVarOptions;
                    optionLength = fsm.Variables.ColorVariables.Length + 1;
                    break;
                case SerializedPropertyType.Float:
                    options = m_cachedFloatVarOptions;
                    optionLength = fsm.Variables.FloatVariables.Length + 1;
                    break;
                case SerializedPropertyType.Integer:
                    options = m_cachedIntVarOptions;
                    optionLength = fsm.Variables.IntVariables.Length + 1;
                    break;
                //currently only GameObject:
                case SerializedPropertyType.ObjectReference:
                    options = m_cachedGameObjectVarOptions;
                    optionLength = fsm.Variables.GameObjectVariables.Length + 1;
                    break;
                case SerializedPropertyType.String:
                    options = m_cachedStringVarOptions;
                    optionLength = fsm.Variables.StringVariables.Length + 1;
                    break;
                default:
                    options = m_cachedObjectVarOptions;
                    optionLength = fsm.Variables.ObjectVariables.Length + 1;
                    break;
            }
        }

        void FSMStateGUI(Rect position, PlayMakerFSM playFsm,
            SerializedProperty parentProperty, SerializedProperty stateNameProperty,
            IEnumerable<string> autoSelectNames)
        {
            if (playFsm == null)
            {
                EditorGUI.PropertyField(position, stateNameProperty, GUIContent.none);
                return;
            }

            var fsm = playFsm.FsmTemplate ? playFsm.FsmTemplate.fsm : playFsm.Fsm;

            var stateOptionLength = fsm.States.Length + 1;
            var needCacheStates = !m_cachedStateOptions.IsInitializedWith(stateOptionLength) || m_lastCachedFsm != fsm;
            if (needCacheStates)
                CacheOptions(playFsm.FsmTemplate ? playFsm.FsmTemplate.fsm : playFsm.Fsm);

            var disabled = AutoSelect(stateNameProperty, autoSelectNames, m_cachedStateOptions);
            var idx = Array.IndexOf(m_cachedStateOptions, stateNameProperty.stringValue);

            using (new EditorGUI.DisabledScope(disabled))
            {
                idx = EditorGUI.Popup(position, idx, m_cachedStateOptions);
                if (!disabled && idx >= 0 && idx < m_cachedStateOptions.Length)
                    stateNameProperty.stringValue = m_cachedStateOptions[idx];
            }

            // we don't need the user to see or config the field, if it auto-linked itself successfully
            var self = attribute is FSMDrawerAttribute drawer && drawer.Self;
            if (disabled && self)
                k_hideProperties.Add(parentProperty.propertyPath);
        }

        void FSMEventGUI(Rect position, PlayMakerFSM playFsm,
            SerializedProperty parentProperty, SerializedProperty eventNameProperty,
            IEnumerable<string> autoSelectNames)
        {
            if (playFsm == null)
            {
                EditorGUI.PropertyField(position, eventNameProperty, GUIContent.none);
                return;
            }

            var fsm = playFsm.FsmTemplate ? playFsm.FsmTemplate.fsm : playFsm.Fsm;

            var eventOptionLength = fsm.Events.Length + 1;
            var needCacheEvents = !m_cachedEventOptions.IsInitializedWith(eventOptionLength) || m_lastCachedFsm != fsm;
            if (needCacheEvents)
                CacheOptions(playFsm.FsmTemplate ? playFsm.FsmTemplate.fsm : playFsm.Fsm);

            var disabled = AutoSelect(eventNameProperty, autoSelectNames, m_cachedEventOptions);
            var idx = Array.IndexOf(m_cachedEventOptions, eventNameProperty.stringValue);

            using (new EditorGUI.DisabledScope(disabled))
            {
                idx = EditorGUI.Popup(position, idx, m_cachedEventOptions);
                if (!disabled && idx >= 0 && idx < m_cachedEventOptions.Length)
                    eventNameProperty.stringValue = m_cachedEventOptions[idx];
            }

            // we don't need the user to see or config the field, if it auto-linked itself successfully
            var self = attribute is FSMDrawerAttribute drawer && drawer.Self;
            if (disabled && self)
               k_hideProperties.Add(parentProperty.propertyPath);
        }

        static void CacheOptions(HutongGames.PlayMaker.Fsm fsm)
        {
            k_hideProperties.Clear();

            m_lastCachedFsm = fsm;

            CacheOption(fsm.Events, out m_cachedEventOptions);
            CacheOption(fsm.States, out m_cachedStateOptions);
            CacheOption(fsm.Variables.StringVariables, out m_cachedStringVarOptions);
            CacheOption(fsm.Variables.IntVariables, out m_cachedIntVarOptions);
            CacheOption(fsm.Variables.FloatVariables, out m_cachedFloatVarOptions);
            CacheOption(fsm.Variables.BoolVariables, out m_cachedBoolVarOptions);
            CacheOption(fsm.Variables.ColorVariables, out m_cachedColorVarOptions);
            CacheOption(fsm.Variables.ObjectVariables, out m_cachedObjectVarOptions);
            CacheOption(fsm.Variables.GameObjectVariables, out m_cachedGameObjectVarOptions);
        }

        static void CacheOption(IReadOnlyList<INameable> namedVars, out string[] cachedOptions)
        {
            cachedOptions = new string[namedVars.Count + 1];
            for (var i = 0; i < namedVars.Count; i++)
                cachedOptions[i] = namedVars[i].Name;
            cachedOptions[namedVars.Count] = Name.Ignore;
        }

        static bool AutoSelect(SerializedProperty property, IEnumerable<string> fallBacks, string[] options)
        {
            foreach (var fallBack in fallBacks)
            {
                var idx = Array.IndexOf(options, fallBack);
                if (idx < 0 || idx >= options.Length)
                    continue;

                property.stringValue = options[idx];
                return true;
            }

            return false;
        }
    }
}

#endif