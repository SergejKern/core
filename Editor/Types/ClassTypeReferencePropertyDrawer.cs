// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Extensions;
using Core.Types;
using Core.Unity.Types.Attribute;
using Core.Unity.Types;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using Object = UnityEngine.Object;

using static Core.Editor.Extensions.EditorExtensions;

namespace Core.Editor.Types
{
    /// <inheritdoc />
    /// <summary>
    /// Custom property drawer for <see cref="T:Core.Unity.Types.ClassTypeReference" /> properties.
    /// </summary>
    [CustomPropertyDrawer(typeof(ClassTypeReference))]
    [CustomPropertyDrawer(typeof(ClassTypeConstraintAttribute), true)]
    public sealed class ClassTypeReferencePropertyDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) =>
            EditorStyles.popup.CalcHeight(GUIContent.none, 0);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var classRefProp = property.FindPropertyRelative(ClassTypeReference.Editor_ClassRefPropName);
            var scriptProp = property.FindPropertyRelative(ClassTypeReference.Editor_FallbackPropName);

            var constraintAttr = attribute as ClassTypeConstraintAttribute;
            using (var constraintListScope = SimplePool<List<IClassTypeConstraint>>.I.GetScoped())
            {
                var constraintList = constraintListScope.Obj;
                constraintAttr?.GetConstraints(constraintList);
                ClassTypeDrawer.DrawTypeSelectionControl(position, classRefProp, scriptProp, label, constraintList);
            }

        }
    }

    public static class ClassTypeDrawer
    {
        static EditorWindow m_currentInspectorWindow;
        #region Type Filtering

        /// <summary>
        /// Gets or sets a function that returns a collection of types that are
        /// to be excluded from drop-down. A value of <c>null</c> specifies that
        /// no types are to be excluded.
        /// </summary>
        /// <remarks>
        /// <para>This property must be set immediately before presenting a class
        /// type reference property field using <see cref="PropertyField"/>
        /// or <see cref="PropertyField"/> since the value of this
        /// property is reset to <c>null</c> each time the control is drawn.</para>
        /// <para>Since filtering makes extensive use of <see cref="ICollection{Type}.Contains"/>
        /// it is recommended to use a collection that is optimized for fast
        /// lookups such as <see cref="HashSet{Type}"/> for better performance.</para>
        /// </remarks>
        /// <example>
        /// <para>Exclude a specific type from being selected:</para>
        /// <code language="csharp"><![CDATA[
        /// private SerializedProperty _someClassTypeReferenceProperty;
        /// 
        /// public override void OnInspectorGUI() {
        ///     serializedObject.Update();
        /// 
        ///     ClassTypeReferencePropertyDrawer.ExcludedTypeCollectionGetter = GetExcludedTypeCollection;
        ///     EditorGUILayout.PropertyField(_someClassTypeReferenceProperty);
        /// 
        ///     serializedObject.ApplyModifiedProperties();
        /// }
        /// 
        /// private ICollection<Type> GetExcludedTypeCollection() {
        ///     var set = new HashSet<Type>();
        ///     set.Add(typeof(SpecialClassToHideInDropdown));
        ///     return set;
        /// }
        /// ]]></code>
        /// </example>
        static Func<ICollection<Type>> ExcludedTypeCollectionGetter { get; set; }

        public static List<Type> GetFilteredTypes(IReadOnlyCollection<IClassTypeConstraint> filter)
        {
            var types = new List<Type>();

            var excludedTypes = ExcludedTypeCollectionGetter?.Invoke();

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) 
                FilterTypes(assembly, filter, excludedTypes, types);

            types.Sort((a, b) => string.Compare(a.FullName, b.FullName, StringComparison.Ordinal));

            return types;
        }

        public static void FilterTypes(Assembly assembly, 
            IReadOnlyCollection<IClassTypeConstraint> filter, 
            ICollection<Type> excludedTypes, 
            List<Type> output)
        {
            output.AddRange(from type in assembly.GetTypes()
                            where type.IsVisible
                            where filter == null || filter.All(f=>f.IsConstraintSatisfied(type))
                            where excludedTypes == null || !excludedTypes.Contains(type)
                            select type);
            // LINQ-Expression-meaning:
            //foreach (var type in assembly.GetTypes())
            //{
            //    if (!type.IsVisible)
            //        continue;
            //    if (filter!= null && !filter.IsConstraintSatisfied(type))
            //        continue;
            //    if (excludedTypes!= null && excludedTypes.Contains(type))
            //        continue;
            //    output.Add(type);
            //}
        }

        #endregion

        #region Type Utility

        static readonly Dictionary<string, Type> k_typeMap = new Dictionary<string, Type>();

        static Type ResolveType(string classRef)
        {
            if (k_typeMap.TryGetValue(classRef, out var type))
                return type;

            type = !string.IsNullOrEmpty(classRef) ? Type.GetType(classRef) : null;
            k_typeMap[classRef] = type;
            return type;
        }

        #endregion

        #region Control Drawing / Event Handling

        static readonly int k_controlHint = typeof(ClassTypeReferencePropertyDrawer).GetHashCode();
        static readonly GUIContent k_tempContent = new GUIContent();

        static void DrawTypeSelectionControl(Rect position, GUIContent label, ref string classRef, ref Object script, 
            IReadOnlyCollection<IClassTypeConstraint> filter = null)
        {
            if (label != null && label != GUIContent.none)
                position = EditorGUI.PrefixLabel(position, label);

            var controlID = GUIUtility.GetControlID(k_controlHint, FocusType.Keyboard, position);

            var triggerDropDown = false;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (Event.current.GetTypeForControl(controlID))
            {
                case EventType.ExecuteCommand:
                    if (Event.current.commandName == "TypeReferenceUpdated")
                    {
                        if (m_selectionControlID == controlID)
                        {
                            if (classRef != m_selectedClassRef)
                            {
                                classRef = m_selectedClassRef;
                                script = m_script;
                                GUI.changed = true;
                            }

                            m_selectionControlID = 0;
                            m_selectedClassRef = null;
                        }
                    }
                    break;

                case EventType.MouseDown:
                    if (GUI.enabled && position.Contains(Event.current.mousePosition))
                    {
                        GUIUtility.keyboardControl = controlID;
                        triggerDropDown = true;
                        Event.current.Use();
                    }
                    break;

                case EventType.KeyDown:
                    if (GUI.enabled && GUIUtility.keyboardControl == controlID)
                    {
                        if (Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.Space)
                        {
                            triggerDropDown = true;
                            Event.current.Use();
                        }
                    }
                    break;

                case EventType.Repaint:
                    if (classRef.IsNullOrEmpty())
                        k_tempContent.text = "(None)";
                    else
                    {
                        var t = ResolveType(classRef);
                        if (t != null)
                            k_tempContent.text = $"{t.Name} ({t.Namespace})";
                        else
                        {
                            // Remove assembly name from content of popup control.
                            var classRefParts = classRef.Split(',');
                            k_tempContent.text = $"{{Missing}} {classRefParts[0].Trim()}";
                            if (script is MonoScript ms)
                            {
                                classRef = ClassTypeReference.GetClassRef(ms.GetClass());
                                GUI.changed = true;
                            }
                        }
                    }

                    EditorStyles.popup.Draw(position, k_tempContent, controlID);
                    break;
            }

            if (triggerDropDown) 
                OnDropDown(position, classRef, controlID, filter);
        }

        static void OnDropDown(Rect position, string classRef, int controlID, 
            IReadOnlyCollection<IClassTypeConstraint> filter = null)
        {
            m_selectionControlID = controlID;
            m_selectedClassRef = classRef;
            m_script = null;

            var filteredTypes = GetFilteredTypes(filter);
            
            m_currentInspectorWindow = EditorWindow.focusedWindow;
            GetNamesAndNamespacesForTypes(filteredTypes, out var names, out var namespaces);
            var popup = CreateTypeSelectorPopup(filteredTypes, names, namespaces, new MenuFromNamespace(), k_onSelectedTypeName);
            PopupWindow.Show(position, popup);
        }

        public static void DrawTypeSelectionControl(Rect position, SerializedProperty classRefProp, SerializedProperty scriptProp,
            GUIContent label, List<IClassTypeConstraint> filter = null)
        {
            try
            {
                var restoreShowMixedValue = EditorGUI.showMixedValue;
                EditorGUI.showMixedValue = classRefProp.hasMultipleDifferentValues;

                var classRef = classRefProp.stringValue;
                var scriptAsset = scriptProp.objectReferenceValue;
                DrawTypeSelectionControl(position, label, ref classRef, ref scriptAsset, filter);
                classRefProp.stringValue = classRef;
                scriptProp.objectReferenceValue = scriptAsset;

                EditorGUI.showMixedValue = restoreShowMixedValue;
            }
            finally
            {
                ExcludedTypeCollectionGetter = null;
            }
        }

        static int m_selectionControlID;
        static string m_selectedClassRef;
        static MonoScript m_script;

        static readonly GenericMenu.MenuFunction2 k_onSelectedTypeName = OnSelectedTypeName;

        static void OnSelectedTypeName(object userData)
        {
            var selectedType = userData as Type;
            //Debug.Log($"OnSelectedTypeName {selectedType}");

            m_selectedClassRef = ClassTypeReference.GetClassRef(selectedType);
            var scripts = Resources.FindObjectsOfTypeAll<MonoScript>();
            foreach (var s in scripts)
            {
                if (s.GetClass() != selectedType)
                    continue;
                m_script = s;
                break;
            }

            var typeReferenceUpdatedEvent = EditorGUIUtility.CommandEvent("TypeReferenceUpdated");
            if (m_currentInspectorWindow == null)
                return;
            m_currentInspectorWindow.SendEvent(typeReferenceUpdatedEvent);
        }

        #endregion
    }
}
