using UnityEditor;
using UnityEngine;
using Core.Extensions;

namespace Core.Editor.Extensions
{
    public static class CustomEditorPrefs
    {
        public static void SetColor(string key, Color value)
        {
            EditorPrefs.SetFloat(key + "/R", value.r);
            EditorPrefs.SetFloat(key + "/G", value.g);
            EditorPrefs.SetFloat(key + "/B", value.b);
            EditorPrefs.SetFloat(key + "/A", value.a);
        }

        public static Color GetColor(string key)
        {
            var r = EditorPrefs.GetFloat(key + "/R");
            var g = EditorPrefs.GetFloat(key + "/G");
            var b = EditorPrefs.GetFloat(key + "/B");
            var a = EditorPrefs.GetFloat(key + "/A");
            return new Color(r, g, b, a);
        }


        public static void SetGuid(string key, Object o)
        {
            var guid = "";
            if (o != null)
                guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(o));

            EditorPrefs.SetString(key, guid);
        }

        public static T GetAssetFromGuid<T>(string key) where T : Object
        {
            var guid = EditorPrefs.GetString(key, "");
            var path = AssetDatabase.GUIDToAssetPath(guid);
            return !path.IsNullOrEmpty() ? AssetDatabase.LoadAssetAtPath<T>(path) : null;
        }

        public static Vector3Int GetVector3Int(string key, Vector3Int defaultValue = default)
        {
            var x = EditorPrefs.GetInt($"{key}.X", defaultValue.x);
            var y = EditorPrefs.GetInt($"{key}.Y", defaultValue.x);
            var z = EditorPrefs.GetInt($"{key}.Z", defaultValue.x);
            return new Vector3Int(x,y,z);
        }

        public static void SetVector3Int(string key, Vector3Int value)
        {
            EditorPrefs.SetInt($"{key}.X", value.x);
            EditorPrefs.SetInt($"{key}.Y", value.y);
            EditorPrefs.SetInt($"{key}.Z", value.z);
        }

    }
}
