﻿using UnityEngine;

namespace Core.Editor.Data
{
    public struct AssetRenamed
    {
        public Object RenamedAsset;
        public string OldName;
        public string NewName;
    }
}