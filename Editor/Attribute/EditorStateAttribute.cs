using System;
using System.Linq;

namespace Core.Editor.Attribute
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EditorStateAttribute : System.Attribute
    {
        public readonly int Order;
        readonly Type[] m_validStateTypes;

        public EditorStateAttribute(Type t, int order = 0)
        {
            m_validStateTypes = new[] {t};
            Order = order;
        }

        public EditorStateAttribute(Type[] t, int order = 0)
        {
            m_validStateTypes = t;
            Order = order;
        }

        public bool IsValid(Type t) => m_validStateTypes.Contains(t);
    }
}