﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

using Core.Unity.Attributes;
using Core.Extensions;
using static Core.Editor.Extensions.EditorExtensions;

namespace Core.Editor.Attribute
{
    [CustomPropertyDrawer(typeof(MultiPropertyAttribute), true)]
    public class MultiPropertyDrawer : PropertyDrawer
    {
        static readonly Dictionary<Type, MultiPropertyEditorDrawer> k_multiPropEditorDrawer = new Dictionary<Type, MultiPropertyEditorDrawer>();

        MultiPropertyAttribute GetAttributeData()
        {
            if (!(attribute is MultiPropertyAttribute mpa))
                return null;
            // Get the attribute list, sorted by "order".
            if (mpa.Stored != null)
                return mpa;

            if (k_multiPropEditorDrawer.IsNullOrEmpty())
                InitEditorPropDrawer();

            var objArr = fieldInfo.GetCustomAttributes(typeof(MultiPropertyAttribute), false);
            var converted =
                Array.ConvertAll(objArr, item => (MultiPropertyAttribute) item);
            mpa.Stored = converted.OrderBy(s => s.order);
            return mpa;
        }

        static void InitEditorPropDrawer()
        {
            var drawerTypes = new List<Type>();
            ReflectionGetAllTypes(typeof(MultiPropertyEditorDrawer), ref drawerTypes, out _, out _);
            foreach (var drawerType in drawerTypes)
            {
                var property = drawerType.GetProperty(nameof(MultiPropertyEditorDrawer.PreviewForAttribute));
                if (property == null)
                    continue;

                // create an instance of that type
                var instance = (MultiPropertyEditorDrawer) Activator.CreateInstance(drawerType);

                var actionType = (Type) property.GetValue(instance, null);
                k_multiPropEditorDrawer.Add(actionType, instance);
            }
        }

        static bool IsVisible(MultiPropertyAttribute mpa, SerializedProperty property)
        {
            // If the attribute is invisible, regain the standard vertical spacing.
            foreach (var attr in mpa.Stored)
            {
                if (k_multiPropEditorDrawer.TryGetValue(attr.GetType(), out var val))
                {
                    if (!val.IsVisible(attr, property))
                        return false;
                }
                else if (!attr.EDITOR_IsVisible(property))
                    return false;
            }

            return true;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var mpa = GetAttributeData();
            if (mpa == null)
                return -EditorGUIUtility.standardVerticalSpacing;

            // If the attribute is invisible, regain the standard vertical spacing.
            if(!IsVisible(mpa, property))
                return -EditorGUIUtility.standardVerticalSpacing;

            // In case no attribute returns a modified height, return the property's default one:
            var height = EditorGUI.GetPropertyHeight(property, label, true);

            // Go through the attributes, and try to get an altered height, if no altered height return default height.
            return mpa.Stored.Aggregate(height, (current, attr) 
                => k_multiPropEditorDrawer.TryGetValue(attr.GetType(), out var val)
                ? val.GetPropertyHeight(attr, property, label, current)
                : attr.EDITOR_GetPropertyHeight(property, label, current));
        }

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var mpa = GetAttributeData();

            if (mpa == null)
                return;

            // Calls to IsVisible. If it returns false for any attribute, the property will not be rendered.
            if (!IsVisible(mpa, property))
                return;

            // Calls to OnPreRender before the last attribute draws the UI.
            foreach (var attr in mpa.Stored)
            {
                if (k_multiPropEditorDrawer.TryGetValue(attr.GetType(), out var val))
                    val.OnPreGUI(attr, ref position, property, label);
                else
                    attr.EDITOR_OnPreGUI(ref position, property, label);
            }
            // The last attribute is in charge of actually drawing something:
            var last = mpa.Stored.Last();
            if (k_multiPropEditorDrawer.TryGetValue(last.GetType(), out var lastDrawer))
                lastDrawer.OnGUI(last, ref position, property, label);
            else last.EDITOR_OnGUI(ref position, property, label);

            // Calls to OnPostRender after the last attribute draws the UI. These are called in reverse order.
            foreach (var attr in mpa.Stored.Reverse())
            {
                if (k_multiPropEditorDrawer.TryGetValue(attr.GetType(), out var val))
                    val.OnPostGUI(attr, ref position, property);
                else attr.EDITOR_OnPostGUI(ref position, property);
            }
        }
    }



    // todo 0: maybe make it more similar to unity workflow with attributes? eg. [CustomMultiPropertyDrawer (typeof(MultiPropertyAttribute), true)]
    public abstract class MultiPropertyEditorDrawer
    {
        public abstract Type PreviewForAttribute { get; }

        public virtual void OnGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property, GUIContent label) => 
            EditorGUI.PropertyField(position, property, label, true);
        
        public virtual void OnPreGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property, GUIContent label) { }
        public virtual void OnPostGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property) { }
        public virtual bool IsVisible(MultiPropertyAttribute attr, SerializedProperty property) { return true; }

        public virtual float GetPropertyHeight(MultiPropertyAttribute attr, SerializedProperty property, GUIContent label, float height)
            => height;
    }

}