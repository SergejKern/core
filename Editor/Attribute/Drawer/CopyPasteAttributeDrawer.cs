﻿using System;

using UnityEditor;
using UnityEngine;
using Core.Unity.Attributes;

using Object = UnityEngine.Object;
using static Core.Editor.Extensions.SerializedPropertyExtensions;

namespace Core.Editor.Attribute
{
    public class CopyPasteAttributeDrawer : MultiPropertyEditorDrawer
    {
        public override Type PreviewForAttribute => typeof(CopyPasteAttribute);

        public override float GetPropertyHeight(MultiPropertyAttribute attr, SerializedProperty property, GUIContent label, float height)
            => height + 16f;

        public override void OnPreGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty prop,
            GUIContent label)
        {
            // base.OnPreGUI(ref position, prop, label);
            var width = position.width;

            if (width <= 1)
                width = Screen.width;

            position.width = width;
            //position.x += 16;

            var buttonPos = new Rect(width - 50, position.y, 20, 16);
            if (GUI.Button(buttonPos, "C"))
                CopyPasteOperation.Copy(prop);

            buttonPos.x += 25;
            if (GUI.Button(buttonPos, "P"))
                CopyPasteOperation.Paste(prop);
        }
    }

    public static class CopyPasteOperation
    {
        public struct CopyData
        {
            public Object SourceObject;
            public string PropertyPath;

            public bool DirectReference;

            public bool IsCut;
            public bool DoNameCheck;
        }

        internal static CopyData CopiedProperty;
        public static bool HasData => CopiedProperty.SourceObject != null;

        public static void Paste(SerializedProperty prop)
        {
            DoPaste(prop.Copy());
            prop.serializedObject.ApplyModifiedProperties();
        }

        public static void Copy(SerializedProperty prop, bool directReference = false, bool doNameCheck = true) =>
            CopiedProperty = new CopyData()
                { PropertyPath = prop.propertyPath, SourceObject = prop.serializedObject.targetObject,
                    DirectReference = directReference,
                    IsCut = false, 
                    DoNameCheck = doNameCheck };
        
        // ReSharper disable once MethodNameNotMeaningful
        public static void Cut(SerializedProperty prop, bool directReference = true, bool doNameCheck = false) =>
            CopiedProperty = new CopyData()
                { PropertyPath = prop.propertyPath, SourceObject = prop.serializedObject.targetObject,
                    DirectReference = directReference,
                    IsCut = true, DoNameCheck = doNameCheck };

        static void DoPaste(SerializedProperty dest_iterator)
        {
            using (var source_iterator =
                new SerializedObject(CopiedProperty.SourceObject).FindProperty(CopiedProperty.PropertyPath))
            {
                if (CopiedProperty.DirectReference)
                {
                    if (CopiedProperty.DoNameCheck && !string.Equals(source_iterator.name, dest_iterator.name))
                        return;
                    DoCopyPaste(source_iterator, dest_iterator);
                }
                else while (source_iterator.NextVisible(true) && dest_iterator.NextVisible(true)) //iterate through all serializedProperties
                {
                    //try obtaining the property in destination component
                    if (CopiedProperty.DoNameCheck && !string.Equals(source_iterator.name, dest_iterator.name))
                        break;

                    DoCopyPaste(source_iterator, dest_iterator);
                }
            }

            if (!CopiedProperty.IsCut) return;
            // ReSharper disable once ConvertToUsingDeclaration
            using (var source_iterator = new SerializedObject(CopiedProperty.SourceObject).FindProperty(CopiedProperty.PropertyPath))
            {
                var serObj = source_iterator.serializedObject;
                Clear(source_iterator);
                serObj.ApplyModifiedProperties();
                CopiedProperty = default;
            }

        }
    }
}
