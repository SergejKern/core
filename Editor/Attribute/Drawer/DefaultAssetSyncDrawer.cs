﻿using System;
using System.Reflection;
using Core.Editor.Extensions;
using Core.Unity.Attributes;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Attribute
{
    // ReSharper disable once UnusedType.Global
    // used via reflection
    public class DefaultAssetSyncDrawer : MultiPropertyEditorDrawer
    {
        public override Type PreviewForAttribute => typeof(DefaultAssetSyncAttribute);

        public override void OnPreGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property, GUIContent label)
        {           
            var syncAttr = (DefaultAssetSyncAttribute) attr;

            if (property.propertyType == SerializedPropertyType.ObjectReference &&
                property.objectReferenceValue == null)
            {
                property.objectReferenceValue = (Object) GetObjectPropertyValue(syncAttr, property);
                GUI.changed = true;
                return;
            }

            if (property.propertyType == SerializedPropertyType.Generic)
            {
                GetProp(syncAttr.ShouldGetStatic_PropertyPath, property, out var parentObj, out var shouldGetProperty);
                if (shouldGetProperty != null && (bool) shouldGetProperty.GetValue(parentObj))
                {
                    property.SetValue(GetObjectPropertyValue(syncAttr, property));
                    GUI.changed = true;
                }
            }
        }

        public override void OnPostGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property)
        {
            var syncAttr = (DefaultAssetSyncAttribute) attr;

            if (property.propertyType == SerializedPropertyType.ObjectReference &&
                property.objectReferenceValue != null)
            {
                SetObjectPropertyValue(syncAttr, property, property.objectReferenceValue);
                return;
            }
            
            if (property.propertyType == SerializedPropertyType.Generic)
            {
                GetProp(syncAttr.ShouldSetStatic_PropertyPath, property, out var parentObj, out var shouldSetProperty);
                if (shouldSetProperty != null && (bool) shouldSetProperty.GetValue(parentObj))
                    SetObjectPropertyValue(syncAttr, property, property.GetValue<object>());
            }
        }

        static void SetObjectPropertyValue(DefaultAssetSyncAttribute syncAttr, SerializedProperty property, object value)
        {
            GetProp(syncAttr.SyncVariable_PropertyPath, property, out var parentObj, out var syncProperty);
            if (syncProperty != null)
                syncProperty.SetValue(parentObj, value);
        }

        static object GetObjectPropertyValue(DefaultAssetSyncAttribute syncAttr, SerializedProperty property)
        {
            GetProp(syncAttr.SyncVariable_PropertyPath, property, out var parentObj, out var syncProperty);

            if (syncProperty != null)
                return syncProperty.GetValue(parentObj);

            Debug.LogWarning("Error when trying to sync default asset. Not a property: " + syncAttr.SyncVariable_PropertyPath);
            return null;
        }

        static void GetProp(string propPath, SerializedProperty property, 
            out object parentObj,
            out PropertyInfo syncProperty)
        {
            var propertyPath = property.propertyPath;
            var subLength = propertyPath.Length - property.name.Length - 1;
            parentObj = property.serializedObject.targetObject;

            if (subLength > 0)
            {
                var parentPath = propertyPath.Substring(0, subLength);
                var parent = property.serializedObject.FindProperty(parentPath);
                parentObj = parent.GetValue<object>();
            }

            syncProperty = parentObj.GetType().GetProperty(propPath);
        }
    }
}
