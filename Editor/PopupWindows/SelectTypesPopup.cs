using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Utility;
using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.PopupWindows
{
    public class SelectTypesPopup : PopupWindowContent
    {
        struct FoldOutGroup
        {
            public string Path;
            public bool Expanded;
            public bool HasVisibleItems;

            public List<FoldOutGroup> Group;
            public List<Item> Items;
        }

        struct Item
        {
            public string Class;
            public GenericMenu.MenuFunction2 Func;
            public object UserData;
        }

        readonly List<FoldOutGroup> m_groups = new List<FoldOutGroup>();
        readonly List<Item> m_globalItems = new List<Item>();
        Item m_selectedItem;

        string m_searchText;

        Vector2 m_scrollPos;
        //string m_openFoldout;

        GUIStyle m_buttonStyle;

        const string k_searchFocus = "SelectTypes_Search";

        public static Vector2 WindowSize => new Vector2(400, 400);
        public override Vector2 GetWindowSize() => WindowSize;


        GUIStyle ButtonStyle
        {
            get
            {
                if (m_buttonStyle != null)
                    return m_buttonStyle;
                m_buttonStyle = new GUIStyle(EditorStyles.toolbarButton) {alignment = TextAnchor.MiddleLeft};
                return m_buttonStyle;
            }
        }

        public override void OnGUI(Rect rect)
        {
            m_selectedItem = default;
            GUILayout.Label("Select Type", EditorStyles.boldLabel);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                GUI.SetNextControlName(k_searchFocus);
                m_searchText = EditorGUILayout.TextField("Search: ", m_searchText);

                if (check.changed)
                    UpdateGroupFilter();
                if (!string.Equals(GUI.GetNameOfFocusedControl(),k_searchFocus))
                    GUI.FocusControl(k_searchFocus);
            }

            using (var scroll=new GUILayout.ScrollViewScope(m_scrollPos, false, true))
            {
                m_scrollPos = scroll.scrollPosition;
                IterateGroups(m_groups);
                IterateItems(m_globalItems);
            }

            if (m_selectedItem.Func != null)
            {
                m_selectedItem.Func.Invoke(m_selectedItem.UserData);
                EditorWindow.GetWindow<PopupWindow>().Close();
            }

        }

        void UpdateGroupFilter() => UpdateGroupVisibility(m_groups, out _);

        bool Filter(Item item) => !m_searchText.IsNullOrEmpty()
                                  && item.Class.IndexOf(m_searchText, StringComparison.OrdinalIgnoreCase) < 0;


        bool HasVisibleItems(IEnumerable<Item> items) => items.Any(item => !Filter(item));

        void UpdateGroupVisibility(IList<FoldOutGroup> group, out bool hasVisibleItems)
        {
            hasVisibleItems = false;
            for (var index = 0; index < group.Count; index++)
            {
                var g = group[index];
                UpdateGroupVisibility(ref g, out var visibleItems);
                hasVisibleItems |= visibleItems;
                group[index] = g;
            }
        }

        void UpdateGroupVisibility(ref FoldOutGroup group, out bool hasVisibleItems)
        {
            UpdateGroupVisibility(group.Group, out hasVisibleItems);
            hasVisibleItems |= HasVisibleItems(group.Items);
            group.HasVisibleItems = hasVisibleItems;

            if (m_searchText.Length > 3)
                group.Expanded = hasVisibleItems;
        }


        void IterateItems(IEnumerable<Item> items)
        {
            foreach (var item in items)
            {
                if (Filter(item))
                    continue;
                if (!GUILayout.Button(item.Class, ButtonStyle))
                    continue;

                m_selectedItem = item;
            }
        }

        void IterateGroups(IList<FoldOutGroup> group)
        {
            for (var index = 0; index < group.Count; index++)
            {
                var g = group[index];

                if (!g.HasVisibleItems)
                    continue;
                // using (new EditorGUILayout.HorizontalScope())
                // {
                g.Expanded = EditorGUILayout.Foldout(g.Expanded, g.Path, true, EditorStyles.foldoutHeader);

                // EditorGUILayout.LabelField(g.Path, EditorStyles.foldoutHeader);
                // }

                CustomGUI.HSplitter();
                if (g.Expanded)
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        GUILayout.Space(20f);

                        using (new EditorGUILayout.VerticalScope())
                        {
                            IterateGroups(g.Group);
                            IterateItems(g.Items);
                        }
                    }
                }                
                group[index] = g;
            }
        }

        public void AddTypeItem(string path,
            GenericMenu.MenuFunction2 func,
            object userData)
        {
            var paths = path.Split('/');

            var group = m_groups;
            var items = m_globalItems;
            for (var i = 0; i < paths.Length-1; i++)
            {
                var idx = group.FindIndex(g => string.Equals(g.Path, paths[i]));
                if (idx == -1)
                {
                    var newGroup = new List<FoldOutGroup>();
                    items = new List<Item>();
                    group.Add(new FoldOutGroup()
                    {
                        Group = newGroup,
                        Items = items,
                        Path = paths[i],
                        HasVisibleItems = true
                    });
                    group = newGroup;
                }
                else
                {
                    items = group[idx].Items;
                    group = group[idx].Group;
                }
            }

            var className = paths[paths.Length - 1];

            items.Add(new Item()
            {
                Class = className,
                Func = func,
                UserData = userData
            });
        }
    }
}