using System;
using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.PopupWindows
{
    public class NamePopup : PopupWindowContent
    {
        public NamePopup(GenericMenu.MenuFunction2 onOK, 
            GenericMenu.MenuFunction cancel = null,
            CheckName checkName = null, 
            string[] options = null)
        {
            m_onOK = onOK;
            m_onCancel = cancel;
            m_onNameChange = checkName;
            m_options = options;
        }

        public delegate bool CheckName(string name);

        readonly GenericMenu.MenuFunction m_onCancel;
        readonly GenericMenu.MenuFunction2 m_onOK;
        readonly CheckName m_onNameChange;
        readonly string[] m_options;

        string m_inputText;

        bool m_ok;

        public static Vector2 WindowSize => new Vector2(400, 50);
        public override Vector2 GetWindowSize() => WindowSize;

        public override void OnGUI(Rect rect)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Name: ", EditorStyles.boldLabel);
                if (m_options != null)
                {
                    var idx = Array.IndexOf(m_options, m_inputText);
                    idx = EditorGUILayout.Popup(idx, m_options);
                    m_inputText = idx.IsInRange(m_options) ? m_options[idx] : "";
                }
                else m_inputText = GUILayout.TextField(m_inputText);

                if (check.changed) 
                    m_ok = m_onNameChange == null || m_onNameChange.Invoke(m_inputText);
            }

            using (new GUILayout.HorizontalScope())
            {
                using (new EditorGUI.DisabledScope(!m_ok))
                {
                    // todo: OK on Enter
                    if (GUILayout.Button("OK"))
                    {
                        m_onOK.Invoke(m_inputText);
                        EditorWindow.GetWindow<PopupWindow>().Close();
                    }
                }
                // todo: Cancel on Esc
                if (GUILayout.Button("Cancel"))
                {
                    m_onCancel?.Invoke();
                    EditorWindow.GetWindow<PopupWindow>().Close();
                }
            }
        }
    }
}