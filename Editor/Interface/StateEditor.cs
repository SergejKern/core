using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Editor.Attribute;
using Core.Extensions;
using Core.Interface;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Interface
{
    public abstract class StateEditor : IEditor, IUpdatable, ISceneGUICallable, IRepaintable
    {
        public abstract string Name { get; }
        protected abstract Type ThisType { get; }
        public object ParentContainer { get; private set; }

        protected IEditorState[] m_editorStates;
        protected IEditorState m_state;

        Vector2 m_guiScrollPos;

        public virtual void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            SceneView.duringSceneGui += OnSceneGUI;
            Undo.undoRedoPerformed += UndoRedoPerformed;

            m_editorStates = CreateStatesForEditor(ThisType, this);
            InitStates();
        }

        void InitStates()
        {
            if (!m_editorStates.IsNullOrEmpty())
                m_state = m_editorStates[0];

            foreach (var state in m_editorStates)
                state.Init(this);
        }

        public virtual void Terminate()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
            Undo.undoRedoPerformed -= UndoRedoPerformed;

            foreach (var state in m_editorStates)
                state.Terminate();
        }

        public virtual void OnGUI(float width)
        {
            using (var scrollViewScope = new GUILayout.ScrollViewScope(m_guiScrollPos))
            {
                m_guiScrollPos = scrollViewScope.scrollPosition;
                MainEditorGUI(width);
                StateGUI(width);
            }
        }

        public virtual void MainEditorGUI(float width) { }
        public virtual void StateGUI(float width) => m_state?.OnGUI(width);

        public virtual void Update()
        {
            if (m_state == null)
                return;
            foreach (var ed in m_state.SubEditors)
            {
                if (ed is IUpdatable updatable)
                    updatable.Update();
            }
        }

        public virtual void OnSceneGUI(SceneView sceneView)
        {
            if (m_state == null)
                return;
            foreach (var ed in m_state.SubEditors)
            {
                if (ed is ISceneGUICallable sceneGUICallable)
                    sceneGUICallable.OnSceneGUI(sceneView);
            }
        }

        public virtual void UndoRedoPerformed() { }

        static IEditorState[] CreateStatesForEditor(Type editorType, IEditor parent)
        {
            var stateTypes = GetStateEditorTypes(editorType).ToArray();
            return CreateEditors(stateTypes, parent);
        }

        static IEditorState[] CreateEditors(IReadOnlyList<Type> stateTypes, IEditor parent)
        {
            var editorStates = new IEditorState[stateTypes.Count];
            for (var i = 0; i < editorStates.Length; i++)
            {
                editorStates[i] = (IEditorState) Activator.CreateInstance(stateTypes[i]);
                editorStates[i].SetParent(parent);
            }   
            return editorStates;
        }

        static IEnumerable<Type> GetStateEditorTypes(Type editorType)
        {
            var stateType = typeof(IEditorState);

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => stateType.IsAssignableFrom(p)
                            && stateType != p
                            && !p.IsAbstract
                            && p.GetCustomAttribute<ParentEditorAttribute>()?.IsValid(editorType) == true);
            return types;
        }

        public void Repaint()
        {
            switch (ParentContainer)
            {
                case EditorWindow w: w.Repaint(); break;
                case UnityEditor.Editor e: e.Repaint(); break;
                case IRepaintable r: r.Repaint(); break;
            }
        }
    }
}
