﻿using UnityEngine;

namespace Core.Editor.Interface.GUI
{
    public interface IGUIGrid
    {
        Vector2 Offset { get; set; }
        Rect DrawRect { get; }
        Rect EventRect { get; }

        Color GridColor { get; }
        bool Dragging { get; set; }
        float Zoom { get; set; }
    }

    public interface IRectSelection
    {
        Rect EventRect { get; }

        bool Selecting { get; set; }
        Rect CurrentSelection { get; set; }
    }
}