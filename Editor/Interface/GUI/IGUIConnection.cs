using UnityEngine;

namespace Core.Editor.Interface.GUI
{
    public enum ConnectionPos
    {
        None,
        Left,
        Down,
        Right,
        Up
    }

    public interface IGUIConnectable : IGUIElement
    {
        ConnectionPos IncomingPos { get; }
        ConnectionPos OutgoingPos { get; }
    }

    public interface IGUIConnection
    {
        IGUIConnectable NodeFrom { get; }
        IGUIConnectable NodeTo { get; }
        Color Color { get; }
    }
}