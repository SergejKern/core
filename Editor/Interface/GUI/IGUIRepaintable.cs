﻿
namespace Core.Editor.Interface.GUI
{
    public interface IGUIRepaintable
    {
        void Repaint();
    }
}
