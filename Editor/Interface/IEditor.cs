using Core.Unity.Types;
using System;
using Core.Interface;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Interface
{
    public interface IEditor : INamed
    {
        void Init(object parentContainer);
        void Terminate();
        void OnGUI(float width);
        object ParentContainer { get; }
    }
    public interface IInspectorEditor : IEditor
    {
        Object TargetObj { get; set; }
        SerializedObject SerializedObject { get; }
    }

    public interface IInspectorEditor<T> : IInspectorEditor
    {
        T Target { get; set; }
    }

    public interface IEditorExtras
    {
        bool ShowAlways { get; }
        bool ShowLabel { get; }
    }

    public interface ISceneGUICallable
    {
        void OnSceneGUI(SceneView sceneView);
    }

    public interface IRepaintable
    {
        void Repaint();
    }
    [Serializable]
    public class RefIEditor : InterfaceContainer<IEditor> {}
}