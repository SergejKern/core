using System;
using System.Collections.Generic;
using Core.Unity.Types;

namespace Core.Editor.Interface
{
    public interface IEditorState : IEditor
    {
        Type ParentEditor { get; }

        bool IsApplicable { get; }

        IEnumerable<IEditor> SubEditors { get; }
        void SetParent(IEditor parent);
    }

    [Serializable]
    public class RefIEditorState : InterfaceContainer<IEditorState> {}
}