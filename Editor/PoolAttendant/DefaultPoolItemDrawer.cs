﻿using Core.Unity.Utility.PoolAttendant;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.PoolAttendant
{
    [CustomPropertyDrawer(typeof(DefaultPoolItem))]
    public class DefaultPoolItemDrawer : PropertyDrawer
    {
        SerializedProperty m_prefab;
        SerializedProperty m_size;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
        }

        public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
        {
            m_prefab = property.FindPropertyRelative(nameof(DefaultPoolItem.Prefab));
            m_size = property.FindPropertyRelative(nameof(DefaultPoolItem.Size));

            EditorGUI.BeginProperty(pos, label, property);

            EditorGUI.PropertyField(
                new Rect(pos.x, pos.y, 40, pos.height), m_size, GUIContent.none);

            m_prefab.objectReferenceValue = EditorGUI.ObjectField(
                new Rect(pos.x + 40, pos.y, pos.width - 40, pos.height), "",
                m_prefab.objectReferenceValue, typeof(GameObject), false);


            EditorGUI.EndProperty();
        }
    }
}