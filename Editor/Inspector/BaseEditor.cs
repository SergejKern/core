﻿using Core.Editor.Interface;
using Core.Events;
using Core.Unity.Interface;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Inspector
{
    //[CustomEditor(typeof(Object), true, isFallback = true)]
    //[CanEditMultipleObjects]
    public abstract class BaseEditor<T> : BaseEditor, IInspectorEditor<T> where T: Object
    {
        public override string Name => $"{GetType().Name} {TargetName}";
        public virtual T Target
        {
            get => TargetObj as T;
            set => TargetObj = value;
        }

        string TargetName => Target != null ? Target.name : "";
    }

    public class BaseEditor : IInspectorEditor, IRepaintable, IEventListener<ObjectChanged>
    {
        VerificationResult m_result;
        public VerificationResult Result => m_result;

        public virtual string Name => "BaseEditor";
        public object ParentContainer { get; private set; }

        public Object TargetObj
        {
            get => m_target;
            set
            {
                m_target = value;
                SerializedObject = m_target != null ? new SerializedObject(m_target) : null;
                OnTargetChanged();
            }
        }

        Object m_target;

        public SerializedObject SerializedObject { get; private set; }
        public bool IsDetached => ParentContainer is EditorWindow;

        public virtual void Init(object parentContainer)
        {
            EventMessenger.AddListener(this);
            ParentContainer = parentContainer;
            Verify();
        }

        public virtual void Terminate() => EventMessenger.RemoveListener(this);
        
        // todo 2 CORE_EDITOR: take a look also at https://gist.github.com/LotteMakesStuff/e354cf6e8a4a8194fced3323b15fc1ba
        // ..Copy paste all props? via context menu? (CopyPasteAttribute)
        public virtual void OnGUI(float width)
        {
            if (ParentContainer is UnityEditor.Editor ed) 
                ed.DrawDefaultInspector();
            if (GUI.changed)
                OnChanged();
            
            VerificationGUI();
        }

        public virtual void OnTargetChanged(){}
        
        public void VerificationGUI()
        {
            if (GUI.changed)
                Verify();

            HelpBox(m_result.ErrorLogs, MessageType.Error);
            HelpBox(m_result.WarningLogs, MessageType.Warning);
            HelpBox(m_result.InfoLogs, MessageType.Info);
        }

        void Verify()
        {
            m_result = VerificationResult.Default;
            if (m_target is IVerify verifyTarget)
                verifyTarget.Verify(ref m_result);
        }

        static void HelpBox(IEnumerable<LogData> logData, MessageType messageType)
        {
            foreach (var data in logData)
                EditorGUILayout.HelpBox($"{data.LogString}", messageType);
        }

        protected virtual void OnChanged()
        {
            //Debug.Log($"On Changed {target}");
            EditorUtility.SetDirty(m_target);
            SerializedObject.Update();
            EventMessenger.TriggerEvent(new ObjectChanged { SourceEditor = this, Changed = m_target });
        }

        public void OnEvent(ObjectChanged eventType)
        {
            if (eventType.Changed != m_target)
                return;
            if (ReferenceEquals(eventType.SourceEditor, this))
                return;
            SerializedObject.Update();
            //Repaint();
        }

        public static void CreateEditor<T>(Object targetObject, ref T previousEditor)
            where T : BaseEditor, new()
        {
            var needEditor = targetObject != null;

            if (previousEditor == null && needEditor)
                previousEditor = new T() { TargetObj = targetObject };
            else if (previousEditor != null && !needEditor)
            {
                previousEditor.Terminate();
                previousEditor = null;
            }
        }

        public void Repaint()
        {
            switch (ParentContainer)
            {
                case EditorWindow w: w.Repaint(); break;
                case UnityEditor.Editor e: e.Repaint(); break;
                case IRepaintable r: r.Repaint(); break;
            }
        }
    }

}