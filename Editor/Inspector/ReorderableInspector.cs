﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using Core.Editor.Extensions;
//using Core.Editor.Utility.GUIStyles;
//using Core.Unity.Attribute;
//using UnityEditor;
//using UnityEditor.Callbacks;
//using UnityEditorInternal;
//using UnityEngine;
//using Object = UnityEngine.Object;

//namespace Core.Editor.Inspector
//{
//    [CustomEditor(typeof(Object), true, isFallback = true)]
//    [CanEditMultipleObjects]
//    public class ReorderableArrayInspector : UnityEditor.Editor
//    {
//        protected static bool m_forceInit;
//        [DidReloadScripts]
//        static void HandleScriptReload()
//        {
//            m_forceInit = true;

//            EditorApplication.delayCall = () => { EditorApplication.delayCall = () => { m_forceInit = false; }; };
//        }

//        static GUIStyle m_styleHighlight;
//        public bool IsSubEditor;

//        readonly GUILayoutOption m_uiExpandWidth = GUILayout.ExpandWidth(true);
//        readonly GUILayoutOption m_uiWidth50 = GUILayout.Width(50);
//        readonly GUIContent m_labelBtnCreate = new GUIContent("Create");
//        GUIStyle m_styleEditBox;

//        readonly List<SortableListData> m_listIndex = new List<SortableListData>();
//        readonly Dictionary<string, UnityEditor.Editor> m_editableIndex = new Dictionary<string, UnityEditor.Editor>();
//        readonly bool m_alwaysDrawInspector = false;
//        bool m_isInitialized;
//        bool m_hasSortableArrays;
//        bool m_hasEditable;

//        protected readonly Dictionary<string, ContextMenuData> ContextData = new Dictionary<string, ContextMenuData>();

//        ~ReorderableArrayInspector()
//        {
//            m_listIndex.Clear();
//            //hasSortableArrays = false;
//            m_editableIndex.Clear();
//            //hasEditable = false;
//            m_isInitialized = false;
//        }

//        #region Initialization

//        void OnEnable() => InitInspector();

//        protected virtual void InitInspector(bool force)
//        {
//            if (force)
//                m_isInitialized = false;
//            InitInspector();
//        }

//        protected virtual void InitInspector()
//        {
//            if (m_isInitialized && m_forceInit == false)
//                return;

//            m_styleEditBox = new GUIStyle(EditorStyles.helpBox) { padding = new RectOffset(5, 5, 5, 5) };

//            FindTargetProperties();
//            m_isInitialized = true;
//            if (m_hasSortableArrays == false)
//                m_listIndex.Clear();

//            FindContextMenu();
//        }

//        protected void FindTargetProperties()
//        {
//            m_listIndex.Clear();
//            m_editableIndex.Clear();
//            var typeScriptable = typeof(ScriptableObject);

//            var propIterator = serializedObject.GetIterator();
//            // This iterator goes through all the child serialized properties, looking
//            // for properties that have the SortableArray attribute
//            if (!propIterator.NextVisible(true))
//                return;
//            do
//            {
//                FindTargetPropertiesArray(propIterator);

//                if (propIterator.propertyType != SerializedPropertyType.ObjectReference)
//                    continue;
//                var propType = propIterator.GetTypeReflection();
//                if (propType == null)
//                    continue;

//                var isScriptable = propType.IsSubclassOf(typeScriptable);
//                if (!isScriptable)
//                    continue;
//#if EDIT_ALL_SCRIPTABLES
//				var makeEditable = true;
//#else
//                var makeEditable = propIterator.HasAttribute<EditScriptableAttribute>();
//#endif
//                if (!makeEditable)
//                    continue;

//                UnityEditor.Editor scriptableEditor = null;
//                if (propIterator.objectReferenceValue != null)
//                {
//                    CreateCachedEditorWithContext(propIterator.objectReferenceValue,
//                        serializedObject.targetObject, null,
//                        ref scriptableEditor);

//                    var reorderable = scriptableEditor as ReorderableArrayInspector;
//                    if (reorderable != null)
//                        reorderable.IsSubEditor = true;
//                }

//                m_editableIndex.Add(propIterator.propertyPath, scriptableEditor);
//                m_hasEditable = true;
//            }
//            while (propIterator.NextVisible(true));
//        }

//        void FindTargetPropertiesArray(SerializedProperty prop)
//        {
//            if (!prop.isArray || prop.propertyType == SerializedPropertyType.String)
//                return;
//#if LIST_ALL_ARRAYS
//			var canTurnToList = true;
//#else
//            var canTurnToList = prop.HasAttribute<ReorderableAttribute>();
//#endif
//            if (!canTurnToList)
//                return;
//            m_hasSortableArrays = true;
//            CreateListData(serializedObject.FindProperty(prop.propertyPath));
//        }

//        static IEnumerable<MethodInfo> GetAllMethods(Type t)
//        {
//            if (t == null)
//                return Enumerable.Empty<MethodInfo>();
//            const BindingFlags binding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
//            return t.GetMethods(binding).Concat(GetAllMethods(t.BaseType));
//        }

//        void FindContextMenu()
//        {
//            ContextData.Clear();

//            // Get context menu
//            var targetType = target.GetType();
//            var contextMenuType = typeof(ContextMenu);
//            var methods = GetAllMethods(targetType).ToArray();
//            for (var index = 0; index < methods.GetLength(0); ++index)
//            {
//                var methodInfo = methods[index];
//                foreach (ContextMenu contextMenu in methodInfo.GetCustomAttributes(contextMenuType, false))
//                    AddContextData(contextMenu, methodInfo);
//            }
//        }

//        void AddContextData(ContextMenu contextMenu, MethodInfo methodInfo)
//        {
//            if (ContextData.ContainsKey(contextMenu.menuItem))
//            {
//                var data = ContextData[contextMenu.menuItem];
//                if (contextMenu.validate)
//                    data.Validate = methodInfo;
//                else
//                    data.Function = methodInfo;
//                ContextData[data.MenuItem] = data;
//            }
//            else
//            {
//                var data = new ContextMenuData(contextMenu.menuItem);
//                if (contextMenu.validate)
//                    data.Validate = methodInfo;
//                else
//                    data.Function = methodInfo;
//                ContextData.Add(data.MenuItem, data);
//            }
//        }

//        void CreateListData(SerializedProperty property)
//        {
//            var parent = property.TopmostParentPath();

//            // Try to find the grand parent in SortableListData
//            var data = m_listIndex.Find(listData => listData.Parent.Equals(parent));
//            if (data == null)
//            {
//                data = new SortableListData(parent);
//                m_listIndex.Add(data);
//            }

//            data.AddProperty(property);
//            var attr = property.GetAttributes<ReorderableAttribute>();
//            if (attr == null || attr.Length != 1)
//                return;
//            var arrayAttr = (ReorderableAttribute)attr[0];
//            if (arrayAttr != null)
//                HandleReorderableOptions(arrayAttr, property, data);
//        }

//        static void HandleReorderableOptions(ReorderableAttribute arrayAttr, SerializedProperty property, SortableListData data)
//        {
//            // Custom element header
//            if (!string.IsNullOrEmpty(arrayAttr.ElementHeader))
//            {
//                data.ElementHeaderCallback = i =>
//                    $"{arrayAttr.ElementHeader} {(arrayAttr.HeaderZeroIndex ? i : i + 1)}";
//            }

//            // Draw property as single line
//            if (!arrayAttr.ElementSingleLine)
//                return;
//            var list = data.GetPropertyList(property);
//            list.elementHeightCallback = index => EditorGUIUtility.singleLineHeight + 6;
//            list.drawElementBackgroundCallback = (rect, index, active, focused) =>
//            {
//                if (focused == false)
//                    return;
//                GUI.Box(rect, GUIContent.none, CustomStyles.SortListStyleHighlight);
//            };

//            list.drawElementCallback = ListDrawElementCallback(property, data);
//        }

//        static ReorderableList.ElementCallbackDelegate ListDrawElementCallback(SerializedProperty property, SortableListData data) =>
//        (rect, index, active, focused) =>
//        {
//            var element = property.GetArrayElementAtIndex(index);
//            element.isExpanded = false;

//            var childCount = data.GetElementCount(property);
//            if (childCount < 1)
//                return;

//            rect.y += 3;
//            rect.height -= 6;

//            if (!element.NextVisible(true))
//                return;
//            var restoreWidth = EditorGUIUtility.labelWidth;
//            EditorGUIUtility.labelWidth /= childCount;

//            const float padding = 5f;
//            var width = rect.width - padding * (childCount - 1);
//            width /= childCount;

//            var childRect = new Rect(rect) { width = width };
//            var depth = element.Copy().depth;
//            do
//            {
//                if (element.depth != depth)
//                    break;

//                if (childCount <= 2)
//                    EditorGUI.PropertyField(childRect, element, false);
//                else
//                    EditorGUI.PropertyField(childRect, element, GUIContent.none, false);
//                childRect.x += width + padding;
//            }
//            while (element.NextVisible(false));

//            EditorGUIUtility.labelWidth = restoreWidth;
//        };

//        /// <summary>
//        /// Given a SerializedProperty, return the automatic ReorderableList assigned to it if any
//        /// </summary>
//        /// <param name="property"></param>
//        /// <returns></returns>
//        protected ReorderableList GetSortableList(SerializedProperty property)
//        {
//            if (m_listIndex.Count == 0)
//                return null;

//            var parent = property.TopmostParentPath();

//            var data = m_listIndex.Find(listData => listData.Parent.Equals(parent));
//            return data?.GetPropertyList(property);
//        }

//        /// <summary>
//        /// Set a drag and drop handler function on a SerializedObject's ReorderableList, if any
//        /// </summary>
//        /// <param name="property"></param>
//        /// <param name="handler"></param>
//        /// <returns></returns>
//        protected bool SetDragDropHandler(SerializedProperty property, Action<SerializedProperty, Object[]> handler)
//        {
//            if (m_listIndex.Count == 0)
//                return false;

//            var parent = property.TopmostParentPath();

//            var data = m_listIndex.Find(listData => listData.Parent.Equals(parent));
//            if (data == null)
//                return false;

//            data.SetDropHandler(property, handler);
//            return true;
//        }
//        #endregion

//        protected bool InspectorGUIStart(bool force = false)
//        {
//            // Not initialized, try initializing
//            if (m_hasSortableArrays && m_listIndex.Count == 0)
//                InitInspector();
//            if (m_hasEditable && m_editableIndex.Count == 0)
//                InitInspector();

//            // No sortable arrays or list index uninitialized
//            var cannotDrawOrderable = (m_hasSortableArrays == false || m_listIndex.Count == 0);
//            var cannotDrawEditable = (m_hasEditable == false || m_editableIndex.Count == 0);
//            if (cannotDrawOrderable && cannotDrawEditable && force == false)
//            {
//                if (IsSubEditor)
//                    DrawPropertiesExcluding(serializedObject, "m_Script");
//                else
//                    base.OnInspectorGUI();

//                DrawContextMenuButtons();
//                return false;
//            }

//            serializedObject.Update();
//            return true;
//        }

//        protected virtual void DrawInspector() => 
//            DrawPropertiesAll();

//        public override void OnInspectorGUI()
//        {
//            if (!InspectorGUIStart(m_alwaysDrawInspector))
//                return;

//            EditorGUI.BeginChangeCheck();

//            DrawInspector();

//            if (EditorGUI.EndChangeCheck())
//            {
//                serializedObject.ApplyModifiedProperties();
//                InitInspector(true);
//            }

//            DrawContextMenuButtons();
//        }

//        protected enum IteratorControl
//        {
//            Draw,
//            Continue,
//            Break
//        }

//        protected void IterateDrawProperty(SerializedProperty property, Func<IteratorControl> filter = null)
//        {
//            if (!property.NextVisible(true))
//                return;
//            // Remember depth iteration started from
//            var depth = property.Copy().depth;
//            do
//            {
//                // If goes deeper than the iteration depth, get out
//                if (property.depth != depth)
//                    break;
//                if (IsSubEditor && property.name.Equals("m_Script"))
//                    continue;

//                if (filter != null)
//                {
//                    var filterResult = filter();
//                    if (filterResult == IteratorControl.Break)
//                        break;
//                    if (filterResult == IteratorControl.Continue)
//                        continue;
//                }

//                DrawPropertySortableArray(property);
//            }
//            while (property.NextVisible(false));
//        }

//        /// <summary>
//        /// Draw a SerializedProperty as a ReorderableList if it was found during
//        /// initialization, otherwise use EditorGUILayout.PropertyField
//        /// </summary>
//        /// <param name="property"></param>
//        protected void DrawPropertySortableArray(SerializedProperty property)
//        {
//            // Try to get the sortable list this property belongs to
//            SortableListData listData = null;
//            if (m_listIndex.Count > 0)
//                listData = m_listIndex.Find(data => property.propertyPath.StartsWith(data.Parent));

//            var isScriptableEditor = m_editableIndex.TryGetValue(property.propertyPath, out var scriptableEditor);

//            // Has ReorderableList
//            if (listData != null)
//            {
//                // Try to show the list
//                if (listData.DoLayoutProperty(property))
//                    return;
//                EditorGUILayout.PropertyField(property, false);
//                if (!property.isExpanded)
//                    return;
//                EditorGUI.indentLevel++;
//                var targetProp = serializedObject.FindProperty(property.propertyPath);
//                IterateDrawProperty(targetProp);
//                EditorGUI.indentLevel--;
//            }
//            // Else try to draw ScriptableObject editor
//            else if (isScriptableEditor)
//            {
//                var hasHeader = property.HasAttribute<HeaderAttribute>();
//                var hasSpace = property.HasAttribute<SpaceAttribute>();

//                float foldoutSpace = hasHeader ? 24 : 7;
//                if (hasHeader && hasSpace)
//                    foldoutSpace = 31;

//                hasSpace |= hasHeader;

//                // No data in property, draw property field with create button
//                if (scriptableEditor == null)
//                {
//                    bool doCreate;
//                    using (new EditorGUILayout.HorizontalScope())
//                    {
//                        EditorGUILayout.PropertyField(property, m_uiExpandWidth);
//                        using (new EditorGUILayout.VerticalScope(m_uiWidth50))
//                        {
//                            if (hasSpace)
//                                GUILayout.Space(10);
//                            doCreate = GUILayout.Button(m_labelBtnCreate, EditorStyles.miniButton);
//                        }
//                    }

//                    if (!doCreate)
//                        return;
//                    var propType = property.GetTypeReflection();
//                    var createdAsset = CreateAssetWithSavePrompt(propType, "Assets");

//                    if (createdAsset == null)
//                        return;
//                    property.objectReferenceValue = createdAsset;
//                    property.isExpanded = true;
//                }
//                // Has data in property, draw foldout and editor
//                else
//                {
//                    EditorGUILayout.PropertyField(property);

//                    var rectFoldout = GUILayoutUtility.GetLastRect();
//                    rectFoldout.width = 20;
//                    if (hasSpace) rectFoldout.yMin += foldoutSpace;

//                    property.isExpanded = EditorGUI.Foldout(rectFoldout, property.isExpanded, GUIContent.none);

//                    if (!property.isExpanded)
//                        return;
//                    EditorGUI.indentLevel++;
//                    using (new EditorGUILayout.VerticalScope(m_styleEditBox))
//                    {
//                        var restoreIndent = EditorGUI.indentLevel;
//                        EditorGUI.indentLevel = 1;
//                        scriptableEditor.serializedObject.Update();
//                        scriptableEditor.OnInspectorGUI();
//                        scriptableEditor.serializedObject.ApplyModifiedProperties();
//                        EditorGUI.indentLevel = restoreIndent;
//                    }
//                    EditorGUI.indentLevel--;
//                }
//            }
//            else
//            {
//                var targetProp = serializedObject.FindProperty(property.propertyPath);

//                var isStartProp = targetProp.propertyPath.StartsWith("m_");
//                using (new EditorGUI.DisabledScope(isStartProp))
//                    EditorGUILayout.PropertyField(targetProp, targetProp.isExpanded);
//            }
//        }

//        // Creates a new ScriptableObject via the default Save File panel
//        static ScriptableObject CreateAssetWithSavePrompt(Type type, string path)
//        {
//            path = EditorUtility.SaveFilePanelInProject("Save ScriptableObject", "New " + type.Name + ".asset", "asset", "Enter a file name for the ScriptableObject.", path);
//            if (path == "")
//                return null;
//            var asset = CreateInstance(type);
//            AssetDatabase.CreateAsset(asset, path);
//            AssetDatabase.SaveAssets();
//            AssetDatabase.Refresh();
//            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
//            EditorGUIUtility.PingObject(asset);
//            return asset;
//        }

//        #region Helper functions
//        /// <summary>
//        /// Draw the default inspector, with the sortable arrays
//        /// </summary>
//        public void DrawPropertiesAll()
//        {
//            var propIterator = serializedObject.GetIterator();
//            IterateDrawProperty(propIterator);
//        }

//        /// <summary>
//        /// Draw the default inspector, except for the given property names
//        /// </summary>
//        /// <param name="propertyNames"></param>
//        public void DrawPropertiesExcept(params string[] propertyNames)
//        {
//            var propIterator = serializedObject.GetIterator();

//            IterateDrawProperty(propIterator,
//                () 
//                    => propertyNames.Contains(propIterator.name)
//                        ? IteratorControl.Continue 
//                        : IteratorControl.Draw);
//        }

//        /// <summary>
//        /// Draw the default inspector, starting from a given property
//        /// </summary>
//        /// <param name="propertyStart">Property name to start from</param>
//        public void DrawPropertiesFrom(string propertyStart)
//        {
//            var canDraw = false;
//            var propIterator = serializedObject.GetIterator();
//            IterateDrawProperty(propIterator,
//                () =>
//                {
//                    if (propIterator.name.Equals(propertyStart))
//                        canDraw = true;
//                    return canDraw 
//                        ? IteratorControl.Draw 
//                        : IteratorControl.Continue;
//                });
//        }

//        /// <summary>
//        /// Draw the default inspector, up to a given property
//        /// </summary>
//        /// <param name="propertyStop">Property name to stop at</param>
//        public void DrawPropertiesUpTo(string propertyStop)
//        {
//            var propIterator = serializedObject.GetIterator();
//            IterateDrawProperty(propIterator,
//                () => propIterator.name.Equals(propertyStop) 
//                    ? IteratorControl.Break 
//                    : IteratorControl.Draw);
//        }

//        /// <summary>
//        /// Draw the default inspector, starting from a given property to a stopping property
//        /// </summary>
//        /// <param name="propertyStart">Property name to start from</param>
//        /// <param name="propertyStop">Property name to stop at</param>
//        public void DrawPropertiesFromUpTo(string propertyStart, string propertyStop)
//        {
//            var canDraw = false;
//            var propIterator = serializedObject.GetIterator();
//            IterateDrawProperty(propIterator,
//                () =>
//                {
//                    if (propIterator.name.Equals(propertyStop))
//                        return IteratorControl.Break;

//                    if (propIterator.name.Equals(propertyStart))
//                        canDraw = true;

//                    return canDraw == false 
//                        ? IteratorControl.Continue 
//                        : IteratorControl.Draw;
//                });
//        }

//        public void DrawContextMenuButtons()
//        {
//            if (ContextData.Count == 0) return;

//            EditorGUILayout.Space();
//            EditorGUILayout.LabelField("Context Menu", EditorStyles.boldLabel);
//            foreach (var kv in ContextData)
//            {
//                var enabledState = GUI.enabled;
//                var isEnabled = true;
//                if (kv.Value.Validate != null)
//                    isEnabled = (bool)kv.Value.Validate.Invoke(target, null);

//                GUI.enabled = isEnabled;
//                if (GUILayout.Button(kv.Key) && kv.Value.Function != null)
//                    kv.Value.Function.Invoke(target, null);

//                GUI.enabled = enabledState;
//            }
//        }
//        #endregion
//    }
//}