﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

#if CORE_ED_INSPECTOR_TRANSFORM
namespace Core.Editor.Inspector
{
    [CustomEditor(typeof(Transform), true)]
    [CanEditMultipleObjects]
    public class CustomTransformInspector : UnityEditor.Editor
    {
        // Unity's built-in editor
        UnityEditor.Editor m_defaultEditor;
        Transform m_transform;

        void OnEnable()
        {
            // When this inspector is created, also create the built-in inspector
            m_defaultEditor = CreateEditor(targets, Type.GetType("UnityEditor.TransformInspector, UnityEditor"));
            m_transform = target as Transform;
        }

        void OnDisable()
        {
            // When OnDisable is called, the default editor we created should be destroyed to avoid memory leakage.
            // Also, make sure to call any required methods like OnDisable
            if (m_defaultEditor == null)
                return;
            var disableMethod = m_defaultEditor.GetType().GetMethod("OnDisable", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if (disableMethod != null)
                disableMethod.Invoke(m_defaultEditor, null);
            DestroyImmediate(m_defaultEditor);
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.LabelField("Local Space", EditorStyles.boldLabel);
            m_defaultEditor.OnInspectorGUI();

            // Show World Space Transform
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("World Space", EditorStyles.boldLabel);

            GUI.enabled = false;
            EditorGUILayout.Vector3Field($"{nameof(Transform.position)}", m_transform.position);
            EditorGUILayout.Vector3Field($"{nameof(Transform.rotation)}", m_transform.rotation.eulerAngles);
            EditorGUILayout.Vector3Field($"scale", m_transform.lossyScale);
            GUI.enabled = true;
        }
    }
}
#endif