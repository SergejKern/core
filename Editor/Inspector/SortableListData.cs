//using System;
//using System.Collections.Generic;
//using Core.Editor.Utility.GUIStyles;
//using UnityEditor;
//using UnityEditorInternal;
//using UnityEngine;
//using Object = UnityEngine.Object;
//using static Core.Editor.Extensions.SerializedPropertyExtensions;

//namespace Core.Editor.Inspector
//{
//    /// <summary>
//    /// Internal class that manages ReorderableLists for each reorderable
//    /// SerializedProperty in a SerializedObject's direct child
//    /// </summary>
//    public class SortableListData
//    {
//        public string Parent { get; }
//        public Func<int, string> ElementHeaderCallback;

//        readonly Dictionary<string, ReorderableList> m_propIndex = new Dictionary<string, ReorderableList>();

//        readonly Dictionary<string, Action<SerializedProperty, Object[]>> m_propDropHandlers =
//            new Dictionary<string, Action<SerializedProperty, Object[]>>();

//        readonly Dictionary<string, int> m_countIndex = new Dictionary<string, int>();

//        public SortableListData(string parent) => Parent = parent;

//        public void AddProperty(SerializedProperty property)
//        {
//            // Check if this property actually belongs to the same direct child
//            if (property.TopmostParentPath().Equals(Parent) == false)
//                return;

//            var propList = new ReorderableList(
//                property.serializedObject, property,
//                true, false,
//                true, true)
//            {
//                headerHeight = 5,
//                drawElementCallback = PropListDrawElementCallback(property),
//                elementHeightCallback = ElementHeightCallback(property),
//                drawElementBackgroundCallback = PropListDrawElementBackgroundCallback(property)
//            };

//            m_propIndex.Add(property.propertyPath, propList);
//        }

//        static ReorderableList.ElementHeightCallbackDelegate ElementHeightCallback(SerializedProperty property) =>
//            index => ElementHeightCallback(property, index);

//        static ReorderableList.ElementCallbackDelegate PropListDrawElementBackgroundCallback(
//            SerializedProperty property) =>
//            (rect, index, active, focused) =>
//            {
//                if (focused == false)
//                    return;
//                rect.height = ElementHeightCallback(property, index);
//                GUI.Box(rect, GUIContent.none, CustomStyles.SortListStyleHighlight);
//            };

//        ReorderableList.ElementCallbackDelegate PropListDrawElementCallback(SerializedProperty property) =>
//            (rect, index, active, focused) =>
//            {
//                var targetElement = property.GetArrayElementAtIndex(index);

//                var isExpanded = targetElement.isExpanded;
//                rect.height = EditorGUI.GetPropertyHeight(targetElement, GUIContent.none, isExpanded);

//                if (targetElement.hasVisibleChildren)
//                    rect.xMin += 10;

//                // Get Unity to handle drawing each element
//                var propHeader = new GUIContent(targetElement.displayName);
//                if (ElementHeaderCallback != null)
//                    propHeader.text = ElementHeaderCallback(index);
//                EditorGUI.PropertyField(rect, targetElement, propHeader, isExpanded);
//            };

//        static float ElementHeightCallback(SerializedProperty property, int index)
//        {
//            var arrayElement = property.GetArrayElementAtIndex(index);
//            var calculatedHeight = EditorGUI.GetPropertyHeight(arrayElement,
//                GUIContent.none,
//                arrayElement.isExpanded);
//            calculatedHeight += 3;
//            return calculatedHeight;
//        }

//        public bool DoLayoutProperty(SerializedProperty property)
//        {
//            if (m_propIndex.ContainsKey(property.propertyPath) == false)
//                return false;

//            // Draw the header
//            var headerText = $"{property.displayName} [{property.arraySize}]";
//            EditorGUILayout.PropertyField(property, new GUIContent(headerText), false);

//            // Save header rect for handling drag and drop
//            var dropRect = GUILayoutUtility.GetLastRect();

//            // Draw the reorderable list for the property
//            if (property.isExpanded)
//            {
//                var newArraySize = EditorGUILayout.IntField("Size", property.arraySize);
//                if (newArraySize != property.arraySize)
//                    property.arraySize = newArraySize;
//                m_propIndex[property.propertyPath].DoLayoutList();
//            }

//            // Handle drag and drop into the header
//            var evt = Event.current;
//            if (evt == null)
//                return true;

//            if (evt.type != EventType.DragUpdated && evt.type != EventType.DragPerform)
//                return true;
//            if (dropRect.Contains(evt.mousePosition) == false)
//                return true;

//            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
//            if (evt.type != EventType.DragPerform)
//                return true;
//            DragAndDrop.AcceptDrag();
//            if (m_propDropHandlers.TryGetValue(property.propertyPath, out var handler))
//            {
//                handler?.Invoke(property, DragAndDrop.objectReferences);
//            }
//            else
//            {
//                foreach (var dragged_object in DragAndDrop.objectReferences)
//                {
//                    if (dragged_object.GetType() != property.GetType())
//                        continue;

//                    var newIndex = property.arraySize;
//                    property.arraySize++;

//                    var target = property.GetArrayElementAtIndex(newIndex);
//                    target.objectReferenceInstanceIDValue = dragged_object.GetInstanceID();
//                }
//            }

//            evt.Use();
//            return true;
//        }

//        public int GetElementCount(SerializedProperty property)
//        {
//            if (property.arraySize <= 0)
//                return 0;

//            if (m_countIndex.TryGetValue(property.propertyPath, out var count))
//                return count;

//            var element = property.GetArrayElementAtIndex(0);
//            var countElement = element.Copy();
//            var childCount = 0;
//            if (countElement.NextVisible(true))
//            {
//                var depth = countElement.Copy().depth;
//                do
//                {
//                    if (countElement.depth != depth)
//                        break;
//                    childCount++;
//                } while (countElement.NextVisible(false));
//            }

//            m_countIndex.Add(property.propertyPath, childCount);
//            return childCount;
//        }

//        public ReorderableList GetPropertyList(SerializedProperty property)
//        {
//            return m_propIndex.ContainsKey(property.propertyPath)
//                ? m_propIndex[property.propertyPath]
//                : null;
//        }

//        public void SetDropHandler(SerializedProperty property, Action<SerializedProperty, Object[]> handler)
//        {
//            var path = property.propertyPath;
//            if (m_propDropHandlers.ContainsKey(path))
//                m_propDropHandlers[path] = handler;
//            else
//                m_propDropHandlers.Add(path, handler);
//        }
//    }
//}