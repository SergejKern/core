﻿using Core.Editor.Interface;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Inspector
{
    [CustomEditor(typeof(Object), true, isFallback = true)]
    [CanEditMultipleObjects]
    public class BaseInspector : BaseInspector<BaseEditor> {}

    public abstract class BaseInspector<T> : UnityEditor.Editor, IRepaintable where T: IInspectorEditor, new ()
    {
        public virtual bool CanDetach => true;

        T m_editor;
        float m_currentWidth;

        public virtual void OnEnable()
        {
            m_editor = new T() { TargetObj = target };
            m_editor.Init(this);
        }

        public virtual void OnDisable() => m_editor?.Terminate();

        public override void OnInspectorGUI()
        {
            if (m_editor == null)
                return;
            DrawDetachButton();

            var ctrlRect = EditorGUILayout.GetControlRect(false, 1f);
            if (Event.current.type == EventType.Repaint)
                m_currentWidth = ctrlRect.width;

            m_editor.TargetObj = target;

            //Debug.Log($"{Event.current} {ctrlRect.width} {EditorGUIUtility.currentViewWidth}");
            m_editor.OnGUI(m_currentWidth);
        }
        
        protected void DrawDetachButton()
        {
            if (!CanDetach)
                return;
            if (!GUILayout.Button("Detach", EditorStyles.miniButton, GUILayout.Width(75)))
                return;
            OpenDetached();
        }

        protected virtual void OpenDetached() => BaseEditorWindow.OpenWindow(new T() { TargetObj = target });

        public new static void DrawPropertiesExcluding(SerializedObject obj,
            params string[] propertyToExclude) =>
            UnityEditor.Editor.DrawPropertiesExcluding(obj, propertyToExclude);
    }

}