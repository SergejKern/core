﻿using System.Collections.Generic;
using System.Linq;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Tools
{
    public class DependencyViewer : Singleton<DependencyViewer>
    {
        const int k_maxRecursion = 20;

        struct FoldOutGroupMain
        {
            public Object Asset;
            public bool Expanded;
            public FoldOutGroupMain[] Group;
        }
        struct FoldOutGroupAssetList
        {
            public Object[] Asset;
            public int[] ReferenceCount;

            public bool Expanded;
        }

        Object m_activeObject;
        readonly List<Object> m_subSelection = new List<Object>();

        FoldOutGroupMain m_mainHierarchy;
        FoldOutGroupAssetList m_multiReferenced;
        FoldOutGroupAssetList m_lostAssets;

        HideFlags m_flags;

        public void DrawGUI(Object activeObject)
        {
            if (m_activeObject != activeObject) 
                BuildFoldOutGroups(activeObject);

            DrawHierarchy();
            DrawSelectedAssetOptions();
        }

        void DrawSelectedAssetOptions()
        {
            if (GUILayout.Button("Select All"))
            {
                var allSubAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(m_activeObject))
                    .Where(asset => asset != m_mainHierarchy.Asset);

                m_subSelection.AddRange(allSubAssets);
            }            
            if (GUILayout.Button("Deselect All"))
                m_subSelection.Clear();

            if (m_subSelection.IsNullOrEmpty())
                return;
            m_flags = (HideFlags) EditorGUILayout.EnumFlagsField("HideFlags: ", m_flags);
            if (GUILayout.Button("Apply HideFlags"))
            {
                foreach (var asset in m_subSelection)
                {
                    asset.hideFlags = m_flags;
                    EditorUtility.SetDirty(asset);
                }
                EditorUtility.SetDirty(m_mainHierarchy.Asset);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(m_mainHierarchy.Asset));
            }

            if (GUILayout.Button("Apply Default Name"))
            {
                foreach (var asset in m_subSelection)
                {
                    asset.DefaultName();
                    EditorUtility.SetDirty(asset);
                }
                EditorUtility.SetDirty(m_mainHierarchy.Asset);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(m_mainHierarchy.Asset));
            }

            if (GUILayout.Button("Fix Asset Name"))
            {
                foreach (var asset in m_subSelection)
                {
                    if (!(asset is IScriptableObject so))
                        continue;

                    so.TryFixAssetName();
                    EditorUtility.SetDirty(asset);
                }
                EditorUtility.SetDirty(m_mainHierarchy.Asset);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(m_mainHierarchy.Asset));
            }

            if (GUILayout.Button("Remove Sub Asset"))
            {
                foreach (var asset in m_subSelection)
                    asset.DestroyEx();

                // force re-init:
                m_activeObject = null;
            }

        }

        void BuildFoldOutGroups(Object activeObject)
        {
            m_activeObject = activeObject;
            m_mainHierarchy = default;
            m_multiReferenced = default;
            m_lostAssets = default;

            var mainAsset = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(activeObject));
            var allAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(activeObject));

            if (!(mainAsset is IDependencies))
            {
                m_mainHierarchy = new FoldOutGroupMain {Asset = mainAsset};
                return;
            }

            using (var assetRefsScoped = SimplePool<Dictionary<Object, int>>.I.GetScoped())
            {
                var hierarchy = SubAssetHierarchy.GetSubAssetHierarchy(assetRefsScoped.Obj, allAssets, mainAsset);
                m_mainHierarchy = BuildFoldOutGroup(hierarchy);

                var assetRefs = assetRefsScoped.Obj;

                var multiReferenced = assetRefs.Where(a => a.Value > 1);
                var lostAssets = assetRefs.Where(a => a.Value == 0);

                m_multiReferenced = new FoldOutGroupAssetList
                {
                    Asset = multiReferenced.Select(a => a.Key).ToArray(),
                    ReferenceCount = multiReferenced.Select(a => assetRefs[a.Key]).ToArray(),

                    Expanded = false,
                };

                m_lostAssets = new FoldOutGroupAssetList
                {
                    Asset = lostAssets.Select(a => a.Key).ToArray(),
                    ReferenceCount = lostAssets.Select(a => assetRefs[a.Key]).ToArray(),

                    Expanded = false,
                };
            }
        }

        static void CountRefs(Object asset, IDictionary<Object, int> refs, int recurseCount = 0)
        {
            if (recurseCount > k_maxRecursion)
            {
                Debug.LogError($"{nameof(CountRefs)} recursive error");
                return;
            }

            if (!(asset is IDependencies dep))
            {
                Debug.LogWarning($"{asset} does not implement IDependencies");
                return;
            }            

            foreach (var d in dep.Dependencies)
            {
                if (ReferenceEquals(d, dep))
                {
                    Debug.LogWarning($"{d} == {dep} ReferenceEquals");
                    continue;
                }

                if (!refs.ContainsKey(d))
                {
                    Debug.LogWarning($"{d} !refs.ContainsKey");
                    continue;
                }

                refs.TryGetValue(d, out var prevValue);
                refs[d]++;

                if (prevValue == 0)
                    CountRefs(d, refs, recurseCount+1);
            }
        }

        static FoldOutGroupMain BuildFoldOutGroup(SubAssetHierarchy.SubAssetData hierarchy)
        {
            var dependencies = BuildFoldOutGroups(hierarchy.SubAssets);
            return new FoldOutGroupMain
            {
                Asset = hierarchy.Asset,
                Expanded = false,
                Group = dependencies
            };
        }

        static FoldOutGroupMain[] BuildFoldOutGroups(IReadOnlyList<SubAssetHierarchy.SubAssetData> subAssets)
        {
            var subGroups = new FoldOutGroupMain[subAssets.Count];
            for (var i = 0; i < subGroups.Length; i++) 
                subGroups[i] = BuildFoldOutGroup(subAssets[i]);
            return subGroups;
        }

        void DrawHierarchy()
        {
            EditorGUILayout.LabelField("Dependency-Viewer: ");

            if (!(m_mainHierarchy.Asset is IDependencies))
            {
                EditorGUILayout.HelpBox("Main Asset does not implement IDependencies", MessageType.Info);
                return;
            }

            DrawHierarchy(ref m_mainHierarchy);

            if (m_multiReferenced.Asset.Length > 0) 
                DrawAssetList(ref m_multiReferenced, "multiple referenced");

            if (m_lostAssets.Asset.Length > 0)
            {
                EditorGUILayout.HelpBox("Contains Lost sub-assets!", MessageType.Warning);
                using (new ColorScope(Color.red))
                    DrawAssetList(ref m_lostAssets, "Lost sub-assets");
            }
        }

        void DrawAssetList(ref FoldOutGroupAssetList al, string title)
        {
            al.Expanded = EditorGUILayout.Foldout(al.Expanded, title, true);
            if (!al.Expanded)
                return;
            using (new GUILayout.VerticalScope(EditorStyles.helpBox))
            {
                for (var i = 0; i < al.Asset.Length; i++)
                {
                    using (new GUILayout.HorizontalScope())
                    {
                        var asset = al.Asset[i];
                        var refs = al.ReferenceCount[i];

                        DrawSelectionToggle(asset);
                        if (asset is INamed named)
                            EditorGUILayout.LabelField($"{named.Name}");
                        else EditorGUILayout.LabelField("-");

                        EditorGUILayout.ObjectField(asset, typeof(Object), false);
                        EditorGUILayout.LabelField($"refs: {refs}");
                    }
                }
            }
        }

        void DrawHierarchy(ref FoldOutGroupMain[] foldOutGroups, int recurseCount = 0)
        {
            for (var i = 0; i < foldOutGroups.Length; i++) 
                DrawHierarchy(ref foldOutGroups[i], recurseCount);
        }

        void DrawHierarchy(ref FoldOutGroupMain foldOutGroup, int recurseCount = 0)
        {
            if (recurseCount > 10)
            {
                Debug.LogError($"{nameof(DrawHierarchy)} recursive error");
                return;
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                DrawSelectionToggle(foldOutGroup.Asset);
                foldOutGroup.Expanded = !foldOutGroup.Group.IsNullOrEmpty() 
                                        && EditorGUILayout.Toggle(foldOutGroup.Expanded, EditorStyles.foldout, GUILayout.Width(20f));

                if (foldOutGroup.Asset is INamed named)
                    EditorGUILayout.LabelField($"{named.Name}");
                else EditorGUILayout.LabelField("-");

                EditorGUILayout.ObjectField(foldOutGroup.Asset, typeof(Object), false);

                if (!foldOutGroup.Expanded)
                    return;
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(20f);

                using (new EditorGUILayout.VerticalScope())
                {
                    DrawHierarchy(ref foldOutGroup.Group, recurseCount+1);
                }
            }
        }
        void DrawSelectionToggle(Object asset)
        {
            var selected = m_subSelection.Contains(asset);
            var newSelected = EditorGUILayout.Toggle(m_subSelection.Contains(asset), GUILayout.Width(20f));
            if (selected == newSelected) 
                return;

            if (newSelected)
                m_subSelection.Add(asset);
            else
                m_subSelection.Remove(asset);
        }
    }

    public static class SubAssetHierarchy
    {
        const int k_maxRecursion = 20;
        public struct SubAssetData
        {
            public Object Asset;
            public SubAssetData[] SubAssets;
        }

        public static SubAssetData GetSubAssetHierarchy(Object activeObject)
        {
            var mainAsset = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(activeObject));
            var allAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(activeObject));

            if (!(mainAsset is IDependencies))
                return new SubAssetData {Asset = mainAsset};

            using (var assetRefsScoped = SimplePool<Dictionary<Object, int>>.I.GetScoped())
                return GetSubAssetHierarchy(assetRefsScoped.Obj, allAssets, mainAsset);
        }

        public static SubAssetData GetSubAssetHierarchy(Dictionary<Object, int> assetRefs, 
            IEnumerable<Object> allAssets, Object mainAsset)
        {
            foreach (var asset in allAssets)
                assetRefs.Add(asset, 0);

            CountRefs(mainAsset, assetRefs);

            assetRefs[mainAsset]++;
            return BuildHierarchy(mainAsset, assetRefs);
        }

        public static FindResult TryFindSubAssetInHierarchy(Object toFind,
            SubAssetData hierarchy, out SubAssetData found)
        {
            found = default;
            if (hierarchy.Asset == toFind)
            {
                found = hierarchy;
                return FindResult.Found;
            }
            foreach (var subHierarchy in hierarchy.SubAssets)
            {
                if (TryFindSubAssetInHierarchy(toFind, subHierarchy, out found) == FindResult.Found)
                    return FindResult.Found;
            }
            return FindResult.NotFound;
        }

        static void CountRefs(Object asset, IDictionary<Object, int> refs, int recurseCount = 0)
        {
            if (recurseCount > k_maxRecursion)
                return;
            if (!(asset is IDependencies dep))
                return;

            foreach (var d in dep.Dependencies)
            {
                if (ReferenceEquals(d, dep))
                    continue;
                // ignore dependencies that are part of other main-assets
                if (!refs.ContainsKey(d))
                    continue;

                refs.TryGetValue(d, out var prevValue);
                refs[d]++;

                if (prevValue == 0)
                    CountRefs(d, refs, recurseCount+1);
            }
        }

        static SubAssetData BuildHierarchy(Object asset, IReadOnlyDictionary<Object, int> assetRefs, int recurseCount = 0)
        {
            if (recurseCount > k_maxRecursion)
                return default;

            var getDependencies = asset is IDependencies;
            if (!assetRefs.TryGetValue(asset, out var val))
                getDependencies = false;

            var hierarchical = val == 1;
            if (!hierarchical)
                getDependencies = false;

            SubAssetData[] subAssets = null;

            if (getDependencies) 
                subAssets = GetSubAssets(asset, assetRefs, recurseCount);

            return new SubAssetData
            {
                Asset = asset,
                SubAssets = subAssets
            };
        }

        static SubAssetData[] GetSubAssets(Object asset, IReadOnlyDictionary<Object, int> assetRefs, int recurseCount)
        {
            var dep = (IDependencies) asset;
            var dependencies = new SubAssetData[dep.Dependencies.Count(d => d != asset)];
            var i = 0;
            foreach (var d in dep.Dependencies)
            {
                if (d == asset)
                    continue;

                dependencies[i] = BuildHierarchy(d, assetRefs, recurseCount+1);
                ++i;
            }

            return dependencies;
        }
    }
}
