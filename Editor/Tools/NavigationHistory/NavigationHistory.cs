#if CORE_ED_NAV_HISTORY
using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Types;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

// todo 3: Fix why objects get cleaned up f.e. after recompile (don't remove but debug path etc)
// todo 2: improve performance,
namespace Core.Editor.Tools.NavigationHistory
{
    [InitializeOnLoad]
    public class NavigationHistory 
    {
        public static bool IsFolder(string path) { return path.IndexOf(".", StringComparison.Ordinal) == -1; }
        public class NavigationHistoryObject
        {
            public Object UEObject;
            public bool IsPinned;
            public string AssetPath;
            public Texture2D Icon;
            public bool IsAsset() => AssetPath.Length > 0;
            public bool IsNonFolderAsset() => IsAsset() && !NavigationHistory.IsFolder(AssetPath);
            public bool IsFolder() => IsAsset() && NavigationHistory.IsFolder(AssetPath);

            public NavigationHistoryObject(Object ueObj)
            {
                UEObject = ueObj;
                AssetPath = AssetDatabase.GetAssetPath(UEObject);
                Icon = AssetDatabase.GetCachedIcon(AssetPath) as Texture2D;
                if (Icon != null)
                    return;
                if (!IsAsset()) 
                    Icon = EditorGUIUtility.ObjectContent(null, typeof(GameObject)).image as Texture2D;
            }

            public override bool Equals(object o) => o is NavigationHistoryObject navObj && UEObject.Equals(navObj.UEObject);

            // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
            public override int GetHashCode() => base.GetHashCode();
        }
    
        const int k_historySize = 10;
    
        public static int PinnedElements;

        public static bool FilterSceneObj = false;
        public static bool FilterAssets = false;
        public static bool FilterFolders = false;

        public static List<NavigationHistoryObject> History;
        public static int SelectedId=-1;

        static string m_lastGuid = "";
        static Object m_lastObject;
        static Object[] m_onInitObjects;

        static bool m_doNavigateBack;
        static bool m_doNavigateForward;

        public static Action DrawAction = null;

        static NavigationHistory()
        {
            EditorApplication.delayCall += Init;
        }

        static bool m_initialized;
        public static void Init()
        {
            History = new List<NavigationHistoryObject>();
            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyChangeListener;

            EditorSceneManager.sceneOpened += OnSceneOpened;
            SceneView.duringSceneGui += OnSceneGUI;
            m_onInitObjects = Selection.objects;

            if (EditorApplication.isPlayingOrWillChangePlaymode || !m_initialized)
            {
                Selection.activeObject = null;
                Load();
            }
            SelectedId = 0;
            m_initialized = true;
        }

        public static string GetHierarchicalPath(GameObject go)
        {
            if (go == null)
                return "";
            var path = go.name;

            var tr = go.transform;
            while (tr.parent != null)
            {
                tr = tr.parent;
                path = tr.name + "/" + path;
            }
            return path;
        }
        public static void Save(string key = "")
        {
            var serializedString="";
            var addComma = false;
            foreach (var t in History)
            {
                if (t.UEObject == null)
                    continue;
                
                if (addComma)
                    serializedString += ";";
                
                serializedString += t.UEObject.GetInstanceID() + ";";
                serializedString += GetHierarchicalPath(t.UEObject as GameObject) + ";";
                serializedString += t.IsPinned.ToString();
                addComma = true;
            }

            EditorPrefs.SetString("NavigationHistory" + key, serializedString);
        }

        public static void Load(string key = "")
        {
            if (!EditorPrefs.HasKey("NavigationHistory" + key))
                return;
        
            ClearAll();
            var serializedString = EditorPrefs.GetString("NavigationHistory" + key);
            var splitString = serializedString.Split(';');
            for (var i = 0; i < splitString.Length; i+=3)
            {
                if (!int.TryParse(splitString[i], out var instanceID))
                {
                    Debug.LogError("NavigationHistory.Load: parsing error!");
                    continue;
                }
                var ueObj = EditorUtility.InstanceIDToObject(instanceID);
                if (ueObj == null)
                {
                    ueObj = GameObject.Find(splitString[i + 1]);
                    if (ueObj == null)
                        continue;
                }
                var nho = new NavigationHistoryObject(ueObj);
                if (!bool.TryParse(splitString[i + 2], out var pin))
                {
                    Debug.LogError("NavigationHistory.Load: parsing error!");
                    return;
                }
                nho.IsPinned = pin;
                History.Add(nho);
            }

            if (m_onInitObjects!= null)
                Selection.objects = m_onInitObjects;
        }

        public static void ClearAll()
        {
            History.Clear();
            PinnedElements = 0;
            SelectedId = -1;
        }

        public static void UnpinAll()
        {
            var i = History.Count - 1;
            while (i >= 0)
            {
                UnPinAt(i);
                i--;
            }
        }

        public static void ClearUnpinned(int from = 0)
        {
            var i = History.Count - 1;
            while (i >= from)
            {
                if (!History[i].IsPinned)
                    RemoveAt(i);
                
                i--;
            }
        }

        // ReSharper disable once FlagArgument
        public static void ChangePinStateAt(int i, bool pin)
        {
            if (History[i].IsPinned == pin) return;
            PinnedElements += pin ? 1 : -1;
            History[i].IsPinned = pin;
            Save();
        }

        public static void PinAt(int i) => ChangePinStateAt(i, true);

        public static void UnPinAt(int i) => ChangePinStateAt(i, false);

        public static void RemoveAt(int i)
        {
            UnPinAt(i);
            History.RemoveAt(i);
            if (SelectedId > i)
                SelectedId--;
            else if (SelectedId == i)
            {
                //Selection.activeObject = null;
                SelectedId = History.Count;
            }
        }

        public static bool RemoveIfNullAt(int i)
        {
            if (History.Count <= i || History[i].UEObject != null)
                return false;
            RemoveAt(i);
            return true;
        }

        public static void CleanupHistory()
        {
            var i = 0;
            for (; i < History.Count; )
            {
                if (!RemoveIfNullAt(i))
                    ++i;
            }
        }

        [MenuItem(MenuStrings.NavigationWindow)]
        static void MenuCall() => EditorWindow.GetWindow<NavigationHistoryWindow>(false, "Navigation History");

        [MenuItem(MenuStrings.NavigateBackward)]
        static void NavigateBack()
        {
            if (SelectedId != 0)
                m_doNavigateBack = true;
        }

        [MenuItem(MenuStrings.NavigateForward)]
        static void NavigateForth()
        {
            if (SelectedId != (History.Count-1))
                m_doNavigateForward = true;
        }

        static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            CleanupHistory();
            Load();
        }

        // todo 3: do this not on sceneGUI only (confusing), either add more hooks (eg. nav-window update/gui)
        // todo 3: or add non-saved editor-helper-update-object
        static void OnSceneGUI(SceneView sceneView)
        {
            if (m_doNavigateBack && !FirstSelected())
            {
                m_doNavigateBack = false;
                SelectPrevious();
            }

            if (m_doNavigateForward && !LastSelected())
            {
                m_doNavigateForward = false;
                SelectNext();
            }
            // RepaintAllViews is extremely expensive and should not be called frequently.
            //UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
        }

        public static OperationResult ShowFolderContentsWithReflection(int i)
        {
            var projectBrowserType = Type.GetType("UnityEditor.ProjectBrowser,UnityEditor");
            if (projectBrowserType == null)
            {
                Debug.LogError("Can't find UnityEditor.ProjectBrowser type!");
                return OperationResult.Error;
            }

            var lastProjectBrowser = projectBrowserType.GetField("s_LastInteractedProjectBrowser",
                BindingFlags.Static | BindingFlags.Public);
            if (lastProjectBrowser == null)
            {
                Debug.LogError("Can't find s_LastInteractedProjectBrowser field!");
                return OperationResult.Error;
            }

            var lastProjectBrowserInstance = lastProjectBrowser.GetValue(null);
            var projectBrowserViewMode =
                projectBrowserType.GetField("m_ViewMode", BindingFlags.Instance | BindingFlags.NonPublic);

            if (projectBrowserViewMode == null)
            {
                Debug.LogError("Can't find m_ViewMode field!");
                return OperationResult.Error;
            }

            // 0 - one column, 1 - two column
            var viewMode = (int) projectBrowserViewMode.GetValue(lastProjectBrowserInstance);
            if (viewMode != 1)
                return OperationResult.Error;

            var showFolderContents = projectBrowserType.GetMethod("ShowFolderContents",
                BindingFlags.NonPublic | BindingFlags.Instance);

            if (showFolderContents == null)
            {
                Debug.LogError("Can't find ShowFolderContents method!");
                return OperationResult.Error;
            }

            var folder = History[i].UEObject;
            showFolderContents.Invoke(lastProjectBrowserInstance,
                new object[] {folder.GetInstanceID(), true});
            return OperationResult.OK;
        }

        public static void SelectObject(int i)
        {
            if (!SelectionValidAt(ref i))
                return;

            var folderContentsShown = false;
            if (History[i].IsAsset() && History[i].IsFolder())
                folderContentsShown = ShowFolderContentsWithReflection(i) == OperationResult.OK;
            if (!folderContentsShown)
                Selection.activeObject = History[i].UEObject;
            
            SelectedId = i;
        }

        static bool SelectionValidAt(ref int i)
        {
            while (RemoveIfNullAt(i))
            {
                if (History.Count <= i)
                    i--;
            }

            return History.Count > i;
        }

        public static void SelectNext()
        {
            if (SelectedId >= (History.Count-1))
                return;

            SelectedId++;
            SelectObject(SelectedId);
        }

        public static void SelectPrevious()
        {
            if (SelectedId <= 0)
                return;

            SelectedId--;
            SelectObject(SelectedId);
        }

        public static bool FirstSelected() => SelectedId <= 0;

        public static bool LastSelected() => SelectedId >= (History.Count - 1);

        static void OnHierarchyChangeListener(int instanceID, Rect selectionRect) => CheckSelection();

        static void CheckSelection()
        {
            if (FilterSceneObj && FilterAssets && FilterFolders)
                return;

            GetActiveObj(out var activeObj);

            var lastInHistory = SelectedId >= 0 && 
                                 History.Count > SelectedId &&
                                 activeObj == History[SelectedId].UEObject;

            if (activeObj != null && !lastInHistory)
            {
                if (!UpdateHistoryForSelection(activeObj))
                    return;
            }

            DrawAction?.Invoke();
        }

        // returns true if drawing is ok?
        static bool UpdateHistoryForSelection(Object activeObj)
        {
            var nho = new NavigationHistoryObject(activeObj);

            var filterA = FilterAssets && nho.IsNonFolderAsset();
            var filterF = FilterFolders && nho.IsFolder();
            var filterS = FilterSceneObj && !nho.IsAsset();
            if (filterA || filterF || filterS)
                return false;

            ClearUnpinned(SelectedId + 1);
            var alreadyInListIdx = History.IndexOf(nho);
            if (alreadyInListIdx != -1)
            {
                if (History[alreadyInListIdx].IsPinned)
                {
                    SelectedId = History.Count;
                    return false;
                }

                History.RemoveAt(alreadyInListIdx);
            }

            History.Add(nho);
            if (History.Count > (k_historySize + PinnedElements))
            {
                for (var i = 0; i < History.Count; ++i)
                {
                    if (History[i].IsPinned)
                        continue;
                    History.RemoveAt(i);
                    break;
                }
            }

            SelectedId = History.Count - 1;
            Save();
            return true;
        }

        static void GetActiveObj(out Object activeObj)
        {
            activeObj = null;
            if (Selection.activeObject != m_lastObject)
            {
                m_lastObject = Selection.activeObject;
                activeObj = m_lastObject;
                if (Selection.assetGUIDs.Length > 0)
                    m_lastGuid = Selection.assetGUIDs[0];
                return;
            }

            if (Selection.assetGUIDs.Length <= 0 || m_lastGuid == Selection.assetGUIDs[0])
                return;

            // two column folder changed!
            m_lastGuid = Selection.assetGUIDs[0];

            var path = AssetDatabase.GUIDToAssetPath(m_lastGuid);
            if (IsFolder(path))
                activeObj = AssetDatabase.LoadAssetAtPath(path, typeof(Object));
        }
    }
}

#endif