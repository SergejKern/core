#if CORE_ED_NAV_HISTORY

using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Tools.NavigationHistory
{
    public class NavigationHistoryWindow : EditorWindow
    {
        GUIStyle m_boldButton;
        GUIStyle m_normalButton;
        GUIStyle m_iconStyle;

        Vector2 m_scrollPosition;
        bool m_cancelNextRepaint;

        static bool m_reInitStyles;

        public void OnEnable()
        {
            m_reInitStyles = true;

            NavigationHistory.DrawAction = RepaintListener;
        }

        public void OnDisable()
        {
            NavigationHistory.DrawAction = null;
            m_boldButton = null;
            m_normalButton = null;
            m_iconStyle = null;
        }

        [UnityEditor.Callbacks.DidReloadScripts]
        static void OnScriptsReloaded() => m_reInitStyles = true;

        public void RepaintListener() => Repaint();

        void InitStyles()
        {
            m_iconStyle = new GUIStyle();
            m_normalButton = new GUIStyle(EditorStyles.miniButton) {alignment = TextAnchor.MiddleLeft};
            m_boldButton = new GUIStyle(m_normalButton)
            {
                fontStyle = FontStyle.Bold, normal = {textColor = new Color(0.5f, 1.0f, 0.0f, 1.0f)}
            };
        }

        void Update()
        {
            if (NavigationHistory.History == null)
                return;
            
            NavigationHistory.CleanupHistory();
        }

        void OnGUI()
        {
            if (!GUIValid())
                return;

            //EditorGUI.BeginDisabledGroup(Application.isPlaying);
            OnFilterBarGUI();
            OnSaveLoadBarGUI();
            OnScrollViewGUI();
            //EditorGUI.EndDisabledGroup();
        }

        void OnScrollViewGUI()
        {
            using (var scope = new EditorGUILayout.ScrollViewScope(m_scrollPosition))
            {
                m_scrollPosition = scope.scrollPosition;
                for (var i = 0; i < NavigationHistory.History.Count; i++)
                {
                    OnNavElementGUI(i);
                }
            }
        }

        void OnNavElementGUI(int i)
        {
            GetNavData(i, out var icon, out var navName, out var assetPath, out var isPinned);
            using (new EditorGUILayout.HorizontalScope())
            {
                if (NavigationHistory.History[i].Icon != null)
                {
                    m_iconStyle.normal.background = icon;
                    GUILayout.Box(GUIContent.none, m_iconStyle, GUILayout.Height(16), GUILayout.Width(16));
                    m_iconStyle.normal.background = null;
                }

                using (new EditorGUI.DisabledGroupScope(i == NavigationHistory.SelectedId))
                {
                    var content = new GUIContent(navName, assetPath);
                    if (GUILayout.Button(content, i == NavigationHistory.SelectedId ? m_boldButton : m_normalButton))
                    {
                        NavigationHistory.SelectObject(i);
                        m_cancelNextRepaint = true;
                    }
                }

                NavigationHistory.ChangePinStateAt(i, (GUILayout.Toggle(isPinned, "Pin", m_normalButton, GUILayout.Width(32))));

                if (GUILayout.Button("X", m_normalButton, GUILayout.Width(22)))
                {
                    NavigationHistory.RemoveAt(i);
                    m_cancelNextRepaint = true;
                }
            }
        }

        static void GetNavData(int i, out Texture2D icon, out string navName, out string assetPath, out bool isPinned)
        {
            icon = null; navName = ""; assetPath = ""; isPinned = false;

            if (!i.IsInRange(NavigationHistory.History) || NavigationHistory.History[i].UEObject == null)
                return;

            icon = NavigationHistory.History[i].Icon;
            navName = NavigationHistory.History[i].UEObject.name;
            assetPath = NavigationHistory.History[i].AssetPath;
            isPinned = NavigationHistory.History[i].IsPinned;
        }

        static void OnSaveLoadBarGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                var load = Event.current.alt;
                for (var i = 0; i <= 9; i++)
                {
                    if (!GUILayout.Button(new GUIContent("" + i, "click to save to slot!\npress alt to load from this slot"),
                        GUILayout.Width(25)))
                        continue;
                    if (load)
                        NavigationHistory.Load("." + i);
                    else
                        NavigationHistory.Save("." + i);
                }
            }
        }

        void OnFilterBarGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                //GUI.enabled = !NavigationHistory.FirstSelected();
                //if (GUILayout.Button("<<", GUILayout.Width(35)))
                //{
                //    NavigationHistory.SelectPrevious();
                //}
                //GUI.enabled = true;

                NavigationHistory.FilterSceneObj =
                    !GUILayout.Toggle(!NavigationHistory.FilterSceneObj, "Scene", m_normalButton);
                NavigationHistory.FilterAssets = !GUILayout.Toggle(!NavigationHistory.FilterAssets, "Assets", m_normalButton);
                NavigationHistory.FilterFolders =
                    !GUILayout.Toggle(!NavigationHistory.FilterFolders, "Folders", m_normalButton);
                if (GUILayout.Button("Unpin All", m_normalButton))
                {
                    NavigationHistory.UnpinAll();
                    m_cancelNextRepaint = true;
                }

                if (GUILayout.Button("Clear Unpinned", m_normalButton))
                {
                    NavigationHistory.ClearUnpinned();
                    m_cancelNextRepaint = true;
                }

                //GUI.enabled = !NavigationHistory.LastSelected();
                //if (GUILayout.Button(">>", GUILayout.Width(35)))
                //{
                //    NavigationHistory.SelectNext();
                //}
                //GUI.enabled = true;
            }
        }

        bool GUIValid()
        {
            if (Event.current.type == EventType.Repaint)
            {
                if (m_cancelNextRepaint)
                {
                    m_cancelNextRepaint = false;
                    Repaint();
                    GUIUtility.ExitGUI();
                    return false;
                }
            }

            if (NavigationHistory.History == null)
                return false;

            if (m_normalButton == null || m_reInitStyles)
                InitStyles();

            return true;
        }
    }
}
#endif