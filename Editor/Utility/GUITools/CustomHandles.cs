using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.GUITools
{
    public static class CustomHandles 
    {
        public static Vector3 PositionHandle(Vector3 pos, Quaternion rot, 
            Color x, Color y, Color z, 
            float size = 1f)
        {
            var cap = new Handles.CapFunction(Handles.ArrowHandleCap);
            size = HandleUtility.GetHandleSize(pos) * size;
            var prevColor = Handles.color;
            Handles.color = x;
            pos = Handles.Slider(pos, rot * Vector3.right, size, cap, -1f);
            Handles.color = y;
            pos = Handles.Slider(pos, rot*Vector3.forward, size, cap, -1f);
            Handles.color = z;
            pos = Handles.Slider(pos, rot*Vector3.up, size, cap, -1f);
            Handles.color = prevColor;

            return pos;
        }

    }
}