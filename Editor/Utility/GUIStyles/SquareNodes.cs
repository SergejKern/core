﻿using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.GUIStyles
{
    public static class SquareNodes
    {
        const string k_squareNodesPath = ResourcePath.EditorResources + "/square_nodes";

        static GUIStyle m_black;

        /// <summary>  </summary>
        public static GUIStyle Black
        {
            get
            {
                if (EditorStyles.miniBoldFont == null)
                    return null;

                if (m_black != null)
                    return m_black;

                m_black = new GUIStyle();
                var normal =
                    (Texture2D) EditorGUIUtility.Load($"{k_squareNodesPath}/black.png");
                var active =
                    (Texture2D) EditorGUIUtility.Load($"{k_squareNodesPath}/black_on.png");
                var press =
                    (Texture2D)EditorGUIUtility.Load($"{k_squareNodesPath}/black_press.png");

                StyleHelpers.InitStyleOnOffPress(m_black, normal, active, press, Color.white, Color.yellow);

                m_black.border = new RectOffset(10, 10, 10, 10);
                m_black.alignment = TextAnchor.MiddleCenter;
                m_black.font = EditorStyles.miniBoldFont;

                return m_black;
            }
        }


        static GUIStyle m_darkGray;

        /// <summary>  </summary>
        public static GUIStyle DarkGray
        {
            get
            {
                if (EditorStyles.miniBoldFont == null)
                    return null;

                if (m_darkGray != null)
                    return m_darkGray;

                m_darkGray = new GUIStyle();
                var normal =
                    (Texture2D)EditorGUIUtility.Load($"{k_squareNodesPath}/dark_grey.png");
                var active =
                    (Texture2D)EditorGUIUtility.Load($"{k_squareNodesPath}/dark_grey_on.png");
                var press =
                    (Texture2D)EditorGUIUtility.Load($"{k_squareNodesPath}/dark_grey_press.png");

                StyleHelpers.InitStyleOnOffPress(m_darkGray, normal, active, press, Color.white, Color.yellow);

                m_darkGray.border = new RectOffset(10, 10, 10, 10);
                m_darkGray.alignment = TextAnchor.MiddleCenter;
                m_darkGray.font = EditorStyles.miniBoldFont;

                return m_darkGray;
            }
        }
    }
}