﻿using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.GUIStyles
{
    public static class BuiltinSkins
    {
        static GUIStyle m_gripBurger;
        /// <summary> A small burger icon, that can be used for resizing sub-windows </summary>
        public static GUIStyle GripBurger
        {
            get
            {
                if (m_gripBurger != null)
                    return m_gripBurger;

                m_gripBurger = new GUIStyle();
                var normal = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/node showhidden@2x-63.png");
                StyleHelpers.InitStyleOnOff(m_gripBurger, normal, normal);

                return m_gripBurger;
            }
        }

        static GUIStyle[] m_sqNodes;
        static GUIStyle[] m_hexNodes;

        public static GUIStyle SqNode(int i)
        {
            if (i < 0 || i >= 14)
                return null;
            if (m_sqNodes == null) m_sqNodes = new GUIStyle[14];
            if (m_sqNodes[i] != null)
                return m_sqNodes[i];

            m_sqNodes[i] = new GUIStyle();

            var j = i;
            // ReSharper disable once StringLiteralTypo
            var skin = "lightskin";
            if (i > 6)
            {
                skin = "darkskin";
                j = i - 7;
            }
            var node = (Texture2D)EditorGUIUtility.Load($"builtin skins/{skin}/images/node{j}.png");
            var nodeActive = (Texture2D)EditorGUIUtility.Load($"builtin skins/{skin}/images/node{j} on.png");

            StyleHelpers.InitStyleOnOff(m_sqNodes[i], node, nodeActive, Color.white, Color.yellow);

            m_sqNodes[i].border = new RectOffset(10, 10, 10, 10);
            m_sqNodes[i].alignment = TextAnchor.MiddleCenter;
            m_sqNodes[i].font = EditorStyles.miniBoldFont;
            // orig size: 
            // InitStyleOnOffFixedSize(_SqNodes[i], node, nodeActive, 50, 60);

            return m_sqNodes[i];
        }

        public static GUIStyle HexNode(int i)
        {
            if (i < 0 || i >= 14)
                return null;
            if (m_hexNodes == null) m_hexNodes = new GUIStyle[14];
            if (m_hexNodes[i] != null)
                return m_hexNodes[i];

            m_hexNodes[i] = new GUIStyle();

            var j = i;
            var skin = "light";
            if (i > 6)
            {
                skin = "dark";
                j = i - 7;
            }

            var node = (Texture2D)EditorGUIUtility.Load($"Nodes/{skin}/node{j} hex.png");
            var nodePress = (Texture2D)EditorGUIUtility.Load($"Nodes/{skin}/node{j} hex press.png");
            var nodeActive = (Texture2D)EditorGUIUtility.Load($"Nodes/{skin}/node{j} hex on.png");

            StyleHelpers.InitStyleOnOffPress(m_hexNodes[i], node, nodeActive, nodePress);
            // orig size: 
            // InitStyleOnOffFixedSize(_SqNodes[i], node, nodeActive, 50, 60);

            return m_hexNodes[i];
        }

        static GUIStyle m_optionalItem;

        public static GUIStyle OptionalItemButton
        {
            get
            {
                if (m_optionalItem != null)
                    return m_optionalItem;

                m_optionalItem = new GUIStyle();
                // ReSharper disable once StringLiteralTypo
                var normal = (Texture2D)EditorGUIUtility.Load("builtin skins/lightskin/images/consolecountbadge@2x.png");

                StyleHelpers.InitStyleOnOffFixedSize(m_optionalItem, normal, normal, 30, 30);

                return m_optionalItem;
            }
        }

        static GUIStyle m_darkSkinButton;

        public static GUIStyle DarkSkinButton
        {
            get
            {
                if (m_darkSkinButton != null)
                    return m_darkSkinButton;

                m_darkSkinButton = new GUIStyle();
                var normal = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/btn.png");
                var active = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/btn on.png");

                StyleHelpers.InitStyleOnOff(m_darkSkinButton, normal, active);

                return m_darkSkinButton;
            }
        }

        static GUIStyle m_lightRoundPlus;

        public static GUIStyle LightRoundPlus
        {
            get
            {
                if (m_lightRoundPlus != null)
                    return m_lightRoundPlus;

                m_lightRoundPlus = new GUIStyle();

                // ReSharper disable StringLiteralTypo
                var normal = (Texture2D)EditorGUIUtility.Load("builtin skins/lightskin/images/ol plus.png");
                var active = (Texture2D)EditorGUIUtility.Load("builtin skins/lightskin/images/ol plus act.png");
                // ReSharper restore StringLiteralTypo

                StyleHelpers.InitStyleOnOffFixedSize(m_lightRoundPlus, normal, active, 8, 8); //orig : 13

                return m_lightRoundPlus;
            }
        }

        static GUIStyle m_breadcrumbMid;

        public static GUIStyle BreadCrumbMid
        {
            get
            {
                if (m_breadcrumbMid != null)
                    return m_breadcrumbMid;

                // ReSharper disable StringLiteralTypo CommentTypo
                m_breadcrumbMid = new GUIStyle();
                var normal = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump mid.png");
                var active = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump mid act.png");
                //var onNormal = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump mid on.png");
                //var onActive = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump mid onact.png");
                StyleHelpers.InitStyleOnOffFixedSize(m_breadcrumbMid, normal, active, 24, 18);
                // ReSharper restore StringLiteralTypo CommentTypo

                return m_breadcrumbMid;
            }
        }

        static GUIStyle m_breadcrumbLeft;

        public static GUIStyle BreadCrumbLeft
        {
            get
            {
                if (m_breadcrumbLeft != null)
                    return m_breadcrumbLeft;
                // ReSharper disable StringLiteralTypo CommentTypo
                m_breadcrumbLeft = new GUIStyle();
                var normal = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump left.png");
                var active = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump left act.png");
                //var onNormal = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump left on.png");
                //var onActive = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/breadcrump left onact.png");
                StyleHelpers.InitStyleOnOffFixedSize(m_breadcrumbLeft, normal, active, 15, 18);
                // ReSharper restore StringLiteralTypo CommentTypo

                return m_breadcrumbLeft;
            }
        }
    }
}