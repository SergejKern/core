﻿using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.GUIStyles
{
    public static class RoundNodes
    {
        static GUIStyle m_debugBreakToggle;
        /// <summary>
        /// Red Breakpoint-icon toggle
        /// </summary>
        public static GUIStyle BreakpointToggle
        {
            get
            {
                if (m_debugBreakToggle != null)
                    return m_debugBreakToggle;

                m_debugBreakToggle = new GUIStyle();
                var breakOn = (Texture2D)EditorGUIUtility.Load($"{ResourcePath.EditorResources}/round_nodes/break_point.png");
                var breakOff = (Texture2D)EditorGUIUtility.Load($"{ResourcePath.EditorResources}/round_nodes/break_point_off.png");

                StyleHelpers.InitStyleOnOffFixedSize(m_debugBreakToggle, breakOff, breakOn, 16, 16);

                return m_debugBreakToggle;
            }
        }


        static GUIStyle m_emptyNode;

        /// <summary>  </summary>
        public static GUIStyle EmptyNode
        {
            get
            {
                if (m_emptyNode != null)
                    return m_emptyNode;

                m_emptyNode = new GUIStyle();
                var node = (Texture2D)EditorGUIUtility.Load($"{ResourcePath.EditorResources}/round_nodes/node.png");
                var nodeActive = (Texture2D)EditorGUIUtility.Load($"{ResourcePath.EditorResources}/round_nodes/node_active.png");

                StyleHelpers.InitStyleOnOffFixedSize(m_emptyNode, node, nodeActive, 15, 15);

                return m_emptyNode;
            }
        }
    }
}