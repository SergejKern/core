﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Core.Unity.Types.InterfaceContainerBase;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Utility.InterfaceReference
{
    public class InterfaceGUI : ScriptableObject
    {
        public static InterfaceGUI Instance
        {
            get
            {
                if (m_instance == null)
                {
                    var instances = Resources.FindObjectsOfTypeAll<InterfaceGUI>().ToList();
                    m_instance = instances.FirstOrDefault();
                    if (m_instance != null)
                    {
                        instances.RemoveAt(0);
                        foreach (var instance in instances)
                            DestroyImmediate(instance);

                    }
                    else
                        m_instance = CreateInstance<InterfaceGUI>();

                }
                m_instance.Initialize();
                return m_instance;
            }
        }
        static InterfaceGUI m_instance;

        public static readonly Color FieldBorder = new Color32(96, 96, 96, 255);
        public static readonly Color FieldBG = new Color32(209, 209, 209, 255);

        public static readonly Color SelectionBorder = new Color32(44, 84, 141, 255);
        public static readonly Color SelectionBG = new Color32(61, 128, 223, 255);

        public static readonly Color DropBorder = new Color32(62, 125, 231, 255);
        public static readonly Color DropBG = new Color32(193, 213, 247, 255);

        public static readonly Color DontPanicBorder = new Color32(90, 133, 127, 255);
        public static readonly Color DontPanicBG = new Color32(114, 228, 211, 255);

        public static readonly Color ErrorBorder = new Color32(150, 50, 50, 255);
        public static readonly Color ErrorBG = new Color32(220, 180, 180, 255);

        public static readonly Color PingBorder = new Color32(159, 131, 45, 255);
        public static readonly Color PingBG = new Color32(243, 225, 60, 255);

        public static readonly Color ObjectGroupBackgroundColor = new Color32(180, 180, 180, 255);
        public static readonly Color ObjectGroupSwitchBackgroundColor = new Color32(194, 194, 194, 255);
        public static readonly Color ProObjectGroupBackgroundColor = new Color32(49, 49, 49, 255);
        public static readonly Color ProObjectGroupSwitchBackgroundColor = new Color32(41, 41, 41, 255);

        #region Utility Methods

        const string k_guiTextureName = "InterfaceGUITexture";
        public static Texture2D BackgroundTexture(Color color)
        {
            var texture = new Texture2D(1, 1)
            {
                name = k_guiTextureName,
                hideFlags = HideFlags.DontSave
            };
            texture.SetPixel(0, 0, color);
            texture.Apply();
            return texture;
        }

        public static Texture2D BuildBoxTexture(Color bgColor, Color borderColor)
        {
            var boxTexture = GUI.skin.box.normal.background;
            if (boxTexture == null)
                return null;
            var texture = new Texture2D(boxTexture.width, boxTexture.height, TextureFormat.ARGB32, false)
            {
                name = k_guiTextureName,
                hideFlags = HideFlags.DontSave,
                wrapMode = TextureWrapMode.Repeat
            };

            for (var y = 0; y < texture.height; ++y)
            {
                for (var x = 0; x < texture.width; ++x)
                {
                    var yBorder = (y == 0 || y == texture.height - 1);
                    var xBorder = (x == 0 || x == texture.width - 1);
                    var border = yBorder || xBorder;
                    texture.SetPixel(x, y, border ? borderColor : bgColor);
                }
            }
            texture.Apply();
            return texture;
        }

        public static bool IsPingable(Object @object)
        {
            if (@object == null)
                return false;


            if (@object is Component || @object is GameObject)
                return true;

            return !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(@object));
        }

        public static string GetObjectName(Object @object)
        {
            if (@object == null)
                return "[null - you shouldn't be seeing this]";

            if (@object is Component || string.IsNullOrEmpty(@object.name))
                return InterfaceContainerBase.ConstructResolvedName(@object.GetType());

            return @object.name;
        }

        /// <summary>
        /// Pings the nearest object up the hierarchy that is ping-able. Necessary for prefabs.
        /// </summary>
        public static void PingObject(Object @object)
        {
            if (@object == null)
                return;

            var component = @object as Component;
            if (component != null)
                @object = component.gameObject;

            if (IsProjectAsset(@object))
            {
                var gameObject = @object as GameObject;
                if (gameObject != null)
                {
                    while (gameObject.transform.parent != null && gameObject.transform.parent.parent != null)
                    {
                        gameObject = gameObject.transform.parent.gameObject;
                    }

                    @object = gameObject;
                }
            }

            EditorGUIUtility.PingObject(@object);
        }

        public static bool IsProjectAsset(Object @object)
        {
            if (@object == null)
                return false;

            return AssetDatabase.Contains(@object) || PrefabUtility.GetPrefabAssetType(@object) != PrefabAssetType.NotAPrefab;
        }

        public static Rect CombineRects(Rect a, Rect b)
        {
            return new Rect(Math.Min(a.x, b.x), Math.Min(a.y, b.y), a.width + b.width, Math.Max(a.height, b.height));
        }

        public static float GetMinWidth(GUIContent content, GUIStyle style = null)
        {
            style = style ?? GUI.skin.label;
            style.CalcMinMaxWidth(content, out var min, out _);
            return min;
        }

        public static float GetScaledTextureWidth(Texture texture, float height, GUIStyle style = null)
        {
            if (texture == null || texture.height == 0)
                return 0.0f;
            
            return texture.width * ((height - (style == null ? 0 : style.padding.top + style.padding.bottom)) / texture.height);
        }

        public static IEnumerable<Object> EnumerateSavedObjects()
        {
            return EnumerateFiles(SearchOption.AllDirectories, "*.asset", "*.prefab").Select(f =>
            {
                var assetPathIndex = f.FullName.IndexOf("Assets/", StringComparison.Ordinal);
                if (assetPathIndex == -1)
                {
                    assetPathIndex = f.FullName.IndexOf(@"Assets\", StringComparison.Ordinal);
                }

                if (assetPathIndex < 0)
                    return null;

                var path = f.FullName.Substring(assetPathIndex, f.FullName.Length - assetPathIndex);
                return AssetDatabase.LoadAssetAtPath<Object>(path);

            }).Where(o => o != null);
        }

        public static IEnumerable<FileInfo> EnumerateFiles(SearchOption searchOption, params string[] searchPatterns)
        {
            var directoryInfo = new DirectoryInfo(Application.dataPath);
            return (searchPatterns ?? new[] { "*.*" }).SelectMany(s => directoryInfo.GetFiles(s, searchOption));
        }

        #endregion

        #region GUI Block Methods

        public static void ColorBlock(Action block)
        {
            var colorBlock = new GUIColorBlock();
            block();
            colorBlock.RestoreColors();
        }

        public static void EnabledBlock(Action block)
        {
            var enabledBlock = new GUIEnabledBlock();
            block();
            enabledBlock.RestoreEnabled();
        }

        public static void HorizontalBlock(Action block)
        {
            GUILayout.BeginHorizontal();
            block();
            GUILayout.EndHorizontal();
        }

        public static void HorizontalBlock(GUIStyle style, Action block)
        {
            GUILayout.BeginHorizontal(style);
            block();
            GUILayout.EndHorizontal();
        }

        public static void VerticalBlock(Action block)
        {
            GUILayout.BeginVertical();
            block();
            GUILayout.EndVertical();
        }

        public static void VerticalBlock(GUIStyle style, Action block)
        {
            GUILayout.BeginVertical(style);
            block();
            GUILayout.EndVertical();
        }

        public static void IndentBlock(Action block)
        {
            IndentBlock(1, block);
        }

        public static void IndentBlock(int indentLevel, Action block)
        {
            EditorGUI.indentLevel += indentLevel;
            block();
            EditorGUI.indentLevel -= indentLevel;
        }

        public static Vector2 ScrollViewBlock(Vector2 scrollPosition, bool alwaysShowHorizontal, bool alwaysShowVertical, Action block)
        {
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, alwaysShowHorizontal, alwaysShowVertical);
            block();
            EditorGUILayout.EndScrollView();
            return scrollPosition;
        }

        #endregion

        #region Private Parts

        static readonly List<FieldInfo> k_textureFields = CachedType<InterfaceGUI>.Type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
            .Where(f => f.FieldType == CachedType<Texture2D>.Type)
            .ToList();

        static GUIStyle InitializedStyle(GUIStyle style, Texture2D normalBackground, Texture2D activeBackground = null)
        {
            style.normal.background = normalBackground;
            style.active.background = activeBackground;
            return style;
        }

        class GUIColorBlock
        {
            readonly Color m_oldColor = GUI.color;
            readonly Color m_oldBackgroundColor = GUI.backgroundColor;
            readonly Color m_oldContentColor = GUI.contentColor;

            public void RestoreColors()
            {
                GUI.color = m_oldColor;
                GUI.backgroundColor = m_oldBackgroundColor;
                GUI.contentColor = m_oldContentColor;
            }
        }

        class GUIEnabledBlock
        {
            readonly bool m_enabled = GUI.enabled;

            public void RestoreEnabled() => GUI.enabled = m_enabled;
        }

        #endregion

        #region Instance Members

        public Texture2D FieldBox;
        public Texture2D PingedBackground;
        public Texture2D PingedBoxBackground;
        public Texture2D SelectionBackground;
        public Texture2D SelectionBoxBackground;
        public Texture2D DropBoxBackground;
        public Texture2D DontPanicBackground;
        public Texture2D ErrorBackground;
        public Texture2D ObjectGroupBackground;
        public Texture2D ObjectGroupSwitchBackground;

        public void Initialize()
        {
            hideFlags = HideFlags.DontSave;
            var isProSkin = EditorGUIUtility.isProSkin;
            if (m_proSkin != null && m_proSkin == isProSkin)
                return;

            DestroyTextures();
            m_proSkin = isProSkin;
            FieldBox = BuildBoxTexture(FieldBG, FieldBorder);
            PingedBackground = BackgroundTexture(PingBG);
            PingedBoxBackground = BuildBoxTexture(PingBG, PingBorder);
            SelectionBackground = BackgroundTexture(SelectionBG);
            SelectionBoxBackground = BuildBoxTexture(SelectionBG, SelectionBorder);
            DropBoxBackground = BuildBoxTexture(DropBG, DropBorder);
            DontPanicBackground = BuildBoxTexture(DontPanicBG, DontPanicBorder);
            ErrorBackground = BuildBoxTexture(ErrorBG, ErrorBorder);
            ObjectGroupBackground = BackgroundTexture(isProSkin ? ProObjectGroupBackgroundColor : ObjectGroupBackgroundColor);
            ObjectGroupSwitchBackground = BackgroundTexture(isProSkin ? ProObjectGroupSwitchBackgroundColor : ObjectGroupSwitchBackgroundColor);
        }

        bool? m_proSkin;

        void OnDestroy() => DestroyTextures();

        void DestroyTextures()
        {
            foreach (var texture in k_textureFields.Select(f => (Texture2D)f.GetValue(this)))
            {
                if (texture != null)
                    DestroyImmediate(texture);
            }
        }

        #endregion

        public static class SelectWindowStyles
        {
            const int k_padding = 4;

            public static readonly GUIStyle SelectedButton = new GUIStyle(GUI.skin.button)
            {
                normal = GUI.skin.button.active
            };

            public static GUIStyle NullOption => InitializedStyle(k_nullOption, Instance.FieldBox);

            static readonly GUIStyle k_nullOption = new GUIStyle(GUI.skin.box)
            {
                alignment = TextAnchor.MiddleCenter,
                normal = new GUIStyleState { textColor = Color.black }
            };

            public static GUIStyle NullSelected => InitializedStyle(k_nullSelected, Instance.SelectionBoxBackground);

            static readonly GUIStyle k_nullSelected = new GUIStyle(GUI.skin.box)
            {
                alignment = TextAnchor.MiddleCenter,
                normal = new GUIStyleState { textColor = Color.white }
            };

            public static GUIStyle Pinged => InitializedStyle(k_pinged, Instance.PingedBackground);

            static readonly GUIStyle k_pinged = new GUIStyle(GUI.skin.label)
            {
                normal = new GUIStyleState { textColor = PingBorder },
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                margin = new RectOffset(0, 0, 0, 0),
                padding = new RectOffset(0, 0, k_padding, k_padding)
            };

            public static GUIStyle SelectedObject => InitializedStyle(k_selectedObject, Instance.SelectionBackground);

            static readonly GUIStyle k_selectedObject = new GUIStyle(GUI.skin.label)
            {
                normal = new GUIStyleState { textColor = Color.white },
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                margin = new RectOffset(0, 0, 0, 0),
                padding = new RectOffset(0, 0, k_padding, k_padding)
            };

            public static GUIStyle ObjectGroup => InitializedStyle(k_objectGroup, Instance.ObjectGroupBackground);

            static readonly GUIStyle k_objectGroup = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                margin = new RectOffset(0, 0, 0, 0),
                padding = new RectOffset(0, 0, k_padding, k_padding)
            };

            public static GUIStyle ObjectGroupSwitch => InitializedStyle(k_objectGroupSwitch, Instance.ObjectGroupSwitchBackground);

            static readonly GUIStyle k_objectGroupSwitch = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                margin = new RectOffset(0, 0, 0, 0),
                padding = new RectOffset(0, 0, k_padding, k_padding)
            };

            public static GUIStyle DontPanic => InitializedStyle(k_dontPanic, Instance.DontPanicBackground);

            static readonly GUIStyle k_dontPanic = new GUIStyle(GUI.skin.box)
            {
                normal = new GUIStyleState { textColor = DontPanicBorder }
            };
        }

        public static class InspectorStyles
        {
            public static readonly GUIStyle NullOutButton = new GUIStyle(GUI.skin.button)
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 16,
                padding = new RectOffset(0, 0, 0, 3)
            };

            public static readonly GUIStyle SelectFromListButton = new GUIStyle(GUI.skin.button)
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 20,
                padding = new RectOffset(0, 0, 0, 0)
            };

            public static GUIStyle Result => InitializedStyle(k_result, GUI.skin.textField.normal.background);

            static readonly GUIStyle k_result = new GUIStyle(GUI.skin.textField)
            {
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                normal = new GUIStyleState { textColor = GUI.skin.textField.normal.textColor }
            };

            public static GUIStyle Selecting => InitializedStyle(k_selecting, Instance.SelectionBoxBackground);

            static readonly GUIStyle k_selecting = new GUIStyle(GUI.skin.textField)
            {
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                normal = new GUIStyleState { textColor = Color.white }
            };

            public static GUIStyle DropBox => InitializedStyle(k_dropBox, Instance.DropBoxBackground);

            static readonly GUIStyle k_dropBox = new GUIStyle(GUI.skin.textField)
            {
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                normal = new GUIStyleState { textColor = DropBorder }
            };

            public static GUIStyle Pinging => InitializedStyle(k_pinging, Instance.PingedBoxBackground);

            static readonly GUIStyle k_pinging = new GUIStyle(GUI.skin.textField)
            {
                alignment = TextAnchor.MiddleLeft,
                imagePosition = ImagePosition.ImageLeft,
                normal = new GUIStyleState { textColor = PingBorder }
            };

            public static GUIStyle Error => InitializedStyle(k_error, Instance.ErrorBackground);

            static readonly GUIStyle k_error = new GUIStyle(GUI.skin.box)
            {
                fontSize = 9,
                normal = new GUIStyleState { textColor = ErrorBorder }
            };
        }
    }

    public static class CachedType<T>
    {
        public static readonly Type Type = typeof(T);
    }

    public struct GUIContentRect
    {
        public GUIContent Content;
        public Rect Rect;

        public GUIContentRect(GUIContent content, Rect rect)
        {
            Content = content;
            Rect = rect;
        }

        public void SetWidth(float width) => Rect.xMax = Rect.xMin + width;

        public void MoveNextTo(Rect rect, float space = 0.0f) => Rect.xMin = rect.xMax + space;

        public static implicit operator Rect(GUIContentRect contentRect) => contentRect.Rect;

        public static implicit operator GUIContent(GUIContentRect contentRect) => contentRect.Content;
    }
}