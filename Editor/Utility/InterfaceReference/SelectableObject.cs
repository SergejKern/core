using UnityEngine;

namespace Core.Editor.Utility.InterfaceReference
{
    public class SelectableObject
    {
        public readonly Object Object;
        public readonly bool IsProjectAsset;
        public readonly bool IsComponent;

        SelectableObject(Object @object)
        {
            Object = @object;
            IsProjectAsset = InterfaceGUI.IsProjectAsset(Object);
            IsComponent = Object is Component;
        }

        public static SelectableObject GetSelectableObject<TResult>(TResult result)
        {
            var @object = result as Object;
            return @object == null ? null : new SelectableObject(@object);
        }
    }
}