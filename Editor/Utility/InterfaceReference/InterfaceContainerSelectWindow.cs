﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Utility.InterfaceReference
{
    public class InterfaceContainerSelectWindow : EditorWindow
    {
        public static InterfaceContainerSelectWindow SelectWindow;

        public static InterfaceContainerSelectWindow ShowSelectWindow(string resultTypeName, bool selectingForProjectAsset, 
            SerializedContainer serializedContainer, IEnumerable<SelectableObject> selectableObjects)
        {
            var window = (InterfaceContainerSelectWindow) CreateInstance(typeof(InterfaceContainerSelectWindow));
            window.Initialize(resultTypeName, selectingForProjectAsset, serializedContainer, selectableObjects);
            window.ShowUtility();
            return window;
        }

        #region Private Parts
    
        string m_resultTypeName;
        bool m_selectingForProjectAsset;
        SerializedContainer m_serializedContainer;
        List<ObjectNode> m_allObjects;
        List<ObjectNode> m_projectAssets;
        List<ObjectNode> m_sceneAssets;
        Vector2 m_scrollPos;
        bool m_close = true;
        bool m_sceneAssetsExist = true;
        bool m_projectAssetsExist = true;
        bool m_selectingProjectAssets;
        bool m_switchBoxStyle;
        const string k_indentString = "    ";

        bool IsValid => m_serializedContainer != null && m_allObjects != null && !m_close;

        void Initialize(string resultTypeName, bool selectingForProjectAsset, SerializedContainer serializedContainer, 
            IEnumerable<SelectableObject> selectableObject)
        {
            serializedContainer.SetSelecting(true);
            m_resultTypeName = resultTypeName;
            m_selectingForProjectAsset = selectingForProjectAsset;
            titleContent = new GUIContent($"Implementing {m_resultTypeName} {(m_selectingForProjectAsset ? "( project assets only )" : "")}");

            m_serializedContainer = serializedContainer;
            m_allObjects = new SelectableObjectsHierarchyBuilder().BuildSelectableObjectsList(selectableObject, m_serializedContainer.ObjectField, out m_selectingProjectAssets);
            m_projectAssets = m_allObjects.Where(g => g.IsProjectAsset).ToList();
            m_sceneAssets = m_allObjects.Where(g => !g.IsProjectAsset).ToList();
            m_projectAssetsExist = m_projectAssets.Any();
            m_sceneAssetsExist = m_sceneAssets.Any();
            m_selectingProjectAssets = (m_selectingProjectAssets || m_selectingForProjectAsset) || (m_projectAssetsExist && !m_sceneAssetsExist);
            m_close = false;
        }

        void OnGUI()
        {
            if(!IsValid)
                return;

            GUINullOption();

            if(!m_allObjects.Any())
            {
                GUILayout.Space(10.0f);
                GUILayout.Label($"\nNo {(m_selectingForProjectAsset ? "project " : "")}assets found that implement or derive from {m_resultTypeName}.\n", InterfaceGUI.SelectWindowStyles.DontPanic, GUILayout.ExpandWidth(true));
                return;
            }
        
            if(!m_selectingForProjectAsset)
            {
                GUISelectObjectType();
            }
        
            m_scrollPos = InterfaceGUI.ScrollViewBlock(m_scrollPos, false, false, () =>
            {
                m_switchBoxStyle = false;
                foreach(var selectableObject in (m_selectingProjectAssets ? m_projectAssets : m_sceneAssets))
                {
                    GUIObjectNode(selectableObject);
                }
            });
        }

        void GUINullOption()
        {
            InterfaceGUI.HorizontalBlock(() =>
            {
                InterfaceGUI.EnabledBlock(() =>
                {
                    GUI.enabled = m_allObjects.Any();

                    if(GUILayout.Button(new GUIContent("▼", "Expand All"), GUILayout.ExpandWidth(false)))
                    {
                        FoldoutAll(m_allObjects, true);
                    }

                    if(GUILayout.Button(new GUIContent("▲", "Collapse All"), GUILayout.ExpandWidth(false)))
                    {
                        FoldoutAll(m_allObjects, false);
                    }
                });

                var style = m_serializedContainer.ObjectField == null && string.IsNullOrEmpty(m_serializedContainer.ResultType) ? InterfaceGUI.SelectWindowStyles.NullSelected : InterfaceGUI.SelectWindowStyles.NullOption;
                if(GUILayout.Button("NULL", style, GUILayout.ExpandWidth(true)))
                {
                    m_serializedContainer.ObjectField = null;
                    m_serializedContainer.ApplyModifiedProperties();
                }
                EditorGUIUtility.AddCursorRect(GUILayoutUtility.GetLastRect(), MouseCursor.Link);

                GUILayout.Space(5.0f);
            });
        }

        void GUISelectObjectType()
        {
            InterfaceGUI.HorizontalBlock(() =>
            {
                InterfaceGUI.EnabledBlock(() =>
                {
                    GUI.enabled = m_sceneAssetsExist;
                    if(GUILayout.Button("Scene Assets", m_selectingProjectAssets ? GUI.skin.button : InterfaceGUI.SelectWindowStyles.SelectedButton))
                    {
                        m_selectingProjectAssets = false;
                    }
                });

                InterfaceGUI.EnabledBlock(() =>
                {
                    GUI.enabled = m_projectAssetsExist;
                    if(GUILayout.Button("Project Assets", m_selectingProjectAssets ? InterfaceGUI.SelectWindowStyles.SelectedButton : GUI.skin.button))
                    {
                        m_selectingProjectAssets = true;
                    }
                });
            });
        }

        void GUIObjectNode(ObjectNode objectNode, int indentLevel = 0)
        {
            GUIObjectNodes.ObjectNodeGUI(objectNode, m_serializedContainer, GetNextStyle(), indentLevel);

            if (!objectNode.Foldout)
                return;

            foreach(var child in objectNode.Children)
            {
                GUIObjectNode(child, indentLevel + 1);
            }
        }
    
        GUIStyle GetNextStyle()
        {
            m_switchBoxStyle = !m_switchBoxStyle;
            return m_switchBoxStyle ? InterfaceGUI.SelectWindowStyles.ObjectGroup : InterfaceGUI.SelectWindowStyles.ObjectGroupSwitch;
        }

        // ReSharper disable once UnusedParameter.Local
        static void FoldoutAll(IEnumerable<ObjectNode> selectableObjects, bool foldout)
        {
            foreach(var selectableObject in selectableObjects)
            {
                FoldoutAll(selectableObject.Children, (selectableObject.Foldout = foldout));
            }
        }

        void Update()
        {
            if(!IsValid)
                Close();
        }

        void OnLostFocus() => m_close = true;
        void OnDestroy() => m_serializedContainer?.SetSelecting(false);

        #endregion

        public class ObjectNode
        {
            public Object Object;

            public ObjectNode Parent;
            public List<ObjectNode> Children = new List<ObjectNode>();

            public bool IsSelectable;
            public bool IsPingable;
            public bool IsProjectAsset;
            public bool Foldout = true;
            public string NodeName;

            public int TotalNodeCount
            {
                get
                {
                    if (m_totalNodeCount == null)
                        m_totalNodeCount = GetTotalNodeCount(this);
                    return m_totalNodeCount.Value;
                }
            }
            int? m_totalNodeCount;

            static int GetTotalNodeCount(ObjectNode node)
            {
                var totalCount = 1;
                foreach(var childNode in node.Children)
                    totalCount += childNode.TotalNodeCount;
                return totalCount;
            }
        }

        class SelectableObjectsHierarchyBuilder
        {
            Dictionary<Object, ObjectNode> m_parentNodes;
            List<ObjectNode> m_rootNodes;

            public List<ObjectNode> BuildSelectableObjectsList(IEnumerable<SelectableObject> selectableObjects, 
                Object selectedObject, out bool selectingProjectAssets)
            {
                selectingProjectAssets = false;

                m_rootNodes = new List<ObjectNode>();
                m_parentNodes = new Dictionary<Object, ObjectNode>();
            
                foreach(var selectableObject in selectableObjects)
                {
                    if(selectedObject == selectableObject.Object)
                    {
                        selectingProjectAssets = selectableObject.IsProjectAsset;
                    }

                    var selectableObjectNode = new ObjectNode
                    {
                        Object = selectableObject.Object,
                        NodeName = InterfaceGUI.GetObjectName(selectableObject.Object),

                        IsSelectable = true,
                        IsPingable = !selectableObject.IsComponent && InterfaceGUI.IsPingable(selectableObject.Object),
                        IsProjectAsset = selectableObject.IsProjectAsset,
                    };

                    if(selectableObject.IsComponent)
                        selectableObjectNode.Parent = GetOrCreateParentNode(selectableObjectNode);
                    else
                        m_rootNodes.Add(selectableObjectNode);
                }

                var missingParents = m_parentNodes.Values.Where(n => n.Parent == null && !m_rootNodes.Contains(n)).ToList();
                m_rootNodes.AddRange(missingParents);
                var @return = SortedNodes(m_rootNodes);

                m_parentNodes.Clear();
                m_rootNodes.Clear();

                return @return;
            }

            static List<ObjectNode> SortedNodes(List<ObjectNode> nodes)
            {
                nodes = nodes.OrderBy(n => n.TotalNodeCount).ThenBy(n => n.NodeName).ToList();
                foreach(var node in nodes)
                    node.Children = SortedNodes(node.Children);

                return nodes;
            }

            ObjectNode GetOrCreateParentNode(ObjectNode childNode)
            {
                GameObject parent = null;

                var component = childNode.Object as Component;
                if(component != null)
                {
                    parent = component.gameObject;
                }
                else
                {
                    var go = childNode.Object as GameObject;
                    if (go != null && go.transform.parent != null)
                        parent = go.transform.parent.gameObject;
                }

                if (parent == null)
                    return null;

                if(!m_parentNodes.TryGetValue(parent, out var parentNode))
                {
                    parentNode = new ObjectNode
                    {
                        Object = parent,
                        IsSelectable = false,
                        IsPingable = InterfaceGUI.IsPingable(parent),
                        IsProjectAsset = InterfaceGUI.IsProjectAsset(parent),
                        NodeName = InterfaceGUI.GetObjectName(parent)
                    };
                    m_parentNodes.Add(parent, parentNode);
                    parentNode.Parent = GetOrCreateParentNode(parentNode);
                }
                parentNode.Children.Add(childNode);

                return parentNode;
            }
        }

        class GUIObjectNodes
        {
            public static void ObjectNodeGUI(ObjectNode objectNode, SerializedContainer serializedContainer, GUIStyle style, int indentLevel)
            {
                InterfaceGUI.HorizontalBlock(() =>
                {
                    var helper = new GUIObjectNodes(objectNode, serializedContainer);
                    helper.DrawGUI(style, indentLevel);
                });
            }

            #region Private Parts

            readonly ObjectNode m_objectNode;
            readonly SerializedContainer m_serializedContainer;
            readonly bool m_displayFoldout;
            bool m_toggleFoldout;
            bool m_select;
            bool m_selectDown;
            bool m_ping;
            bool m_pingDown;

            GUIObjectNodes(ObjectNode objectNode, SerializedContainer serializedContainer)
            {
                m_objectNode = objectNode;
                m_serializedContainer = serializedContainer;
                m_displayFoldout = m_objectNode.Children.Any();
            }

            void DrawGUI(GUIStyle style, int indentLevel)
            {
                CreateContentRects(style, indentLevel, out var nodeGUI, out var foldoutGUI, out var iconGUI, out var labelGUI);
                DetermineActions(nodeGUI, foldoutGUI, labelGUI);
                DrawControls(style, nodeGUI, foldoutGUI, iconGUI, labelGUI);
                ExecuteActions();
            }

            void CreateContentRects(GUIStyle style, int indentLevel, out GUIContentRect nodeGUI, out GUIContentRect foldoutGUI, out GUIContentRect iconGUI, out GUIContentRect labelGUI)
            {
                var foldoutContent = GetFoldoutContent(indentLevel);
                GetObjectNodeContents(m_objectNode, out var iconContent, out var labelContent);

                nodeGUI = new GUIContentRect(GUIContent.none, GUILayoutUtility.GetRect(foldoutContent, style));
                foldoutGUI = new GUIContentRect(foldoutContent, nodeGUI);
                iconGUI = new GUIContentRect(iconContent, nodeGUI);
                labelGUI = new GUIContentRect(labelContent, nodeGUI);

                foldoutGUI.SetWidth(InterfaceGUI.GetMinWidth(foldoutContent, style) + 2.0f);

                iconGUI.MoveNextTo(foldoutGUI);
                iconGUI.SetWidth(iconContent == null ? 0.0f : (InterfaceGUI.GetScaledTextureWidth(iconContent.image, foldoutGUI.Rect.height, style) + 2.0f));

                labelGUI.MoveNextTo(iconGUI);
                labelGUI.SetWidth(InterfaceGUI.GetMinWidth(labelContent, style));
            }

            void DetermineActions(Rect objectRect, Rect foldoutRect, Rect nameRect)
            {
                FoldoutButton(foldoutRect);

                if(m_objectNode.IsSelectable)
                {
                    var selectRect = new Rect(objectRect);
                    if(m_displayFoldout)
                    {
                        selectRect.xMin = foldoutRect.xMax;
                    }
                    if(m_objectNode.IsPingable)
                    {
                        selectRect.xMax = nameRect.xMax;
                    }

                    SelectButton(selectRect);

                    if (!m_objectNode.IsPingable) return;
                    var pingRect = new Rect(objectRect) {xMin = selectRect.xMax};
                    PingButton(pingRect);
                }
                else
                {
                    if (!m_objectNode.IsPingable)
                        return;
                    var pingRect = new Rect(objectRect) {xMin = foldoutRect.xMax};
                    PingButton(pingRect);
                }
            }

            void DrawControls(GUIStyle style, GUIContentRect nodeGUI, GUIContentRect foldoutGUI, GUIContentRect iconGUI, GUIContentRect nameGUI)
            {
                style = DetermineObjectStyle(style, m_objectNode, m_serializedContainer.ObjectField);
                GUI.Label(nodeGUI, m_displayFoldout ? foldoutGUI : nodeGUI, style);

                style.normal.background = null;
                if (iconGUI.Content != null)
                    GUI.Label(iconGUI, iconGUI, style);
                GUI.Label(nameGUI, nameGUI, style);
            }

            void ExecuteActions()
            {
                if (m_toggleFoldout)
                    m_objectNode.Foldout = !m_objectNode.Foldout;
                
                if(m_select)
                {
                    m_serializedContainer.ObjectField = m_objectNode.Object;
                    m_serializedContainer.ResultType = null;
                    m_serializedContainer.ApplyModifiedProperties();
                }

                if (m_ping)
                    InterfaceGUI.PingObject(m_objectNode.Object);
            }

            GUIContent GetFoldoutContent(int indentLevel)
            {
                var foldoutString = " ";
                for(var i = 0; i < indentLevel; ++i)
                {
                    foldoutString += k_indentString;
                }
                foldoutString += (m_objectNode.Foldout ? "▼" : "►");
                return new GUIContent(foldoutString);
            }

            static void GetObjectNodeContents(ObjectNode objectNode, out GUIContent iconContent, out GUIContent labelContent)
            {
                iconContent = null;
                labelContent = new GUIContent(objectNode.NodeName);

                Texture icon;
                if(objectNode.Object is GameObject)
                {
                    icon = objectNode.IsProjectAsset 
                        ? EditorGUIUtility.FindTexture("PrefabNormal Icon") 
                        : EditorGUIUtility.ObjectContent(null, typeof(GameObject)).image;
                }
                else
                    icon = EditorGUIUtility.ObjectContent(objectNode.Object, null).image;

                if (icon != null)
                    iconContent = new GUIContent(icon);
            }

            GUIStyle DetermineObjectStyle(GUIStyle defaultStyle, ObjectNode objectNode, Object selectedObject)
            {
                if (m_ping || m_pingDown)
                    return InterfaceGUI.SelectWindowStyles.Pinged;

                if (selectedObject == objectNode.Object || m_select || m_selectDown)
                    return InterfaceGUI.SelectWindowStyles.SelectedObject;
                

                return defaultStyle;
            }

            void FoldoutButton(Rect rect)
            {
                if (!m_displayFoldout)
                    return;

                if (GUI.Button(rect, "", GUIStyle.none))
                    m_toggleFoldout = true;
            }

            void SelectButton(Rect rect)
            {
                var buttonId = GUIUtility.GetControlID(FocusType.Passive) + 1;
                if (GUI.Button(rect, "", GUIStyle.none))
                    m_select = true;
                
                m_selectDown = GUIUtility.hotControl == buttonId;
                EditorGUIUtility.AddCursorRect(rect, MouseCursor.Link);
            }

            void PingButton(Rect rect)
            {
                var buttonId = GUIUtility.GetControlID(FocusType.Passive) + 1;
                if (GUI.Button(rect, "", GUIStyle.none))
                    m_ping = true;
                m_pingDown = GUIUtility.hotControl == buttonId;
                EditorGUIUtility.AddCursorRect(rect, MouseCursor.Zoom);
            }

            #endregion
        }
    }
}