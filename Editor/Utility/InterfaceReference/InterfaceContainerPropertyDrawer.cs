﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Core.Unity.Types;
using Core.Unity.Types.InterfaceContainerBase;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using Delegate = System.Delegate;

namespace Core.Editor.Utility.InterfaceReference
{
    [CustomPropertyDrawer(typeof(InterfaceContainerBase), true)]
    public class InterfaceContainerPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) =>
            OnGUI(position, property, label, HashCode);
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) =>
            GetPropertyHeight();

        int HashCode => (m_hashCode ?? (m_hashCode = GetHashCode())).Value;
        int? m_hashCode;
    //}
    //public static class InterfaceContainerDrawer 
    //{
        public static void OnGUI(Rect position, SerializedProperty property, GUIContent label, int hashCode)
        {
            var containerType = GetContainerType(property);
            if (containerType == null)
            {
                position.width -= 4;
                position.x += 2;
                position.height -= 2;
                position.y += 1;
                GUI.Label(position, $"Cannot draw '{property.name}'.", InterfaceGUI.InspectorStyles.Error);
                return;
            }

            var resultType = containerType.GetProperty("Result")?.PropertyType;
            var drawContainerMethod = GetDrawMethod(resultType);

            label = EditorGUI.BeginProperty(position, label, property);
            drawContainerMethod(position, label, new SerializedContainer(hashCode, property));
            EditorGUI.EndProperty();
        }

        public static float GetPropertyHeight() => 18f + k_buttonSpace + k_buttonSpace;

        #region Parts
        static Event m_currentEvent;

        const float k_buttonSpace = 1.0f;
        const float k_buttonWidth = 20.0f;

        static void DrawInterfaceContainer<TResult>(Rect position, GUIContent label, SerializedContainer serializedContainer)
            where TResult : class
        {
            m_currentEvent = Event.current;
            var resultTypeName = InterfaceContainerBase.ConstructResolvedName(CachedType<TResult>.Type);

            if (string.IsNullOrEmpty(label.tooltip))
                label.tooltip = resultTypeName;

            GetContentRects(position, label, out var labelRect, out var resultRect, out var nullButtonRect, out var listButtonRect);

            var isProjectAsset = serializedContainer.IsProjectAsset;
            var pingable = !serializedContainer.ObjectFieldProperty.hasMultipleDifferentValues && InterfaceGUI.IsPingable(serializedContainer.ObjectField);
            var dragDropResult = GetDragAndDropResult<TResult>(resultRect, isProjectAsset, serializedContainer);

            EditorGUI.LabelField(labelRect, label);
            DrawSerializedContainer(serializedContainer, pingable, resultRect);

            if (dragDropResult != null)
            {
                serializedContainer.ObjectField = dragDropResult as Object;
                GUI.changed = true;
            }

            if (GUI.Button(nullButtonRect, new GUIContent("○", "Set to null"), InterfaceGUI.InspectorStyles.NullOutButton))
                serializedContainer.ObjectField = null;


            InterfaceGUI.EnabledBlock(() =>
            {
                if (GUI.Button(listButtonRect, new GUIContent("◉", "Select from list"), InterfaceGUI.InspectorStyles.SelectFromListButton))
                    InterfaceContainerSelectWindow.SelectWindow = InterfaceContainerSelectWindow.ShowSelectWindow(resultTypeName, isProjectAsset, serializedContainer, GetSelectable<TResult>(isProjectAsset));
            });
        }

        static void DrawSerializedContainer(SerializedContainer serializedContainer, bool pingable,
            GUIContentRect resultRect)
        {
            InterfaceGUI.EnabledBlock(() =>
            {
                GUI.enabled = pingable;

                InterfaceGUI.ColorBlock(() =>
                {
                    if (serializedContainer.Selecting || serializedContainer.Dropping)
                        GUI.color = new Color(1, 1, 1, 2);
                    else
                        GUI.color = pingable ? new Color(1, 1, 1, 2) : Color.white;

                    DrawField(serializedContainer, resultRect, pingable);
                });
            });
        }

        static void GetContentRects(Rect position, GUIContent label, out GUIContentRect labelRect,
            out GUIContentRect resultRect, out GUIContentRect nullButtonRect, out GUIContentRect listButtonRect)
        {
            labelRect = new GUIContentRect(label, position);
            labelRect.SetWidth(EditorGUIUtility.labelWidth);

            resultRect = new GUIContentRect(null, position);
            resultRect.MoveNextTo(labelRect);
            resultRect.Rect.xMax -= (k_buttonSpace + k_buttonWidth) * 2;

            nullButtonRect = new GUIContentRect(null, position);
            nullButtonRect.MoveNextTo(resultRect, k_buttonSpace);
            nullButtonRect.SetWidth(k_buttonWidth);

            listButtonRect = new GUIContentRect(null, position);
            listButtonRect.MoveNextTo(nullButtonRect, k_buttonSpace);
            listButtonRect.SetWidth(k_buttonWidth);
        }

        // ReSharper disable once FlagArgument
        static void DrawField(SerializedContainer serializedContainer, Rect rect, bool pingable)
        {
            var buttonId = GUIUtility.GetControlID(FocusType.Passive) + 1;
            if (GUI.Button(rect, GUIContent.none, GUIStyle.none) && pingable)
                InterfaceGUI.PingObject(serializedContainer.ObjectField);
            
            var pinging = GUIUtility.hotControl == buttonId && pingable;
            var style = GetStyleFor(serializedContainer, pinging);

            GUI.Label(rect, GUIContent.none, style);

            serializedContainer.GetContainerContents(rect, serializedContainer.Dropping, out var icon, out var label);

            style.normal.background = null;

            if (icon.Content != null)
            {
                icon.SetWidth(InterfaceGUI.GetScaledTextureWidth(icon.Content.image, rect.height + 2.0f));
                label.MoveNextTo(icon, -3.0f);
                GUI.Label(icon, icon, style);
            }

            GUI.Label(label, label, style);

            if (!SerializedContainer.AnyDropping && pingable)
                EditorGUIUtility.AddCursorRect(rect, MouseCursor.Zoom);
        }

        // ReSharper disable once FlagArgument
        static GUIStyle GetStyleFor(SerializedContainer serializedContainer, bool pinging)
        {
            GUIStyle style;
            if (serializedContainer.Dropping)
                style = InterfaceGUI.InspectorStyles.DropBox;

            else if (pinging)
                style = InterfaceGUI.InspectorStyles.Pinging;
            else if (serializedContainer.Selecting)
                style = InterfaceGUI.InspectorStyles.Selecting;
            else
                style = InterfaceGUI.InspectorStyles.Result;

            if (serializedContainer.ObjectFieldProperty.hasMultipleDifferentValues || serializedContainer.ObjectField == null)
            {
                style.alignment = TextAnchor.MiddleCenter;
                style.imagePosition = ImagePosition.TextOnly;
            }
            else
            {
                style.alignment = TextAnchor.MiddleLeft;
                style.imagePosition = ImagePosition.ImageLeft;
            }

            return style;
        }

        // ReSharper disable once FlagArgument
        static TResult GetDragAndDropResult<TResult>(Rect dropArea, bool selectingForProjectAsset, SerializedContainer serializedContainer)
            where TResult : class
        {
            TResult result = null;
            bool? dropping = null;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (m_currentEvent.rawType)
            {
                case EventType.DragExited:
                case EventType.DragUpdated:
                case EventType.DragPerform:
                dropping = false;
                if (!serializedContainer.MouseInRects(dropArea, m_currentEvent.mousePosition))
                    break;
                
                var single = DragAndDrop.objectReferences.SelectMany(GetObjectImplementationsOf<TResult>).FirstOrDefault();
                if (single != null)
                {
                    var singleObject = single as Object;
                    if (singleObject != null && (!selectingForProjectAsset || InterfaceGUI.IsProjectAsset(singleObject)))
                        dropping = true;
                }
                DragAndDrop.visualMode = SerializedContainer.AnyDropping ? DragAndDropVisualMode.Copy : DragAndDropVisualMode.Rejected;

                if (m_currentEvent.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();
                    m_currentEvent.Use();
                    dropping = false;
                    result = single;
                }
                break;
            }

            if (dropping != null)
                serializedContainer.SetDropping(dropping.Value);

            return result;
        }

        static List<TResult> GetObjectImplementationsOf<TResult>(Object @object)
            where TResult : class
        {
            var implementations = new List<TResult>();
            if (@object is TResult implementation)
                implementations.Add(implementation);

            var gameObject = @object as GameObject;
            if (gameObject != null)
                implementations.AddRange(gameObject.GetComponents<Component>().OfType<TResult>());

            var transform = @object as Transform;
            if (transform != null)
                implementations.AddRange(transform.GetComponents<Component>().OfType<TResult>());

            return implementations.Distinct().ToList();
        }

        static IEnumerable<SelectableObject> GetSelectable<TResult>(bool projectAssetsOnly)
            where TResult : class
        {
            var objects = InterfaceGUI.EnumerateSavedObjects().Concat(projectAssetsOnly ? new Object[0] : Object.FindObjectsOfType<Object>());
            var implementations = new HashSet<TResult>();
            foreach (var implementation in objects.SelectMany(GetObjectImplementationsOf<TResult>))
                implementations.Add(implementation);
            
            return implementations.Select(SelectableObject.GetSelectableObject);
        }

        /// <summary>
        /// Given a SerializedProperty of a field, List, or array of InterfaceContainer&lt;T&gt; derivative(s), will initialize null references and return the container derivative type.
        /// </summary>
        /// <returns>The type deriving from InterfaceContainer&lt;T&gt;, if any - null otherwise.</returns>
        static readonly Dictionary<Type, Dictionary<string, Type>> k_containerTypesByPaths = new Dictionary<Type, Dictionary<string, Type>>();
        static readonly Regex k_arrayMatch = new Regex(@"\.Array\.data\[\d+\]", RegexOptions.Compiled);
        const BindingFlags k_fieldBindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;
        static Type GetContainerType(SerializedProperty property)
        {
            var type = property.serializedObject.targetObject.GetType();
            if (!k_containerTypesByPaths.TryGetValue(type, out var containerTypes))
                k_containerTypesByPaths.Add(type, (containerTypes = new Dictionary<string, Type>()));
            
            var cleanPath = k_arrayMatch.Replace(property.propertyPath, "");
            if (containerTypes.TryGetValue(cleanPath, out var containerType))
                return containerType;

            foreach (var field in cleanPath.Split('.'))
            {
                var fieldInfo = GetFieldInChain(type, field);
                if (fieldInfo == null)
                    return null;
                

                type = fieldInfo.FieldType;
                if (type.IsArray)
                    type = type.GetElementType();
                else if (IsSubclassOfRawGeneric(type, typeof(List<>), out var genericArguments))
                    type = genericArguments[0];
            }

            if (!IsSubclassOfRawGeneric(type, typeof(InterfaceContainer<>), out _))
                return null;

            containerTypes.Add(cleanPath, type);
            return type;
        }

        static FieldInfo GetFieldInChain(Type type, string fieldName)
        {
            while (true)
            {
                if (type == null) return null;

                var field = type.GetField(fieldName, k_fieldBindingFlags);
                if (field != null)
                    return field;
                type = type.BaseType;
            }
        }

        /// <summary>
        /// Credit to JaredPar: http://stackoverflow.com/questions/457676/check-if-a-class-is-derived-from-a-generic-class
        /// </summary>
        public static bool IsSubclassOfRawGeneric(Type toCheck, Type generic, out List<Type> genericTypeArguments)
        {
            genericTypeArguments = null;

            while (toCheck != null && toCheck != typeof(object))
            {
                var checkGeneric = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;

                if (generic == checkGeneric)
                {
                    genericTypeArguments = toCheck.GetGenericArguments().ToList();
                    return true;
                }

                toCheck = toCheck.BaseType;
            }

            return false;
        }

        static readonly MethodInfo k_drawMethodInfo = CachedType<InterfaceContainerPropertyDrawer>.Type.GetMethod($"{nameof(DrawInterfaceContainer)}", 
            BindingFlags.Static | BindingFlags.NonPublic);
        static readonly Dictionary<Type, DrawMethod> k_drawMethods = new Dictionary<Type, DrawMethod>();

        delegate void DrawMethod(Rect position, GUIContent label, SerializedContainer serializedContainer);
        static DrawMethod GetDrawMethod(Type type)
        {
            if (!k_drawMethods.TryGetValue(type, out var drawMethod))
                k_drawMethods.Add(type, (drawMethod = (DrawMethod)
                    Delegate.CreateDelegate(CachedType<DrawMethod>.Type, 
                        k_drawMethodInfo.MakeGenericMethod(type))));
            
            return drawMethod;
        }

        #endregion



    }
}