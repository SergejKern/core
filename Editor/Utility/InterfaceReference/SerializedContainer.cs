using System.Collections.Generic;
using Core.Unity.Types.InterfaceContainerBase;
using UnityEditor;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Core.Editor.Utility.InterfaceReference
{
    /// <summary>
    /// Handy class wrapping around an InterfaceContainer-derived serialized property.
    /// </summary>
    public class SerializedContainer
    {
        public SerializedProperty ObjectFieldProperty => m_objectFieldProperty
                                                         ?? (m_objectFieldProperty =
                                                             m_containerProperty.FindPropertyRelative("ObjectField"));

        public SerializedProperty ResultTypeProperty => m_resultTypeProperty
                                                        ?? (m_resultTypeProperty =
                                                            m_containerProperty.FindPropertyRelative("ResultType"));

        public Object ObjectField
        {
            get => ObjectFieldProperty.objectReferenceValue;
            set
            {
                ResultType = "";
                ObjectFieldProperty.objectReferenceValue = value;
            }
        }

        public string ResultType
        {
            get => ResultTypeProperty.stringValue;
            set => ResultTypeProperty.stringValue = value;
        }

        public bool IsProjectAsset => InterfaceGUI.IsProjectAsset(m_containerProperty.serializedObject.targetObject);
        public void ApplyModifiedProperties() => m_containerProperty.serializedObject.ApplyModifiedProperties();
        public bool Dropping => m_droppingHashcode == m_containerHashcode;
        public static bool AnyDropping => m_droppingHashcode != null;
        public bool Selecting => InterfaceContainerSelectWindow.SelectWindow != null && m_selectingHashcode == m_containerHashcode;

        public SerializedContainer(int propertyDrawerHash, SerializedProperty containerProperty)
        {
            m_containerProperty = containerProperty ?? throw new System.ArgumentNullException(nameof(containerProperty));

            unchecked
            {
                var propertyPath = ObjectFieldProperty.propertyPath;
                m_containerHashcode = (propertyDrawerHash * 397) ^ (propertyPath?.GetHashCode() ?? 0);
            }
        }

        public void GetContainerContents(Rect rect, bool droppingFor, out GUIContentRect iconContent,
            out GUIContentRect labelContent)
        {
            Texture icon;
            string resultString, resultTip;
            if (ObjectFieldProperty.hasMultipleDifferentValues)
            {
                icon = null;
                resultString = "-";
                resultTip = null;
            }
            else
            {
                var @object = ObjectField;
                if (InterfaceGUI.IsProjectAsset(@object) && (@object is Component || @object is GameObject))
                    icon = EditorGUIUtility.FindTexture("PrefabNormal Icon");
                else
                    icon = EditorGUIUtility.ObjectContent(@object, null).image;

                resultString = (resultTip = BuildEditorResultString(ResultType, ObjectField)) ?? "null";
            }

            iconContent = icon == null
                ? new GUIContentRect(null, rect)
                : new GUIContentRect(new GUIContent(icon, resultTip), rect);
            labelContent = new GUIContentRect(new GUIContent(resultString, !droppingFor ? resultTip : null), rect);
        }

        // ReSharper disable once FlagArgument
        public void SetDropping(bool dropping)
        {
            if (Dropping == dropping) return;
            m_droppingHashcode = dropping ? (int?)m_containerHashcode : null;
            EditorUtility.SetDirty(m_containerProperty.serializedObject.targetObject);
        }

        // ReSharper disable once FlagArgument
        public void SetSelecting(bool selecting)
        {
            if (Selecting == selecting) return;
            m_selectingHashcode = selecting ? (int?)m_containerHashcode : null;
            EditorUtility.SetDirty(m_containerProperty.serializedObject.targetObject);
        }

        public bool MouseInRects(Rect rect, Vector2 mousePosition) =>
            TimedRect.MouseInRects(m_containerHashcode, rect, mousePosition);

        readonly SerializedProperty m_containerProperty;
        SerializedProperty m_objectFieldProperty;
        SerializedProperty m_resultTypeProperty;
        readonly int m_containerHashcode;

        static int? m_droppingHashcode;
        static int? m_selectingHashcode;

        static string BuildEditorResultString(string resultType, Object @object)
        {
            if (@object == null)
                return !string.IsNullOrEmpty(resultType) 
                    ? resultType 
                    : null;

            var component = @object as Component;
            return component != null ? $"{component.gameObject.name} ( {InterfaceContainerBase.ConstructResolvedName(@object.GetType())} )" 
                : InterfaceGUI.GetObjectName(@object);
        }

        /// <summary>
        /// Created to address a very minor and super-rare graphical inconsistency.
        /// Definitely overkill, but... well there it is. And hey, it works!
        /// </summary>
        class TimedRect
        {
            Rect m_rect;
            readonly Stopwatch m_stopwatch;

            TimedRect(Rect rect)
            {
                m_rect = rect;
                m_stopwatch = Stopwatch.StartNew();
            }

            public static bool MouseInRects(int containerHash, Rect rect, Vector2 mousePosition)
            {
                m_lastAccess = Stopwatch.StartNew();

                if (!k_timestampedRects.TryGetValue(containerHash, out var rects))
                {
                    k_timestampedRects.Add(containerHash, (rects = new List<TimedRect>()));
                }

                rects.Add(new TimedRect(rect));

                var mouseInRect = false;
                rects.RemoveAll(r =>
                {
                    if (r.m_stopwatch.ElapsedMilliseconds > 100)
                        return true;

                    if (r.m_rect.Contains(mousePosition))
                        mouseInRect = true;

                    return false;
                });
                return mouseInRect;
            }

            static Stopwatch m_lastAccess;

            static readonly Dictionary<int, List<TimedRect>>
                k_timestampedRects = new Dictionary<int, List<TimedRect>>();

            static TimedRect()
            {
                EditorApplication.update += MaintainRects;
            }

            static void MaintainRects()
            {
                if (m_lastAccess == null || m_lastAccess.ElapsedMilliseconds <= 5000)
                    return;
                k_timestampedRects.Clear();
                m_lastAccess.Stop();
                m_lastAccess = null;
            }
        }
    }
}