using Core.Editor.Interface.GUI;
using UnityEngine;

namespace Core.Editor.Utility.NodeGUI
{
    public struct GUIConnection : IGUIConnection
    {
        public IGUIConnectable NodeFrom { get; set; }
        public IGUIConnectable NodeTo { get; set; }
        public Color Color { get; set; }
    }
}