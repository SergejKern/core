﻿using Core.Editor.Interface.GUI;
using Core.Unity.Extensions;
using UnityEditor;

namespace Core.Editor.Utility.NodeGUI
{
    public static class GUIConnectionOperations
    {
        public static void Draw(this IGUIConnection connection, GUIGrid grid)
        {
            var fromPos = connection.NodeFrom.GetOutgoingPos(grid);
            var toPos = connection.NodeTo.GetIncomingPos(grid);
          
            var prevColor = Handles.color;
            Handles.color = connection.Color;
            Handles.DrawLine( //DrawBezier
                fromPos,
                toPos
                //Config.From.Pos + offset,
                //Config.To.Pos + offset,
                //Config.Color,
                //null,
                //2f
            );

            var dir = (toPos - fromPos).normalized;

            var a1 = toPos - 10f * dir.Rotate(30f);
            var a2 = toPos - 10f * dir.Rotate(-30f);
            Handles.DrawLine(toPos, a1);
            Handles.DrawLine(toPos, a2);
            Handles.DrawLine(a1, a2);
            Handles.color = prevColor;
        }
    }
}