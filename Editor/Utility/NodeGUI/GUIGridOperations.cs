using Core.Editor.Interface.GUI;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.NodeGUI
{
    public static class GUIGridOperations
    {
        public static void UpdateDragGrid(this IGUIGrid data)
        {
            var rectSelection = data as IRectSelection;
            var e = Event.current;
            switch (e.type)
            {
                case EventType.MouseDown:
                    data.UpdateDragGridMouseDown();
                    rectSelection?.UpdateRectSelectionMouseDown();
                    break;
                case EventType.MouseDrag:
                    data.UpdateDragGridMouseDrag();
                    rectSelection?.UpdateRectSelectionMouseDrag();
                    break;
                case EventType.MouseUp:
                    data.UpdateDragGridMouseUp();
                    rectSelection?.UpdateRectSelectionMouseUp();
                    break;
            }
        }

        const int k_leftClick = 0;
        const int k_rightClick = 1;
        const int k_middleClick = 2;
        static void UpdateDragGridMouseDown(this IGUIGrid data)
        {
            var e = Event.current;
            if (e.button != k_middleClick)
                return;

            if (!data.EventRect.Contains(e.mousePosition))
                return;

            e.Use();
            data.Dragging = true;
        }
        static void UpdateRectSelectionMouseDown(this IRectSelection data)
        {
            var e = Event.current;
            if (e.button != k_leftClick)
                return;
            if (!data.EventRect.Contains(e.mousePosition))
                return;
            e.Use();

            data.Selecting = true;
            data.CurrentSelection = new Rect(e.mousePosition,Vector2.one);
        }

        static void UpdateRectSelectionMouseDrag(this IRectSelection data)
        {
            var e = Event.current;

            if (e.button != k_leftClick)
                return;
            if (!data.Selecting)
                return;
            if (!data.EventRect.Contains(e.mousePosition))
                return;
            e.Use();
            var rect = data.CurrentSelection;
            rect.max = e.mousePosition;
            data.CurrentSelection = rect;
            GUI.changed = true;
        }

        static void UpdateDragGridMouseDrag(this IGUIGrid data)
        {
            var e = Event.current;

            if (e.button != k_middleClick)
                return;
            if (!data.Dragging)
                return;
            if (!data.EventRect.Contains(e.mousePosition))
                return;
            e.Use();
            data.Offset += e.delta / data.Zoom;
            GUI.changed = true;
        }

        static void UpdateDragGridMouseUp(this IGUIGrid data)
        {
            Event.current.Use();
            data.Dragging = false;
            GUI.changed = true;
        }

        static void UpdateRectSelectionMouseUp(this IRectSelection data)
        {
            Event.current.Use();
            data.Selecting = false;
            GUI.changed = true;
        }

        public static void DrawGrid(this GUIGrid data)
        {
            data.DrawGrid(data.CoarseGridSpacing, data.CoarseGridOpacity);
            data.DrawGrid(data.FineGridSpacing, data.FineGridOpacity);
        }
        public static void DrawGrid(this IGUIGrid data, float gridSpacing, float gridOpacity)
        {
            var width = data.DrawRect.width / data.Zoom;
            var height = data.DrawRect.height / data.Zoom;

            var zoomSpacings = gridSpacing * data.Zoom;
            
            var widthDivs = Mathf.CeilToInt(width / gridSpacing)+1;
            var heightDivs = Mathf.CeilToInt(height / gridSpacing)+1;

            using (new HandleGUIScope())
            using (new ColorScope(data.GridColor.r, data.GridColor.g, data.GridColor.b, gridOpacity))
            {
                var off = (0.5f * data.DrawRect.size + data.Offset * data.Zoom);
                var newOffset = new Vector3(off.x % zoomSpacings, off.y % zoomSpacings, 0);

                for (var i = 0; i < widthDivs; i++)
                    Handles.DrawLine(new Vector3(zoomSpacings * i, -zoomSpacings, 0) + newOffset,
                        new Vector3(zoomSpacings * i, data.DrawRect.height+ zoomSpacings, 0f) + newOffset);

                for (var j = 0; j < heightDivs; j++)
                    Handles.DrawLine(new Vector3(-zoomSpacings, zoomSpacings * j, 0) + newOffset,
                        new Vector3(data.DrawRect.width+ zoomSpacings, zoomSpacings * j, 0f) + newOffset);
            }
        }
    }
}