using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility
{
    /// <summary> Helper for GUI </summary>
    public static class CustomGUI
    {
        static readonly GUIStyle k_splitterHorizontal;
        static readonly GUIStyle k_splitterVertical;

        static CustomGUI()
        {
            k_splitterHorizontal = new GUIStyle
            {
                normal = {background = EditorGUIUtility.whiteTexture},
                stretchWidth = true,
                margin = new RectOffset(0, 0, 7, 7)
            };
            k_splitterVertical = new GUIStyle
            {
                normal = {background = EditorGUIUtility.whiteTexture},
                stretchHeight = true,
                margin = new RectOffset(7, 7, 0, 0)
            };
        }

        static readonly Color k_splitterColor = EditorGUIUtility.isProSkin ? new Color(0.157f, 0.157f, 0.157f) : new Color(0.5f, 0.5f, 0.5f);
        static readonly Color k_primaryActiveColor = Color.cyan;
        static readonly Color k_secondaryActiveColor = Color.magenta;
        static readonly Color k_combinedActiveColor = Color.yellow;


        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        public static void HSplitter(float thickness = 1) => Splitter(k_splitterHorizontal, k_splitterColor, GUILayout.Height(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="color"> Color of Splitter</param>
        /// <param name="thickness"> thickness of splitter </param>
        public static void HSplitter(Color color, float thickness = 1)
            => Splitter(k_splitterHorizontal, color, GUILayout.Height(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        /// <param name="splitterStyle"> style for splitter </param>
        public static void HSplitter(GUIStyle splitterStyle, float thickness = 1f)
            => Splitter(splitterStyle, k_splitterColor, GUILayout.Height(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        public static void VSplitter(float thickness = 1) => Splitter(k_splitterVertical, k_splitterColor, GUILayout.Width(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="color"> Color of Splitter</param>
        /// <param name="thickness"> thickness of splitter </param>
        public static void VSplitter(Color color, float thickness = 1)
            => Splitter(k_splitterVertical, color, GUILayout.Width(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        /// <param name="splitterStyle"> style for splitter </param>
        public static void VSplitter(GUIStyle splitterStyle, float thickness = 1f)
            => Splitter(splitterStyle, k_splitterColor, GUILayout.Height(thickness));

        public static void Splitter(GUIStyle splitterStyle, Color color, params GUILayoutOption[] options)
        {
            var position = GUILayoutUtility.GetRect(GUIContent.none, splitterStyle, options);

            if (Event.current.type != EventType.Repaint)
                return;
            var restoreColor = GUI.color;
            GUI.color = color;
            splitterStyle.Draw(position, false, false, false, false);
            GUI.color = restoreColor;
        }


        public static bool ButtonOrReturnKey(string buttonText, params GUILayoutOption[] options)
        {
            var e = Event.current;
            return GUILayout.Button(buttonText, options) || (e.isKey && e.keyCode == KeyCode.Return);
        }

        /// <summary> draws button that can be activated like a toggle </summary>
        /// <param name="active"> is the button active </param>
        /// <param name="text"> text of button </param>
        /// <param name="options"> GUILayout-options </param>
        /// <returns> was button pressed </returns>
        // ReSharper disable FlagArgument
        public static bool ActivityButton(bool active, string text, params GUILayoutOption[] options)
        {
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active)
                GUI.backgroundColor = k_primaryActiveColor;

            using (new EditorGUI.DisabledScope(active))
            {
                if (GUILayout.Button(text, options))
                    buttonPressed = true;
            }

            if (active)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }

        public static bool ActivityButton(bool active, string text, Texture2D tex, params GUILayoutOption[] options)
        {
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active)
                GUI.backgroundColor = k_primaryActiveColor;

            using (new EditorGUI.DisabledScope(active))
            {
                var content = new GUIContent(text, tex);
                if (GUILayout.Button(content, options))
                    buttonPressed = true;
            }

            if (active)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }

        /// <summary> draws button that can have two different toggle states combined </summary>
        public static bool ActivityButton2(bool active1, bool active2, string text, params GUILayoutOption[] options)
        {
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active1)
                GUI.backgroundColor = k_primaryActiveColor;
            if (active2)
                GUI.backgroundColor = k_secondaryActiveColor;
            if (active1 && active2)
                GUI.backgroundColor = k_combinedActiveColor;

            if (GUILayout.Button(text, options))
                buttonPressed = true;

            if (active1 || active2)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }

        /// <summary> draws button that can have two different toggle states combined and a texture </summary>
        public static bool ActivityButton2(bool active1, bool active2, string text, Texture2D texture, params GUILayoutOption[] options)
        {
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active1)
                GUI.backgroundColor = k_primaryActiveColor;
            if (active2)
                GUI.backgroundColor = k_secondaryActiveColor;
            if (active1 && active2)
                GUI.backgroundColor = k_combinedActiveColor;

            var content = new GUIContent(text, texture);
            if (GUILayout.Button(content, options))
                buttonPressed = true;

            if (active1 || active2)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }
        // ReSharper restore FlagArgument

        public static void ReadonlyTextField(string value, params GUILayoutOption[] layoutOptions)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.TextField(value, layoutOptions);
            EditorGUI.EndDisabledGroup();
        }
    }
}