using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.AnimationHelper
{
    public class AvatarMaker
    {
        public static void MakeAvatarMask(GameObject[] gos)
        {
            var avatarMask = new AvatarMask();

            foreach (var go in gos)
            {
                avatarMask.AddTransformPath(go.transform);
            }

            var path = $"Assets/{gos[0].name.Replace(':', '_')}.mask";
            AssetDatabase.CreateAsset(avatarMask, path);
        }

        public static void MakeAvatar(GameObject go)
        {
            var avatar = AvatarBuilder.BuildGenericAvatar(go, "");
            avatar.name = go.name;
            Debug.Log(avatar.isHuman ? "is human" : "is generic");

            var path = $"Assets/{avatar.name.Replace(':', '_')}.ht";
            AssetDatabase.CreateAsset(avatar, path);
        }
    }
}