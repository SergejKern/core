using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Runtime.Interface;
using Core.Unity.Interface;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility
{
    public static class AssetOperations
    {
        public static void FixParentOfAsset(IScriptableObject asset)
        {
            var parent = asset.IntendedParent();
            AssetDatabase.SetMainObject((Object) parent, asset.Path());
            parent.SetDirty();
            asset.Reimport();
        }

        public static void TryFixAssetName(this IScriptableObject asset)
        {
            if(!(asset is INamed named))
                return;
            asset.name = named.Name.Trim();
        }

        public static string DefaultName<T>(this T asset) =>
            asset is IDefaultNameProvider nameProvider 
                ? nameProvider.DefaultName
                : ObjectNames.NicifyVariableName(asset.GetType().Name);

        public static string Path(this IScriptableObject asset) => AssetDatabase.GetAssetPath((Object) asset);
        public static void SetDirty(this IScriptableObject asset) => EditorUtility.SetDirty((Object) asset);
        public static void Reimport(this IScriptableObject asset) => AssetDatabase.ImportAsset(asset.Path());

        public static void RecordUndo(this IScriptableObject asset, string name) => Undo.RecordObject((Object) asset, name);

        public static IScriptableObject Parent(this IScriptableObject asset) => AssetDatabase.LoadMainAssetAtPath(asset.Path()) 
            as IScriptableObject;

        public static IEnumerable<IScriptableObject> ChildAssets(this IScriptableObject asset)
        {
            var parent = asset.Parent();
            return AssetDatabase.LoadAllAssetsAtPath(asset.Path()).Where(a=>!ReferenceEquals(a, parent)).OfType<IScriptableObject>();
        }

        public static IScriptableObject IntendedParent(this IScriptableObject asset) 
            => AssetDatabase.LoadAllAssetsAtPath(asset.Path()).OfType<IParentMarker>().FirstOrDefault().Parent;

        public static void AddToAsset(this IScriptableObject asset, IScriptableObject parent) => 
            AssetDatabase.AddObjectToAsset((Object) asset, (Object) parent);
    }
}